package com.ssi.mayur.exception;

public class InvalidInputsException extends Exception{

	public InvalidInputsException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}
	public InvalidInputsException(String arg0) {
		super("Invalid input");
		
	}

}
