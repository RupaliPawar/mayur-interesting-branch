package com.ssi.mayur.exception;

public class InitializeFieldException extends Exception {

	

	public InitializeFieldException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public InitializeFieldException(String message) {
		super(message);
		
	}

	

}
