package com.ssi.mayur.exception;

public class InsufficientDataException extends Exception{
	public InsufficientDataException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public InsufficientDataException(String arg0) {
		super(arg0);
	}


	
}
