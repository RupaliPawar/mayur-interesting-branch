package com.ssi.mayur.ws.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.ssi.mayur.model.util.UtilityModel;
import com.ssi.mayur.utility.IMayurServiceUtilityConstants;



public class AuthFilter implements Filter, IMayurServiceUtilityConstants{
	private FilterConfig mFilterConfig = null;
	@Override
	public void destroy() {
	
		
	}
   /*this method do the filtering task*/
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
	
		String reqId =UtilityModel.getNewAPIRequestSysId();
		request.setAttribute(K_API_REQUEST_ID, reqId);
		
		filterChain.doFilter(request, response);
	}

	
	/*this method use to start the filter process or initialize filter very first time*/
	@Override
	public void init(FilterConfig pFilterConfig) throws ServletException {
	
		mFilterConfig = pFilterConfig;
	}
}
