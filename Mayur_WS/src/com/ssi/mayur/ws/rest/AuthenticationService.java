package com.ssi.mayur.ws.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.google.gson.reflect.TypeToken;
import com.ssi.mayur.exception.InvalidInputsException;
import com.ssi.mayur.model.IAuthentication;
import com.ssi.mayur.model.vo.AuthenticationUserDetails;
import com.ssi.mayur.model.vo.UserVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.ws.util.UtilityWS;

@Component
@Path("/"+com.ssi.mayur.utility.IMayurServiceUtilityConstants.S_AUTH+"/")
public class AuthenticationService extends MayurServiceBase{

	@Autowired
	@Qualifier("authenticationModel")
	IAuthentication authenticationModel;
	
	public AuthenticationService(){
		super();
		logger = LoggerFactory.getLogger(this.getClass());
	}
	/* This module of web service use for to allow login into application for PHP*/
	@POST
	@Path(M_AUTHENTICATE_USER)
	@Consumes()
	public Response authenticateUser(@Context HttpServletRequest request){
		String inputEntity = "";
		String a = S_AUTH;
		String reqId = "";
		String response = "";
		UserVO userVO = null;
		UserVO returnedUserVO = null;
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_AUTHENTICATE_USER, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			
			try{
				userVO = (UserVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<UserVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(userVO == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			returnedUserVO = authenticationModel.authenticateUserLogin(userVO);
			response = MayurServiceCommonUtility.getJson(returnedUserVO);
			return getOkResponse(reqId, M_AUTHENTICATE_USER, response);
		}
		catch(Exception e){
			return handleException(reqId, M_AUTHENTICATE_USER, e);
		}
	}
	

	
	/*currently not using following service*/
	@POST
	@Path(M_GET_USER)
	@Consumes()
	public Response getUser(@Context HttpServletRequest request){
		String inputEntity = "";
		String a = S_AUTH;
		String reqId = "";
		String response = "";
		String userId = null;
		AuthenticationUserDetails returnedAuthenticationUserDetails = null;
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_GET_USER, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			
			try{
				userId = (String)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<String>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(userId == null || "".equals(userId.trim())){
				throw new InvalidInputsException("Given JSON String is not valid for User Id");
			}
			returnedAuthenticationUserDetails = authenticationModel.getAuthenticationUserDetails(userId);
			response = MayurServiceCommonUtility.getJson(returnedAuthenticationUserDetails);
			return getOkResponse(reqId, M_GET_USER, response);
		}
		catch(Exception e){
			return handleException(reqId, M_GET_USER, e);
		}
	}
	
}
