package com.ssi.mayur.ws.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.google.gson.reflect.TypeToken;
import com.ssi.mayur.dao.mysql.GeneralDAOImpl;
import com.ssi.mayur.exception.InitializeFieldException;

import com.ssi.mayur.exception.InvalidInputsException;

import com.ssi.mayur.model.IGeneral;

import com.ssi.mayur.model.vo.ProductCategoryVO;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductOrderVO;
import com.ssi.mayur.model.vo.MasterProductVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.ws.util.UtilityWS;

@Component
@Path("/"+com.ssi.mayur.utility.IMayurServiceUtilityConstants.S_PRODUCT+"/")
public class ProductService extends MayurServiceBase {
		@Autowired
	    @Qualifier("generalModel")
	    IGeneral generalModel;
	
	public ProductService() {
		super();
		logger = LoggerFactory.getLogger(this.getClass());
	}	

	/* This module of web service use for to create a master product category*/
	@POST
	@Path(M_CREATE_MASTER_PRODUCT_CATEGORY)
	@Consumes()
	public Response createMasterProductCategory(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";
		String response = "";
		MasterProductVO masterProduct = null;
		
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_CREATE_MASTER_PRODUCT_CATEGORY, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				masterProduct = (MasterProductVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<MasterProductVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(masterProduct == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			if(masterProduct!=null){		
				if(masterProduct.getMasterProductName()==null || (masterProduct.getMasterProductName().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter Master Product Name");
				}	
				if(!masterProduct.getMasterProductName().matches("[A-Za-z]+"))
				{
					throw new InvalidInputsException("Master Product name must contain character only");
				}
			}	
			

			generalModel.createMasterProductCategory(masterProduct);
			
			return getOkResponse(reqId, M_CREATE_MASTER_PRODUCT_CATEGORY, "");
			}
		catch(Exception e){		
			 return handleException(reqId, M_CREATE_MASTER_PRODUCT_CATEGORY, e);
		}			
	}
	/* This module of web service use for to modify a master product category*/
	@POST
	@Path(M_MODIFY_MASTER_PRODUCT_CATEGORY)
	@Consumes()
	public Response modifyMasterProductCategory(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";
		String response = "";
		MasterProductVO masterProduct = null;
		
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_MODIFY_MASTER_PRODUCT_CATEGORY, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				masterProduct = (MasterProductVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<MasterProductVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(masterProduct == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
						
			String data=inputEntity;
			int v1=data.indexOf("true");
			int v2=data.indexOf("false");
			if(v1==-1){
				masterProduct.setVisibleMasterProduct(false);
			}
			else{
				masterProduct.setVisibleMasterProduct(true);
			}
						
			if(masterProduct!=null){		
				if(masterProduct.getMasterProductName()==null || (masterProduct.getMasterProductName().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter  Master Product Name");
				}	
				if(!masterProduct.getMasterProductName().matches("[A-Za-z]+"))
				{
					throw new InvalidInputsException("Master Product name must contain character only");
				}
				if(masterProduct.getSysId()==null||(masterProduct.getSysId().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter  Master Product ID");
				}
				
			}						
			generalModel.modifyMasterProductCategory(masterProduct);
			
			return getOkResponse(reqId, M_MODIFY_MASTER_PRODUCT_CATEGORY, "");
			}
		catch(Exception e){		
			 return handleException(reqId, M_MODIFY_MASTER_PRODUCT_CATEGORY, e);
		}			
	}
	
	/* This module of web service use for to retrieve a master product category*/
	@GET
	@Path(M_GET_MASTER_PRODUCT_CATEGORY)
	@Consumes()
	public Response getMasterProductCategory(@Context HttpServletRequest request){
		String inputEntity="";
		String reqId = "";
		String response = "";
	    List<MasterProductVO> roomList=null;
		try{
		
			String authKey = request.getHeader(H_AUTH_KEY);	
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
			logRequest(reqId,M_GET_MASTER_PRODUCT_CATEGORY, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");			
			roomList=generalModel.getMasterProductCategory();
			response=MayurServiceCommonUtility.getJson(roomList);
		    return getOkResponse(reqId, M_GET_MASTER_PRODUCT_CATEGORY, response);
		}
		catch(Exception e){
			
			return handleException(reqId, M_GET_MASTER_PRODUCT_CATEGORY, e);
		}			
	}
	
	/* This module of web service use for to create a  product category*/

	@POST
	@Path(M_CREATE_PRODUCT_CATEGORY)
	@Consumes()
	public Response createProductCategory(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";		
		ProductCategoryVO productCategory = null;	
		GeneralDAOImpl generalDao=new GeneralDAOImpl();
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_CREATE_PRODUCT_CATEGORY, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				productCategory = (ProductCategoryVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<ProductCategoryVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(productCategory == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			if(productCategory!=null){		
				if(productCategory.getProductCategoryName()==null || (productCategory.getProductCategoryName().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter Product category Name");
				}
				if(!productCategory.getProductCategoryName().matches("[A-Za-z]+"))
				{
					throw new InvalidInputsException(" Product Category name must contain character only");
				}
								
			}						
			generalModel.createProductCategory(productCategory);			
			return getOkResponse(reqId, M_CREATE_PRODUCT_CATEGORY, "");
			
			}
		catch(Exception e){		
			 return handleException(reqId, M_CREATE_PRODUCT_CATEGORY, e);
		}			
	}
	/* This module of web service use for to modify  product category*/
	@POST
	@Path(M_MODIFY_PRODUCT_CATEGORY)
	@Consumes()
	public Response modifyProductCategory(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";
		
		ProductCategoryVO productCategory = null;
		
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_MODIFY_PRODUCT_CATEGORY, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				productCategory = (ProductCategoryVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<ProductCategoryVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			String data=inputEntity;
			int v1=data.indexOf("true");
			int v2=data.indexOf("false");
			if(v1==-1){
				productCategory.setIsVisibleProductCategory(false);
			}
			else{
				productCategory.setIsVisibleProductCategory(true);
			}
			if(productCategory == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			if(productCategory!=null){		
				if(productCategory.getProductCategoryName()==null || (productCategory.getProductCategoryName().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter Product category Name");
				}
				if(!productCategory.getProductCategoryName().matches("[A-Za-z]+"))
				{
					throw new InvalidInputsException("Product Category name must contain character only");
				}
			}
			generalModel.modifyProductCategory(productCategory);			
			return getOkResponse(reqId, M_MODIFY_PRODUCT_CATEGORY, "");
			}
		catch(Exception e){		
			 return handleException(reqId, M_MODIFY_PRODUCT_CATEGORY, e);
		}			
	}
	
	/* This module of web service use for to retrieve product category*/
	
	@GET
	@Path(M_GET_PRODUCT_CATEGORY)
	@Consumes()
	public Response getProductCategory(@Context HttpServletRequest request){
		String inputEntity="";
		String reqId = "";
		String response = "";
	    List<ProductCategoryVO> returnedProductCategoryVO=null;
		try{		
			String authKey = request.getHeader(H_AUTH_KEY);	
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
			logRequest(reqId,M_GET_PRODUCT_CATEGORY,MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");						
			returnedProductCategoryVO = generalModel.getProductCategory();
			response=MayurServiceCommonUtility.getJson(returnedProductCategoryVO);
			return getOkResponse(reqId, M_GET_PRODUCT_CATEGORY, response);
		}
		catch(Exception e){			
			return handleException(reqId, M_GET_PRODUCT_CATEGORY, e);
		}
			
	}
	
	
	/* This module of web service use for to create a product */

	@POST
	@Path(M_CREATE_PRODUCT_FOR_CATEGORY)
	@Consumes()
	public Response createPoductForCategory(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";		
		ProductDetailVO productdetail = null;		
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_CREATE_PRODUCT_FOR_CATEGORY, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				productdetail = (ProductDetailVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<ProductDetailVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(productdetail == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			if(productdetail!=null){		
				if(productdetail.getProductCategoryId()==null || (productdetail.getProductCategoryId().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter Product category ID");
				}
				
				
				if(productdetail.getProductName()==null || (productdetail.getProductName().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter Product Name");
				}
				if(productdetail.getPrice()==0.0 ){
					throw new InitializeFieldException("Field must contain data Enter Price");
				}
				if(productdetail.getStock()==0){
					throw new InitializeFieldException("Field must contain data Enter Stock");
				}
				for (int i = 0; i <productdetail.getProductImageList().size(); i++) {
					if(productdetail.getProductImageList().get(i).getProductImage()==null || (productdetail.getProductImageList().get(i).getProductImage().trim().length()<=0)){
						throw new InitializeFieldException("field must contain atleast one image");
					}
				}
			}
											
			generalModel.createPoductForCategory(productdetail);			
			return getOkResponse(reqId, M_CREATE_PRODUCT_FOR_CATEGORY, "");
			}
		catch(Exception e){		
			 return handleException(reqId, M_CREATE_PRODUCT_FOR_CATEGORY, e);
		}			
	}
	/* This module of web service use for to modify a product */
	@POST
	@Path(M_MODIFY_PRODUCT_FOR_CATEGORY)
	@Consumes()
	public Response modifyPoductForCategory(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";		
		ProductDetailVO productDetails = null;		
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_MODIFY_PRODUCT_FOR_CATEGORY, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				productDetails = (ProductDetailVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<ProductDetailVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(productDetails == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			if(productDetails!=null){		
				if(productDetails.getProductCategoryId()==null || (productDetails.getProductCategoryId().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter Product category Name");
			    }				
				
				if(productDetails.getProductName()==null || (productDetails.getProductName().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter Product Name");
				}
				
				if(productDetails.getPrice()==0.0 ){
					throw new InitializeFieldException("Field must contain data Enter Price");
				}
				if(productDetails.getStock()==0){
					throw new InitializeFieldException("Field must contain data Enter Stock");
				}
				
				for (int i = 0; i <productDetails.getProductImageList().size(); i++) {
					if(productDetails.getProductImageList().get(i).getProductImage()==null || (productDetails.getProductImageList().get(i).getProductImage().trim().length()<=0)){
						throw new InitializeFieldException("field must contain atleast one image");
					}
				}
	
			}
			String data=inputEntity;
			int v1=data.indexOf("true");
			int v2=data.indexOf("false");
			if(v1==-1){
				productDetails.setIsVisibleProduct(false);
			}
			else{
				productDetails.setIsVisibleProduct(true);
			}
			
			generalModel.modifyPoductForCategory(productDetails);			
			return getOkResponse(reqId, M_MODIFY_PRODUCT_CATEGORY, "");
			}
		catch(Exception e){		
			 return handleException(reqId, M_MODIFY_PRODUCT_CATEGORY, e);
		}			
	}
	
	
	/* This module of web service use for to retrieve a product */
	
	@GET
	@Path(M_GET_ACTUAL_PRODUCT)
	@Consumes()
	public Response getActualProduct(@Context HttpServletRequest request){
		String inputEntity="";
		String reqId = "";
		String response = "";
	    List<ProductDetailVO> returnedProductCategoryVO=null;
		try{		
			String authKey = request.getHeader(H_AUTH_KEY);	
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
			logRequest(reqId,M_GET_ACTUAL_PRODUCT,MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");						
			returnedProductCategoryVO = generalModel.getActualProduct();
			response=MayurServiceCommonUtility.getJson(returnedProductCategoryVO);
			return getOkResponse(reqId, M_GET_ACTUAL_PRODUCT, response);
		}
		catch(Exception e){			
			return handleException(reqId, M_GET_ACTUAL_PRODUCT, e);
		}
			
	}
	
	/* This module of web service use for to remove a product image */
	
	@POST
	@Path(M_REMOVE_PRODUCT_IAMGE)
	@Consumes()
	public Response removeProductImage(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";
		String response = "";
		ProductDetailVO productImage = null;
		
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_REMOVE_PRODUCT_IAMGE, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				productImage = (ProductDetailVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<ProductDetailVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(productImage == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}	
			generalModel.removeProductImage(productImage);
			
			return getOkResponse(reqId, M_REMOVE_PRODUCT_IAMGE, "");
			}
		catch(Exception e){		
			 return handleException(reqId, M_REMOVE_PRODUCT_IAMGE, e);
		}			
	}
	
	/* This module of web service use for to retrieve a list of product category for specified master product category*/
	@GET
	@Path(M_GET_CATEGORIES+"/{masterProductId}")
	@Consumes()
	public Response getCategories(@Context HttpServletRequest request,@PathParam("masterProductId")String masterProductId){
		String inputEntity="";
		String reqId = "";
		String response = "";
	    List<ProductCategoryVO> returnedProductCategoryVO=null;
		try{		
			String authKey = request.getHeader(H_AUTH_KEY);	
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
			logRequest(reqId,M_GET_CATEGORIES,MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");						
			returnedProductCategoryVO = generalModel.getCategories(masterProductId);
			response=MayurServiceCommonUtility.getJson(returnedProductCategoryVO);
			return getOkResponse(reqId, M_GET_CATEGORIES, response);
		}
		catch(Exception e){			
			return handleException(reqId, M_GET_CATEGORIES, e);
		}
			
	}
	/* This module of web service use for to retrieve a list of product  for specified product category */
	@GET
	@Path(M_GET_PRODUCT_LIST_CATEGORY+"/{productCategoryId}")
	@Consumes()
	public Response getPoductListForCategory(@Context HttpServletRequest request ,@PathParam("productCategoryId")String productCategoryId){
		String inputEntity="";
		String reqId = "";
		String response = "";
		List<ProductDetailVO> returnedProductList=null;
		try{		
			String authKey = request.getHeader(H_AUTH_KEY);	
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
			logRequest(reqId,M_GET_PRODUCT_LIST_CATEGORY, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");			
			productCategoryId=productCategoryId.replaceAll("\\s", "");
			returnedProductList = (List<ProductDetailVO>) generalModel.getPoductListForCategory(productCategoryId);		
			response=MayurServiceCommonUtility.getJson(returnedProductList);
		    return getOkResponse(reqId, M_GET_PRODUCT_LIST_CATEGORY, response);							
		}
		catch(Exception e){			
			return handleException(reqId, M_GET_PRODUCT_LIST_CATEGORY, e);
		}
			
	}
	/* This module of web service use for to retrieve a one product details for specified  product */
	@GET
	@Path(M_GET_ITEM_DETAILS+"/{productId}")
	@Consumes()
	public Response getItemDetails(@Context HttpServletRequest request ,@PathParam("productId")String productId){
		String inputEntity="";
		String reqId = "";
		String response = "";
	    ProductDetailVO returnedUserVO=null;
		try{	
			String authKey = request.getHeader(H_AUTH_KEY);	
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
			logRequest(reqId,M_GET_ITEM_DETAILS, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");			
			returnedUserVO =generalModel.getItemDetails(productId);
			response=MayurServiceCommonUtility.getJson(returnedUserVO);
		    return getOkResponse(reqId, M_GET_ITEM_DETAILS, response);
		}
		catch(Exception e){
			
			return handleException(reqId, M_GET_ITEM_DETAILS, e);
		}			
	}
	

	/*This module of web service use to create order into the database*/
	@POST
	@Path(M_CREATE_ORDER_ENTRY)
	@Consumes()
	public Response createOrderEntry(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";		
		ProductOrderVO productOrder = null;		
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
		    logRequest(reqId, M_CREATE_ORDER_ENTRY, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				productOrder = (ProductOrderVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<ProductOrderVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(productOrder == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			if(productOrder.getTransactionID()==null){
				throw new InitializeFieldException("field must contain transaction ID");
			}
			generalModel.createOrderEntry(productOrder);			
			return getOkResponse(reqId, M_CREATE_ORDER_ENTRY, "");
			}
		    catch(Exception e){					
			 return handleException(reqId, M_CREATE_ORDER_ENTRY, e);
		}			
	}
	

	
	/*This module of web service use to confirm order place successfully or not*/
	@POST
	@Path(M_CONFIRM_ORDER)
	@Consumes()
	public Response confirmOrder(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";		
		ProductOrderVO productOrder = null;		
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
		    logRequest(reqId, M_CONFIRM_ORDER, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				productOrder = (ProductOrderVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<ProductOrderVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(productOrder == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			if(productOrder.getTransactionID()==null){
				throw new InitializeFieldException("field must contain transaction ID");
			}
			generalModel.confirmOrder(productOrder);			
			return getOkResponse(reqId, M_CONFIRM_ORDER, "");
			}
		    catch(Exception e){					
			 return handleException(reqId, M_CONFIRM_ORDER, e);
		}			
	}
	
	
	
	/*This module of web service use to update order into the database*/
	@POST
	@Path(M_UPDATE_ORDER_STATUS)
	@Consumes()
	public Response updateOrderStatus(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";		
		ProductOrderVO orderStatus = null;		
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
		    logRequest(reqId, M_UPDATE_ORDER_STATUS, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				orderStatus = (ProductOrderVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<ProductOrderVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(orderStatus == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			generalModel.updateOrderStatus(orderStatus);			
			return getOkResponse(reqId, M_UPDATE_ORDER_STATUS, "");
	    }
		catch(Exception e){					
			 return handleException(reqId, M_UPDATE_ORDER_STATUS, e);
		}			
	}
	
	
	
	@GET
	@Path(M_GET_ORDER_DETAILS)
	@Consumes()
	public Response getOrderDetails(@Context HttpServletRequest request){
		String inputEntity="";
		String reqId = "";
		String response = "";
	    List<ProductOrderVO> orderDetails=null;
		try{		
			String authKey = request.getHeader(H_AUTH_KEY);	
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
			logRequest(reqId,M_GET_ORDER_DETAILS,MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");						
			orderDetails = generalModel.getorderDetails();
			response=MayurServiceCommonUtility.getJson(orderDetails);
			return getOkResponse(reqId, M_GET_ORDER_DETAILS, response);
		}
		catch(Exception e){				
			return handleException(reqId, M_GET_ORDER_DETAILS, e);		
		}			
	}
	
	
	
	@GET
	@Path(M_GET_ORDER_HISTORY_FOR_USER+"/{userId}")
	@Consumes()
	public Response getOrderHistoryForUser(@Context HttpServletRequest request ,@PathParam("userId")String userId){
		String inputEntity="";
		String reqId = "";
		String response = "";
	    ProductOrderVO productOrderHistory=null;
		try{		
			String authKey = request.getHeader(H_AUTH_KEY);	
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
			logRequest(reqId,M_GET_ORDER_HISTORY_FOR_USER,MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");						
			productOrderHistory = generalModel.getOrderHistoryForUser(userId);
			response=MayurServiceCommonUtility.getJson(productOrderHistory);
			return getOkResponse(reqId, M_GET_ORDER_HISTORY_FOR_USER, response);
		}
		catch(Exception e){				
			return handleException(reqId, M_GET_ORDER_HISTORY_FOR_USER, e);		
		}			
	}
	
	
	
}
