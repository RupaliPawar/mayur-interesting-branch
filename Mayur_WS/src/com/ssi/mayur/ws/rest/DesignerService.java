package com.ssi.mayur.ws.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ssi.mayur.exception.InvalidInputsException;
import com.ssi.mayur.model.IDesigner;
import com.ssi.mayur.model.vo.DesignerExpertVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.ws.util.UtilityWS;


@Component
@Path("/"+com.ssi.mayur.utility.IMayurServiceUtilityConstants.S_DESIGNER+"/")
public class DesignerService extends MayurServiceBase{
	
	public DesignerService() {
		super();
		logger = LoggerFactory.getLogger(this.getClass());
	}

	@Autowired
    @Qualifier("designerModel")
    IDesigner designerModel;

	/* This module of web service retrieve  detail of designers*/
@GET
@Path(M_GET_DESIGNER)
@Consumes()
public Response getDesigner(@Context HttpServletRequest request){
	String inputEntity="";
	String reqId = "";
	String response = "";
    List<DesignerExpertVO> returnedProductCategoryVO=null;
	try{		
		String authKey = request.getHeader(H_AUTH_KEY);	
		inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
		logRequest(reqId,M_GET_DESIGNER,MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");								
		returnedProductCategoryVO =designerModel.getDesigner();
		response=MayurServiceCommonUtility.getJson(returnedProductCategoryVO);
		return getOkResponse(reqId, M_GET_DESIGNER, response);
	}
	catch(Exception e){			
		return handleException(reqId, M_GET_DESIGNER, e);
	}
		
}
/* This module of web service retrieve  detail of specified designer work*/
@GET
@Path(M_GET_DESIGNER_WORK+"/{designerId}")
@Consumes()
public Response getDesignerWork(@Context HttpServletRequest request ,@PathParam("designerId")String designerId){
	String inputEntity="";
	String reqId = "";
	String response = "";
	DesignerExpertVO returnedDesignerWork=null;
	try{		
		String authKey = request.getHeader(H_AUTH_KEY);	
		inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
		logRequest(reqId,M_GET_DESIGNER_WORK,MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");						
		if(designerId==null || designerId.equals("null")){
			throw new InvalidInputsException("Given Design ID does not contain any valid value");
		}
		designerId=designerId.replaceAll("\\s", "");
		returnedDesignerWork = designerModel.getDesignerWork(designerId);
		response=MayurServiceCommonUtility.getJson(returnedDesignerWork);
		return getOkResponse(reqId, M_GET_DESIGNER_WORK, response);
	}
	catch(Exception e){			
		return handleException(reqId, M_GET_DESIGNER_WORK, e);
	}
		
}
}
