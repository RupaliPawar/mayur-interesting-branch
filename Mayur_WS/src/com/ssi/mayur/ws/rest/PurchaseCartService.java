package com.ssi.mayur.ws.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.google.gson.reflect.TypeToken;
import com.ssi.mayur.dao.mysql.GeneralDAOImpl;
import com.ssi.mayur.exception.InitializeFieldException;
import com.ssi.mayur.exception.InvalidInputsException;
import com.ssi.mayur.exception.TimeOutException;
import com.ssi.mayur.model.IGeneral;
import com.ssi.mayur.model.vo.CartVO;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.ws.util.UtilityWS;

@Component
@Path("/"+com.ssi.mayur.utility.IMayurServiceUtilityConstants.S_PURCHASECART+"/")
public class PurchaseCartService extends MayurServiceBase{
	@Autowired
    @Qualifier("generalModel")
    IGeneral generalModel;

	public PurchaseCartService() {		
		super();
		logger = LoggerFactory.getLogger(this.getClass());		
	}
	
	/* This module of web service use for to add product into cart */
	
	@POST
	@Path(M_SAVE_CART_DETAILS)
	@Consumes()
	public Response saveCartDetails(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";		
		CartVO cartDetails=null;
		CartVO returnList=null;
		String response = "";
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_SAVE_CART_DETAILS, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			
			try{
				cartDetails = (CartVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<CartVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(cartDetails == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
//			if(cartDetails!=null){		
//				if(cartDetails.getImeiNumber()==null || (cartDetails.getImeiNumber().trim().length()<=0)){
//					throw new InitializeFieldException("Field must contain data Enter imei number");
//				}	
//				for (int i = 0; i < cartDetails.getListCartItem().size(); i++) {
//					if(cartDetails.getListCartItem().get(i).getSysId()==null){
//						throw new InitializeFieldException("Field must contain data Enter product Id");
//					}
//				}

//			}						
			returnList=generalModel.saveCartDetails(cartDetails);
			response=MayurServiceCommonUtility.getJson(returnList);
			return getOkResponse(reqId, M_SAVE_CART_DETAILS, response);
			}
		catch(Exception e){		
			 return handleException(reqId, M_SAVE_CART_DETAILS, e);
		}			
	}
	
	
	/* This module of web service use for to retrieve details of product available into cart*/
	@GET
	@Path(M_GET_CART_DETAILS+"/{imieId}")
	@Consumes()
	public Response getCartDetails(@Context HttpServletRequest request,@PathParam("imieId")String imieId){
		String inputEntity="";
		String reqId = "";
		String response = "";
		CartVO cartDetails=null;
		try{		
			String authKey = request.getHeader(H_AUTH_KEY);	
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
			logRequest(reqId,M_GET_CART_DETAILS,MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");						
			
			cartDetails = (CartVO) generalModel.getCartDetails(imieId);

			response=MayurServiceCommonUtility.getJson(cartDetails);
			return getOkResponse(reqId, M_GET_CART_DETAILS, response);
		}
		catch(Exception e){			
			return handleException(reqId, M_GET_CART_DETAILS, e);
		}
	}	
	
	/* This module of web service use for to remove product from cart*/
	
	
	@GET
	@Path(M_REMOVE_PRODUCT_FROM_CART+"/{cartId}"+"/{productId}")
	@Consumes()
	public Response removeProductFromCart(@Context HttpServletRequest request,@PathParam("cartId")String cartId,@PathParam("productId")String productId){
		String inputEntity="";
		String reqId = "";
		String response = "";
		CartVO cartList=null;
		try{		
			String authKey = request.getHeader(H_AUTH_KEY);	
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
			logRequest(reqId,M_REMOVE_PRODUCT_FROM_CART,MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");																					
//			if(productId==null || (productId.trim().length()<=0)){
//				throw new InitializeFieldException("Field must contain data Enter product Id");
//			}
			if(cartId==null || (cartId.trim().length()<=0)){
				throw new InitializeFieldException("Field must contain data Enter card Id");
			}
			
			cartList=generalModel.removeProductFromCart(cartId,productId);	
		response=MayurServiceCommonUtility.getJson(cartList);
			return getOkResponse(reqId, M_REMOVE_PRODUCT_FROM_CART,response);
		}
		catch(Exception e){			
			return handleException(reqId, M_REMOVE_PRODUCT_FROM_CART, e);
		}
	}

}

	
	
	
	
	
	
	
	
	
	
	
	
	


