package com.ssi.mayur.ws.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;



import com.google.gson.reflect.TypeToken;
import com.ssi.mayur.exception.InitializeFieldException;
import com.ssi.mayur.exception.InvalidInputsException;
import com.ssi.mayur.model.IGeneral;
import com.ssi.mayur.model.vo.DesignBookletVO;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.ws.util.UtilityWS;


@Component
@Path("/"+com.ssi.mayur.utility.IMayurServiceUtilityConstants.S_DESIGNBOOKET+"/")
public class DesignBookletService extends MayurServiceBase{
	@Autowired
    @Qualifier("generalModel")
    IGeneral generalModel;
	
	public DesignBookletService() {
		super();
		logger = LoggerFactory.getLogger(this.getClass());		
	}
	
	/* This module of web service use for to store details of like product */
	@POST
	@Path(M_SAVE_IN_DESIGNBOOKLET)
	@Consumes()
	public Response saveInDesignBooklet(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";
		String response = "";
		DesignBookletVO designbooklet=null;
		
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_SAVE_IN_DESIGNBOOKLET, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				designbooklet = (DesignBookletVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<DesignBookletVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(designbooklet == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			if(designbooklet!=null){		
				if(designbooklet.getImeiNumber()==null || (designbooklet.getImeiNumber().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter Room Name");
				}	
				
				if(designbooklet.getProductId()==null || (designbooklet.getProductId().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter Room Name");
				}
			}						
			generalModel.saveInDesignBooklet(designbooklet);
			
			return getOkResponse(reqId, M_SAVE_IN_DESIGNBOOKLET, "");
			}
		catch(Exception e){		
			 return handleException(reqId, M_SAVE_IN_DESIGNBOOKLET, e);
		}			
	}
	
	/* This module of web service retrieve  details of like product which u have store into the Designbooklet*/
	@GET
	@Path(M_GET_DETAILS_FOR_DESIGNBOOKLET+"/{imieId}")
	@Consumes()
	public Response getDetailsForDesignBooklet(@Context HttpServletRequest request,@PathParam("imieId")String imieId){
		String inputEntity="";
		String reqId = "";
		String response = "";
	    List<ProductDetailVO> returnedProductCategoryVO=null;
		try{		
			String authKey = request.getHeader(H_AUTH_KEY);	
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());	
			logRequest(reqId,M_GET_DETAILS_FOR_DESIGNBOOKLET,MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey),request.getRequestURL().toString(), "");						
			returnedProductCategoryVO = generalModel.getDetailsForDesignBooklet(imieId);
			response=MayurServiceCommonUtility.getJson(returnedProductCategoryVO);
			return getOkResponse(reqId, M_GET_DETAILS_FOR_DESIGNBOOKLET, response);
		}
		catch(Exception e){			
			return handleException(reqId, M_GET_DETAILS_FOR_DESIGNBOOKLET, e);
		}
	}	

}
