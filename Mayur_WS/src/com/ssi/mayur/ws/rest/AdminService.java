package com.ssi.mayur.ws.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.google.gson.reflect.TypeToken;
import com.ssi.mayur.exception.InitializeFieldException;
import com.ssi.mayur.exception.InvalidInputsException;
import com.ssi.mayur.model.IAdmin;
import com.ssi.mayur.model.vo.UserVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.ws.util.UtilityWS;


@Component
@Path("/"+com.ssi.mayur.utility.IMayurServiceUtilityConstants.S_ADMIN+"/")
public class AdminService extends MayurServiceBase{
	
	@Autowired
	@Qualifier("adminModel")
	IAdmin adminModel;
	
	public AdminService(){
		super();
		logger = LoggerFactory.getLogger(this.getClass());
	}
	
	/*This web service create new user for mobile apps*/
	@POST
	@Path(M_CREATE_USER)
	@Consumes()
	public Response createUser(@Context HttpServletRequest request){
		String inputEntity = "";

		String reqId = "";
		String response = "";
		UserVO userVO = null;
		String returnedUserVO = null;
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_CREATE_USER, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				userVO = (UserVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<UserVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(userVO == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}			
			if(userVO!=null){
				if(userVO.getFname()==null || (userVO.getFname().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter First Name");
				}
								
				if(userVO.getAddress()==null || (userVO.getAddress().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter Address");
				}

				if(userVO.getCity()==null || (userVO.getCity().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter city");
				}				
				if(userVO.getMobileNo()==null || (userVO.getMobileNo().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter mobile no");
				}
								
				if(userVO.getPincode()==null || (userVO.getPincode().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter Pin code");
				}				
				if(userVO.getState()==null || (userVO.getState().trim().length()<=0)){
					throw new InitializeFieldException("Field must contain data Enter State");
				}
				
			}			
			adminModel.createUser(userVO);
			
			return getOkResponse(reqId, M_CREATE_USER, "");
			}
		catch(Exception e){		
			 return handleException(reqId, M_CREATE_USER, e);
		}	
		
	}
	
	/*This module use to login for mobile apps*/
	@POST
	@Path(M_LOGIN)
	@Consumes()
	public Response login(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";
		String response = "";
		UserVO userVO = null;
		UserVO returnedUserVO = null;
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_LOGIN, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				userVO = (UserVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<UserVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(userVO == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			if(userVO.getEmailId()==null || (userVO.getEmailId().trim().length()<=0)){
				throw new InitializeFieldException("Field must contain data Enter email Id");
			}
			if(userVO.getPassword()==null || (userVO.getPassword().trim().length()<=0)){
				throw new InitializeFieldException("Field must contain data Enter Password");
			}
			returnedUserVO =adminModel.login(userVO);
			response=MayurServiceCommonUtility.getJson(returnedUserVO);
			return getOkResponse(reqId, M_LOGIN, response);
			}
		catch(Exception e){		
			 return handleException(reqId, M_LOGIN, e);
		}	
		
	}
	/*This module use to activate user account*/
	@POST
	@Path(M_ACTIVATE_ACCOUNT)
	@Consumes()
	public Response activateAccount(@Context HttpServletRequest request){
		String inputEntity = "";
		String reqId = "";
		String response = "";
		UserVO userVO = null;
		UserVO returnedUserVO = null;
		try{
			inputEntity = UtilityWS.getStringFromInputStream(request.getInputStream());			
			String authKey = request.getHeader(H_AUTH_KEY);
			logRequest(reqId, M_ACTIVATE_ACCOUNT, MayurServiceCommonUtility.concatinateStrings(H_AUTH_KEY, DELIMETER_FOR_KEY_VALUE, authKey), request.getRequestURL().toString(), inputEntity);
			try{
				userVO = (UserVO)MayurServiceCommonUtility.getObjectFromJson(inputEntity, new TypeToken<UserVO>(){}.getType());	
			}
			catch(Exception e){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity", e);
			}
			if(userVO == null){
				throw new InvalidInputsException("Given JSON String is not valid for User Data entity");
			}
			if(userVO.getEmailId()==null || (userVO.getEmailId().trim().length()<=0)){
				throw new InitializeFieldException("Field must contain data Enter email Id");
			}
			if(userVO.getActivateAccountCode()==null || (userVO.getActivateAccountCode().trim().length()<=0)){
				throw new InitializeFieldException("Field must contain data Enter verification code");
			}
			returnedUserVO =adminModel.activateAccount(userVO);
			response=MayurServiceCommonUtility.getJson(returnedUserVO);
			return getOkResponse(reqId, M_ACTIVATE_ACCOUNT, response);
			}
		catch(Exception e){		
			return handleException(reqId, M_ACTIVATE_ACCOUNT, e);
		}	
		
	}

}
