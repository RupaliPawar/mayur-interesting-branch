package com.ssi.mayur.ws.rest;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;

import com.ssi.mayur.exception.AccessDeniedException;
import com.ssi.mayur.exception.DatabaseConnectionException;
import com.ssi.mayur.exception.ExistDataException;
import com.ssi.mayur.exception.InitializeFieldException;
import com.ssi.mayur.exception.InsufficientDataException;
import com.ssi.mayur.exception.InvalidInputsException;
import com.ssi.mayur.exception.TimeOutException;
import com.ssi.mayur.utility.IMayurServiceUtilityConstants;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.ws.vo.ErrorVO;
import com.ssi.mayur.ws.vo.ResponseVO;



public class MayurServiceBase implements IMayurServiceUtilityConstants {
	protected Logger logger = null;
	
	/* this module use to send correct response without any exception*/
	protected Response getOkResponse(String reqId, String method, String data){
		ResponseVO responseBean = new ResponseVO(STATUS_OK, data);
		responseBean.setRequestId(reqId);
		String jsonResponse = responseBean.getAsJSON();
		logResponse(reqId, method, jsonResponse);
		
		Response response = Response.status(Status.OK).entity(jsonResponse).type(MediaType.APPLICATION_JSON).build();		
		
		return response;
	}
	
	/* this module use to decide whether handle the exception or throw it*/
	protected Response handleException(String reqId, String method, Exception e){
		ErrorVO errorBean = new ErrorVO();
//		errorBean.setMessage(e.getMessage());
		errorBean.setDetails(MayurServiceCommonUtility.getExceptionAsString(e));
		
		ResponseVO responseBean = new ResponseVO(MayurServiceCommonUtility.getJson(errorBean));	
		responseBean.setRequestId(reqId);
		
		if(e instanceof InvalidInputsException){
			responseBean.setStatusCode(STATUS_INVALID_INPUTS);
			responseBean.setMessage(INVALID_INPUT);
			String strResponseBean = MayurServiceCommonUtility.getJson(responseBean);
			logResponse(reqId, method, strResponseBean);//edit
//			return Response.status(Status.BAD_REQUEST).entity(strResponseBean).type(MediaType.APPLICATION_JSON).build();
			return Response.status(Status.OK).entity(strResponseBean).type(MediaType.APPLICATION_JSON).build();
		}
		
		if(e instanceof InsufficientDataException){
			responseBean.setStatusCode(STATUS_NO_CONTENT);
			responseBean.setMessage(MESSAGE_NO_DATA);
			String strResponseBean=MayurServiceCommonUtility.getJson(responseBean);
			logResponse(reqId, method, strResponseBean);
			return Response.status(Status.OK).entity(strResponseBean).type(MediaType.APPLICATION_JSON).build();
			
		}
		if(e instanceof InitializeFieldException){
			responseBean.setStatusCode(STATUS_INTIALIZE_FIELD);
			responseBean.setMessage(MESSAGE_INTIALIZE_FIELD);
			String strResponseBean=MayurServiceCommonUtility.getJson(responseBean);
			logResponse(reqId, method, strResponseBean);
			return Response.status(Status.OK).entity(strResponseBean).type(MediaType.APPLICATION_JSON).build();
						
		}
		if(e instanceof CannotGetJdbcConnectionException ){
			responseBean.setStatusCode(STATUS_SERVICE_UNAVAILABLE);
			responseBean.setMessage(MESSAGE_SERVICE_UNAVAILABLE);
			String strResponseBean=MayurServiceCommonUtility.getJson(responseBean);
			logResponse(reqId, method, strResponseBean);
			return Response.status(Status.OK).entity(strResponseBean).type(MediaType.APPLICATION_JSON).build();
			
		}
		if(e instanceof RecoverableDataAccessException){
			responseBean.setStatusCode(STATUS_SERVICE_UNAVAILABLE);
			responseBean.setMessage(MESSAGE_SERVICE_UNAVAILABLE);
			String strResponseBean=MayurServiceCommonUtility.getJson(responseBean);
			logResponse(reqId, method, strResponseBean);
			return Response.status(Status.OK).entity(strResponseBean).type(MediaType.APPLICATION_JSON).build();
		}
		if(e instanceof TimeOutException){
			responseBean.setStatusCode(STATUS_SERVICE_TIME_OUT);
			responseBean.setMessage(MESSAGE_SERVICE_TIME_OUT);
			String strResponseBean=MayurServiceCommonUtility.getJson(responseBean);
			logResponse(reqId, method, strResponseBean);
			return Response.status(Status.OK).entity(strResponseBean).type(MediaType.APPLICATION_JSON).build();
			
		}
		if(e instanceof AccessDeniedException){
			responseBean.setStatusCode(STATUS_ACCESS_DENIED);
			responseBean.setMessage(MESSAGE_ACCESS_DENIY);
			String strResponseBean=MayurServiceCommonUtility.getJson(responseBean);
			logResponse(reqId, method, strResponseBean);
			return Response.status(Status.OK).entity(strResponseBean).type(MediaType.APPLICATION_JSON).build();
		}
		if(e instanceof ExistDataException){
			responseBean.setStatusCode(STATUS_EXIST_DATA);
			responseBean.setMessage(MESSAGE_EXIST_DATA);
			String strResponseBean=MayurServiceCommonUtility.getJson(responseBean);
			logResponse(reqId, method, strResponseBean);
			return Response.status(Status.OK).entity(strResponseBean).type(MediaType.APPLICATION_JSON).build();

		}
		logger.error(MayurServiceCommonUtility.concatinateStrings(reqId,DELIMETER_LOG_FIELD,K_RESPONSE,DELIMETER_LOG_FIELD,e.getMessage()), e);
		responseBean.setStatusCode(STATUS_INTERNAL_EROR);
		String strResponseBean = MayurServiceCommonUtility.getJson(responseBean);
		logResponse(reqId, method, strResponseBean);//edit
//		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(strResponseBean).type(MediaType.APPLICATION_JSON).build();
		return Response.status(Status.OK).entity(strResponseBean).type(MediaType.APPLICATION_JSON).build();
		
	}
	
	/*this module send an request id for every web service request*/
	protected String getRequestId(HttpServletRequest request){
		return (String) request.getAttribute(K_API_REQUEST_ID);
	}
	/*this module use to write log file whenever web service receive an request*/
	protected void logRequest(String reqId, String method, String headers, String url, String requestEntity){
		logger.info(MayurServiceCommonUtility.concatinateStrings(DELIMETER_LOG_FIELD,
												reqId, DELIMETER_LOG_FIELD,
												method, DELIMETER_LOG_FIELD,
												K_REQUEST, DELIMETER_LOG_FIELD, 
												headers, DELIMETER_LOG_FIELD,
												url, DELIMETER_LOG_FIELD,
												requestEntity));
	}
	/*this module use to write log file whenever web service send an response*/
	protected void logResponse(String reqId, String method, String response){
		logger.info(MayurServiceCommonUtility.concatinateStrings(DELIMETER_LOG_FIELD,
												reqId,DELIMETER_LOG_FIELD,
												method, DELIMETER_LOG_FIELD,
												K_RESPONSE,DELIMETER_LOG_FIELD,
												"", DELIMETER_LOG_FIELD,
												"", DELIMETER_LOG_FIELD,
												response));
	}

}
