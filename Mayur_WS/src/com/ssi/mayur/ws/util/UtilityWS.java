package com.ssi.mayur.ws.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;

public class UtilityWS {
	//convert inputstream into string format
	public static String getStringFromInputStream(InputStream is) throws IOException {		 
		StringWriter writer = new StringWriter();
		IOUtils.copy(is, writer, "UTF-8");
		
		return writer.toString(); 
	}
}
