package com.ssi.mayur.ws.util;

import java.util.ArrayList;
import java.util.List;

import com.ssi.mayur.model.vo.CartVO;
import com.ssi.mayur.model.vo.MasterProductVO;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductOrderVO;

public class Population {
	
	/*keep multiple product image into respective product  */
    public List<ProductDetailVO> pupulateProductList(List<ProductDetailVO> productlist){
		
		List<String>productIdList=new ArrayList<String>();
		List<ProductDetailVO>productList=new ArrayList<ProductDetailVO>();
		for (int i = 0; i < productlist.size(); i++) {
			     if(productList.isEmpty()){
				       productList.add(productlist.get(i));
				       productIdList.add(productlist.get(i).getSysId());
			     }
			     else{
				       int actualProductListSize=productList.size();				 
				       for (int j = 0; j < actualProductListSize; j++) {
					            if(productList.get(j).getSysId().equals(productlist.get(i).getSysId())){							
						              if(!productList.get(j).getProductImageList().contains(productlist.get(i).getProductImageList().get(0))){
							                 productList.get(j).getProductImageList().add(productlist.get(i).getProductImageList().get(0));
						              }
                                 }
					             else
					             {
						              if(!productIdList.contains(productlist.get(i).getSysId())){
							                 productList.add(productlist.get(i));
							                 productIdList.add(productlist.get(i).getSysId());
							                 actualProductListSize=productList.size();
						              }						
					              }					
				       }				
			    }			
		 }
		return productList;				
	}

    /*keep same imei number products into single cart  */
    public CartVO populateCartList(List<CartVO> cartList){

    	CartVO cart=new CartVO();
    	List<String>ImageList=new ArrayList<String>(); 
	    List<String>productIdList=new ArrayList<String>();   
	    for (int i = 0; i < cartList.size(); i++) {
	    	
			     if(cart.getSysId()==null){
			         		cart.setImeiNumber(cartList.get(i).getImeiNumber());
			         		cart.setListCartItem(cartList.get(i).getListCartItem());
			         		cart.setSysId(cartList.get(i).getSysId());
			         		productIdList.add(cartList.get(i).getListCartItem().get(0).getSysId());
			         		ImageList.add(cartList.get(i).getListCartItem().get(0).getProductImageList().get(0).getSysId());
			     }
			     else{		
			    	        int cartItemListSize=cart.getListCartItem().size();
			    	        for (int j = 0; j < cartItemListSize; j++) {	
			    	        	if(!productIdList.contains(cartList.get(i).getListCartItem().get(0).getSysId())){
			    	        		cart.getListCartItem().add(cartList.get(i).getListCartItem().get(0));
			    	        		cartItemListSize=cart.getListCartItem().size();
			    	        		productIdList.add(cartList.get(i).getListCartItem().get(0).getSysId());
			    	        		ImageList.add(cartList.get(i).getListCartItem().get(0).getProductImageList().get(0).getSysId());
			    	        	}
			    	        	else{
			    	        		
			    	        		if(!ImageList.contains(cartList.get(i).getListCartItem().get(0).getProductImageList().get(0).getSysId())){
			    	        			 cart.getListCartItem().get(j).getProductImageList().add(cartList.get(i).getListCartItem().get(0).getProductImageList().get(0));
			    	        		}

			    	        	}			    	        	
								
							}
			         		 
			    }
		 }

	  return cart;
 }
    
    
 //Populate the order object
    
    public List<ProductOrderVO> populateProductOrderList(List<ProductOrderVO> orderList){
		
    	List<String>orderIdList=new ArrayList<String>();
		List<ProductOrderVO>orderlist=new ArrayList<ProductOrderVO>();
		for (int i = 0; i < orderList.size(); i++) {
			     if(orderlist.isEmpty()){
				       orderlist.add(orderList.get(i));
				       orderIdList.add(orderList.get(i).getSysId());
			     }
			     else{
				       int orderlistSize=orderlist.size();				 
				       for (int j = 0; j < orderlistSize; j++) {
					            if(orderlist.get(j).getSysId().equals(orderList.get(i).getSysId())){							
						              if(!orderlist.get(j).getProductList().contains(orderList.get(i).getProductList().get(0))){
							                 orderlist.get(j).getProductList().add(orderList.get(i).getProductList().get(0));
						              }
                                 }
					             else
					             {
						              if(!orderIdList.contains(orderList.get(i).getSysId())){
							                 orderlist.add(orderList.get(i));
							                 orderIdList.add(orderList.get(i).getSysId());
							                 orderlistSize=orderlist.size();
						              }						
					              }					
				       }				
			    }			
		 }
    	return orderlist;    	
    }
    


}