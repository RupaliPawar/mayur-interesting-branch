package com.ssi.mayur.ws.vo;

import com.ssi.mayur.utility.MayurServiceCommonUtility;
// Web service response vo
public class ResponseVO {
	private Integer statusCode;
	private String message = "";
	private String requestId = "";
	private String data = "";
	
	public ResponseVO(String data){
		this.data = data;
	}
	
	public ResponseVO(int status, String data){
		statusCode = status;
		this.data = data;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	public String getAsJSON(){
		return MayurServiceCommonUtility.getJson(this);
	}
	
}
