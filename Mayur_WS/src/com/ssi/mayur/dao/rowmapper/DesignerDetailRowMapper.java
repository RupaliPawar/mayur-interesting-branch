package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.DesignerExpertVO;
import com.ssi.mayur.model.vo.WorkVO;

public class DesignerDetailRowMapper implements RowMapper<DesignerExpertVO> {
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public DesignerExpertVO mapRow(ResultSet rs, int arg1)throws SQLException {
		
		List<WorkVO>workimagelist=new ArrayList<WorkVO>();
		DesignerExpertVO designer=new DesignerExpertVO();
		WorkVO workvo=new WorkVO();
		designer.setSysId(DAOBase.getStringColumnValue(rs, "DesignerID"));
		designer.setName(DAOBase.getStringColumnValue(rs, "DesignerName"));
		designer.setPlace(DAOBase.getStringColumnValue(rs, "DesignerPlace"));	
		designer.setRating(DAOBase.getDoubleColumnValue(rs, "DesignerRating"));
		workvo.setDesignerId(DAOBase.getStringColumnValue(rs, "DesignerID"));
		workvo.setWorkimage(DAOBase.getStringColumnValue(rs, "DesignerWorkImage"));
		workimagelist.add(workvo);
		designer.setWorkImage(workimagelist);
		return designer;
	}

}
