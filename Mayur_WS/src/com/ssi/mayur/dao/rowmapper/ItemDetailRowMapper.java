package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductImageVO;

public class ItemDetailRowMapper implements RowMapper<ProductDetailVO>{
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public ProductDetailVO mapRow(ResultSet rs, int arg1) throws SQLException {
		ProductDetailVO productDetail=new ProductDetailVO();
		productDetail.setSysId(DAOBase.getStringColumnValue(rs, "ProductID"));
		productDetail.setDescription(DAOBase.getStringColumnValue(rs,"Description"));				
		productDetail.setProductName(DAOBase.getStringColumnValue(rs,"ProductName"));	
		productDetail.setColor(DAOBase.getStringColumnValue(rs, "Color"));
		productDetail.setPrice(rs.getDouble("Price"));
		productDetail.setStock(DAOBase.getIntColumnValue(rs, "Stock"));
		productDetail.setIsVisibleProduct(DAOBase.getBooleanColumnValue(rs, "ProductVisible"));
		productDetail.setSysId(DAOBase.getStringColumnValue(rs, "ProductID"));		
		ProductImageVO image=new ProductImageVO();
		image.setSysId(DAOBase.getStringColumnValue(rs, "ImageID"));
		image.setProductImage(DAOBase.getStringColumnValue(rs, "Image"));
		List<ProductImageVO>productImageList=new ArrayList<ProductImageVO>();
		productImageList.add(image);
		productDetail.setProductImageList(productImageList);
		return productDetail;
	}

}
