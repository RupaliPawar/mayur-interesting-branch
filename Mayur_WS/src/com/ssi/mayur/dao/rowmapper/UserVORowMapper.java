package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.UserVO;

public class UserVORowMapper implements RowMapper<UserVO> {
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public UserVO mapRow(ResultSet rs, int arg1) throws SQLException {		
		UserVO user=new UserVO();
		user.setSysId(DAOBase.getStringColumnValue(rs,"user_id"));
		user.setEmailId(DAOBase.getStringColumnValue(rs,"email_id"));
		user.setUsername(DAOBase.getStringColumnValue(rs,"user_name"));
		user.setPassword(DAOBase.getStringColumnValue(rs,"password"));
		user.setCreatedOn(DAOBase.getStringColumnValue(rs, "created_on"));
		return user;
	}

}
