package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.CartItemVo;
import com.ssi.mayur.model.vo.CartVO;
import com.ssi.mayur.model.vo.ProductImageVO;

public class CartDetailRowMapper implements RowMapper<CartVO> {
/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public CartVO mapRow(ResultSet rs, int arg1) throws SQLException {
		CartVO cart=new CartVO();
		cart.setImeiNumber(DAOBase.getStringColumnValue(rs, "IMEI"));
		cart.setSysId(DAOBase.getStringColumnValue(rs, "CartID"));	
	
		ProductImageVO image=new ProductImageVO();
		image.setSysId(DAOBase.getStringColumnValue(rs, "ImageID"));
		image.setProductImage(DAOBase.getStringColumnValue(rs, "Image"));
		image.setProductId(DAOBase.getStringColumnValue(rs, "ProductID"));		
		List<ProductImageVO>productImageList=new ArrayList<ProductImageVO>();
		productImageList.add(image);		

		List<CartItemVo> listCartItem = new ArrayList<CartItemVo>(); 
		CartItemVo cartItem=new CartItemVo();
		cartItem.setQuantity(DAOBase.getIntColumnValue(rs, "Quantity"));
		cartItem.setSysId(DAOBase.getStringColumnValue(rs, "ProductID"));//Product ID
		cartItem.setProductName(DAOBase.getStringColumnValue(rs, "ProductName"));
		cartItem.setPrice(DAOBase.getDoubleColumnValue(rs, "Price"));
		cartItem.setIsVisibleProduct(DAOBase.getBooleanColumnValue(rs, "ProductVisible"));
		cartItem.setProductImageList(productImageList);		
		listCartItem.add(cartItem);		
		cart.setListCartItem(listCartItem);
		return cart;
	}
}
