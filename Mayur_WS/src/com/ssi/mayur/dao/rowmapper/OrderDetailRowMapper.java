package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductOrderVO;
import com.ssi.mayur.model.vo.UserVO;

public class OrderDetailRowMapper implements RowMapper<ProductOrderVO> {

	@Override
	public ProductOrderVO mapRow(ResultSet rs, int arg1) throws SQLException {
		// Row mapper retrieve all order details
		
		ProductOrderVO order=new ProductOrderVO();
		List<ProductDetailVO>productList=new ArrayList<ProductDetailVO>();
		ProductDetailVO product=new ProductDetailVO();
		order.setSysId(DAOBase.getStringColumnValue(rs, "order_id"));		
		order.setOrderDate(DAOBase.getStringColumnValue(rs, "order_date"));
		order.setOrderStatus(DAOBase.getStringColumnValue(rs, "order_status"));
		order.setTransactionID(DAOBase.getStringColumnValue(rs, "transaction_id"));
	
		product.setSysId(DAOBase.getStringColumnValue(rs, "product_id"));
		product.setProductName(DAOBase.getStringColumnValue(rs, "product_name"));
		product.setOrderedQuantity(DAOBase.getIntColumnValue(rs, "quantity"));
		product.setPrice(DAOBase.getDoubleColumnValue(rs, "product_amount"));
	
		productList.add(product);
		
		UserVO user=new UserVO();		
		user.setSysId(DAOBase.getStringColumnValue(rs, "user_id"));
		user.setEmailId(DAOBase.getStringColumnValue(rs, "email_id"));
		user.setAddress(DAOBase.getStringColumnValue(rs, "address"));
		user.setPincode(DAOBase.getStringColumnValue(rs, "pincode"));
		user.setCity(DAOBase.getStringColumnValue(rs, "city"));
		user.setState(DAOBase.getStringColumnValue(rs, "state"));
		user.setFname(DAOBase.getStringColumnValue(rs, "fname"));		
		user.setMobileNo(DAOBase.getStringColumnValue(rs, "mobileNo"));		
		user.setLandLineNo(DAOBase.getStringColumnValue(rs, "landLineNo"));
		
    	order.setUser(user);
		order.setProductList(productList);
				
		return order;
	}

}
