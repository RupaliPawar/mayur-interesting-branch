package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.UserVO;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIConversion.User;

public class ExistingUserRowMapper implements RowMapper<UserVO>{

	@Override
	public UserVO mapRow(ResultSet rs, int arg1) throws SQLException {
		
		UserVO user=new UserVO();
		user.setSysId(DAOBase.getStringColumnValue(rs, "user_id"));
		user.setAddress(DAOBase.getStringColumnValue(rs, "address"));	
		user.setCity(DAOBase.getStringColumnValue(rs, "city"));
		user.setEmailId(DAOBase.getStringColumnValue(rs, "email_id"));
		user.setFname(DAOBase.getStringColumnValue(rs, "fname"));
		user.setLandLineNo(DAOBase.getStringColumnValue(rs, "landLineNo"));
		user.setMobileNo(DAOBase.getStringColumnValue(rs, "mobileNo"));
		user.setPassword(DAOBase.getStringColumnValue(rs, "password"));
		user.setPincode(DAOBase.getStringColumnValue(rs, "pincode"));
		user.setState(DAOBase.getStringColumnValue(rs, "state"));		
		user.setRole(DAOBase.getStringColumnValue(rs, "role"));
		user.setVerifiedUser(DAOBase.getBooleanColumnValue(rs, "isVerifiedUser"));
		user.setCreatedOn(DAOBase.getStringColumnValue(rs, "created_on"));
		user.setUsername(DAOBase.getStringColumnValue(rs, "user_name"));
		
		
		return user;
	}

}
