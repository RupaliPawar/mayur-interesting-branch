package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.MasterProductVO;

public class ProductMasterRowMapper implements RowMapper<MasterProductVO>{
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public MasterProductVO mapRow(ResultSet rs, int arg1) throws SQLException {
		MasterProductVO masterProduct=new MasterProductVO();
		masterProduct.setSysId(DAOBase.getStringColumnValue(rs, "master_product_id"));
		masterProduct.setMasterProductName(DAOBase.getStringColumnValue(rs, "master_product_type"));
		masterProduct.setVisibleMasterProduct(DAOBase.getBooleanColumnValue(rs, "is_visible"));
		masterProduct.setMasterProductImage(DAOBase.getStringColumnValue(rs, "bg_image"));	
		return masterProduct;
	}

}
