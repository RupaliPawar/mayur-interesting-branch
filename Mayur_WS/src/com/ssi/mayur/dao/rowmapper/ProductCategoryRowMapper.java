package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.ProductCategoryVO;
import com.ssi.mayur.model.vo.MasterProductVO;
import com.sun.org.apache.bcel.internal.generic.GETSTATIC;

public class ProductCategoryRowMapper  implements RowMapper<ProductCategoryVO>{
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public ProductCategoryVO mapRow(ResultSet rs, int arg1)throws SQLException {					
		ProductCategoryVO productCategory=new ProductCategoryVO();
		productCategory.setSysId(DAOBase.getStringColumnValue(rs, "ProductCategoryID"));
		productCategory.setProductCategoryName(DAOBase.getStringColumnValue(rs, "ProductCategoryName"));
		productCategory.setProductCategoryImage(DAOBase.getStringColumnValue(rs, "ProductCategoryIcon"));
		productCategory.setMasterProductId(DAOBase.getStringColumnValue(rs, "MasterProduct"));
		productCategory.setIsVisibleProductCategory(DAOBase.getBooleanColumnValue(rs, "ProductCategoryVisible"));		
		return productCategory;
	}

}
