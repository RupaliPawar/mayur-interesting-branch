package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductImageVO;


public class ProductListRowMapper implements RowMapper<ProductDetailVO> {
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public ProductDetailVO mapRow(ResultSet rs, int arg1) throws SQLException {
		ProductDetailVO productListvo=new ProductDetailVO();
		productListvo.setSysId(DAOBase.getStringColumnValue(rs, "ProductID"));
		productListvo.setProductName(DAOBase.getStringColumnValue(rs, "ProductName"));
		productListvo.setPrice(DAOBase.getDoubleColumnValue(rs, "Price"));
		productListvo.setIsVisibleProduct(DAOBase.getBooleanColumnValue(rs, "ProductVisible"));		
		ProductImageVO image=new ProductImageVO();
		image.setSysId(DAOBase.getStringColumnValue(rs, "ImageID"));
		image.setProductImage(DAOBase.getStringColumnValue(rs, "Image"));
		image.setProductId(DAOBase.getStringColumnValue(rs, "ProductID"));		
		List<ProductImageVO>productImageList=new ArrayList<ProductImageVO>();
		productImageList.add(image);
		productListvo.setProductImageList(productImageList);		
		return productListvo;
	}

}
