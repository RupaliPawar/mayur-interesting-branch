package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.CartItemVo;
import com.ssi.mayur.model.vo.CartVO;

public class CartDetailWithSpecifiedImeiRowMapper implements RowMapper<CartVO>{
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public CartVO mapRow(ResultSet rs, int arg1) throws SQLException {
		CartVO cart=new CartVO();		
		cart.setImeiNumber(DAOBase.getStringColumnValue(rs, "imeino"));			
		cart.setSysId(DAOBase.getStringColumnValue(rs, "cartId"));	
		return cart;		
	}	  
 }
