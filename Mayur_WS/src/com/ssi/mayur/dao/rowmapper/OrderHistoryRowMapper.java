package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductOrderVO;

public class OrderHistoryRowMapper implements RowMapper<ProductOrderVO>{
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public ProductOrderVO mapRow(ResultSet rs, int arg1) throws SQLException {
		ProductOrderVO productOrder=new ProductOrderVO();
		ProductDetailVO productDetail=new ProductDetailVO();
		List<ProductDetailVO>product=new ArrayList<ProductDetailVO>();		
		productOrder.setOrderDate(DAOBase.getStringColumnValue(rs, "orderDate"));
		productDetail.setProductName(DAOBase.getStringColumnValue(rs, "productName"));
		productDetail.setPrice(DAOBase.getDoubleColumnValue(rs, "price"));		
		product.add(productDetail);		
//		productOrder.setProductList(product);						
		return productOrder;
	}

}
