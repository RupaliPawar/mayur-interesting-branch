package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductImageVO;

public class ProductDetailRowMapper implements RowMapper<ProductDetailVO>{
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public ProductDetailVO mapRow(ResultSet rs, int arg1) throws SQLException {
		
		ProductDetailVO product=new ProductDetailVO();
		product.setColor(DAOBase.getStringColumnValue(rs, "Color"));
		product.setDescription(DAOBase.getStringColumnValue(rs, "Description"));
		product.setPrice(DAOBase.getDoubleColumnValue(rs, "Price"));
		product.setProductCategoryId(DAOBase.getStringColumnValue(rs, "ProductCategory"));
		product.setProductName(DAOBase.getStringColumnValue(rs, "ProductName"));
		product.setSysId(DAOBase.getStringColumnValue(rs, "ProductID"));
		product.setStock(DAOBase.getIntColumnValue(rs, "Stock"));
	
		ProductImageVO image=new ProductImageVO();
		image.setSysId(DAOBase.getStringColumnValue(rs, "ImageID"));
		image.setProductImage(DAOBase.getStringColumnValue(rs, "Image"));
		image.setProductId(DAOBase.getStringColumnValue(rs, "ProductID"));
		
		List<ProductImageVO>productImageList=new ArrayList<ProductImageVO>();
		productImageList.add(image);
		product.setProductImageList(productImageList);
				
		return product;
	}
	  
 }
 
