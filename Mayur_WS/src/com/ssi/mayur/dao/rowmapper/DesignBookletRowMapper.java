package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.DesignBookletVO;

public class DesignBookletRowMapper implements RowMapper<DesignBookletVO>{
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public DesignBookletVO mapRow(ResultSet rs, int arg1) throws SQLException {
		DesignBookletVO booklet=new DesignBookletVO();
		booklet.setSysId(DAOBase.getStringColumnValue(rs, "designbooklet_id"));
		booklet.setImeiNumber(DAOBase.getStringColumnValue(rs, "imei_no"));
		booklet.setProductId(DAOBase.getStringColumnValue(rs, "product_id"));		
		return booklet;
	}
	  
  }
