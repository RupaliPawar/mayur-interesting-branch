package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.ProductCategoryVO;

public class ProductRowMapper implements RowMapper<ProductCategoryVO>{
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public ProductCategoryVO mapRow(ResultSet rs, int arg1)throws SQLException {
		ProductCategoryVO productCategory=new ProductCategoryVO();
		productCategory.setSysId(DAOBase.getStringColumnValue(rs, "product_category_id"));
		productCategory.setProductCategoryImage(DAOBase.getStringColumnValue(rs, "product_icon"));
		productCategory.setMasterProductId(DAOBase.getStringColumnValue(rs, "master_product_id"));
		productCategory.setProductCategoryName(DAOBase.getStringColumnValue(rs, "product_category_name"));
		productCategory.setIsVisibleProductCategory(DAOBase.getBooleanColumnValue(rs, "is_visible_productcategory"));			
		return productCategory;
	}
	
}

