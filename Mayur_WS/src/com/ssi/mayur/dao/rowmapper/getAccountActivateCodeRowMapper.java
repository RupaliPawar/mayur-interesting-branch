package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.UserVO;

public class getAccountActivateCodeRowMapper implements RowMapper<UserVO>{

	@Override
	public UserVO mapRow(ResultSet rs, int arg1) throws SQLException {
		UserVO user=new UserVO();
		user.setActivateAccountCode(DAOBase.getStringColumnValue(rs, "activate_code"));
		user.setSysId(DAOBase.getStringColumnValue(rs, "user_id"));
		user.setEmailId(DAOBase.getStringColumnValue(rs, "email_id"));		
		return user;
	}

}
