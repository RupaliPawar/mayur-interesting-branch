package com.ssi.mayur.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.model.vo.CustomerReviewVO;
import com.ssi.mayur.model.vo.DesignerExpertVO;
import com.ssi.mayur.model.vo.WorkVO;

public class DesignerWorkRowMapper implements RowMapper<DesignerExpertVO>{
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
	@Override
	public DesignerExpertVO mapRow(ResultSet rs, int arg1) throws SQLException {		
		List<WorkVO>workimagelist=new ArrayList<WorkVO>();
		List<CustomerReviewVO>customerReviewList=new ArrayList<CustomerReviewVO>();
		DesignerExpertVO designer=new DesignerExpertVO();
		WorkVO workvo=new WorkVO();		
		CustomerReviewVO custreviewvo=new CustomerReviewVO();
		designer.setPersonImageUrl(DAOBase.getStringColumnValue(rs,"designer_profile_image"));
		designer.setContactNo(DAOBase.getStringColumnValue(rs, "contactno"));
		designer.setEmailId(DAOBase.getStringColumnValue(rs, "email_id"));
		designer.setDetailOFDesignFirm(DAOBase.getStringColumnValue(rs, "detail_designer"));
		workvo.setSysId(DAOBase.getStringColumnValue(rs, "work_id"));
		workvo.setWorkimage(DAOBase.getStringColumnValue(rs, "work_image"));
		workvo.setWorkDescription(DAOBase.getStringColumnValue(rs,"work_description"));
		workimagelist.add(workvo);
		custreviewvo.setSysId(DAOBase.getStringColumnValue(rs, "review_id"));
		
		custreviewvo.setCustomerName(DAOBase.getStringColumnValue(rs, "fname"));
		custreviewvo.setCustomerRating(DAOBase.getDoubleColumnValue(rs, "customer_rating"));
		custreviewvo.setCustomerReviewDescription(DAOBase.getStringColumnValue(rs, "review_description"));
		custreviewvo.setReviewDate(DAOBase.getStringColumnValue(rs, "review_date"));	
		customerReviewList.add(custreviewvo);
		designer.setWorkImage(workimagelist);
		designer.setCustReview(customerReviewList);
		return designer;		
	}
}
