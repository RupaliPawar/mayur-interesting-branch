package com.ssi.mayur.dao.mysql;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.dao.IGeneralDAO;
import com.ssi.mayur.dao.rowmapper.CartDetailRowMapper;
import com.ssi.mayur.dao.rowmapper.CartDetailWithSpecifiedImeiRowMapper;
import com.ssi.mayur.dao.rowmapper.DesignBookletRowMapper;
import com.ssi.mayur.dao.rowmapper.ExistingUserRowMapper;
import com.ssi.mayur.dao.rowmapper.ItemDetailRowMapper;
import com.ssi.mayur.dao.rowmapper.OrderDetailRowMapper;
import com.ssi.mayur.dao.rowmapper.OrderHistoryRowMapper;
import com.ssi.mayur.dao.rowmapper.ProductCategoryRowMapper;
import com.ssi.mayur.dao.rowmapper.ProductDetailRowMapper;
import com.ssi.mayur.dao.rowmapper.ProductListRowMapper;
import com.ssi.mayur.dao.rowmapper.ProductMasterRowMapper;
import com.ssi.mayur.dao.rowmapper.ProductRowMapper;
import com.ssi.mayur.exception.AccessDeniedException;
import com.ssi.mayur.exception.InsufficientDataException;
import com.ssi.mayur.exception.InvalidInputsException;
import com.ssi.mayur.model.util.UtilityModel;
import com.ssi.mayur.model.vo.CartItemVo;
import com.ssi.mayur.model.vo.CartVO;
import com.ssi.mayur.model.vo.DesignBookletVO;
import com.ssi.mayur.model.vo.ProductCategoryVO;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductImageVO;
import com.ssi.mayur.model.vo.ProductOrderVO;
import com.ssi.mayur.model.vo.MasterProductVO;
import com.ssi.mayur.model.vo.UserVO;
import com.ssi.mayur.utility.IMayurServiceUtilityConstants;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.utility.UtilityEmail;
import com.ssi.mayur.ws.util.Population;

public class GeneralDAOImpl extends DAOBase implements IGeneralDAO,IMayurServiceUtilityConstants,Serializable{
	DataSource dataSource = null;
	
	private static Logger logger = LoggerFactory.getLogger(GeneralDAOImpl.class);	
	
	@Value("${app.emailid}")
	private  String emailID;
	@Value("${app.email.password}")
	private String password;
	
 /*This module use to create a master category and store into the database*/
	@Override
	public void createMasterProductCategory(MasterProductVO masterProduct) throws Exception {
//		TransactionStatus status=null;	
		try{						 
	         String sql="insert into mst_product_category_type(master_product_id,master_product_type,is_visible,bg_image) values(?,?,?,?)";
		     ArrayList<Object>paramList=new ArrayList<Object>();
		     if(masterProduct.getMasterProductImage()!=null){
		        paramList.add(masterProduct.getSysId());
		        paramList.add(masterProduct.getMasterProductName());
		        paramList.add(masterProduct.isVisibleMasterProduct());
		        paramList.add(masterProduct.getMasterProductImage());
		        executeDbUpdates(sql, paramList);               		        
		   }
		   else{			    
			    paramList.add(masterProduct.getSysId());
		        paramList.add(masterProduct.getMasterProductName());
		        paramList.add(masterProduct.isVisibleMasterProduct());
		        paramList.add("");
		        executeDbUpdates(sql, paramList);
		   }

		}		
		catch(Exception e){
			throw e;
		}
	}
	 /*This module use to modify or update a master category and store into the database*/
	@Override
	public void modifyMasterProductCategory(MasterProductVO masterProduct) throws Exception {
		try{
			 List<String>masterProductIdList=getMasterProductCategoryId();
	            if(!masterProductIdList.contains(masterProduct.getSysId())){
	            	throw new InvalidInputsException("Given master product category ID is invalid");
	            }

	       String sql="update mst_product_category_type set master_product_type=?,is_visible=?,bg_image=? where master_product_id=?";
	       String sqlUpdate="update mst_product_category_type set master_product_type=?,is_visible=? where master_product_id=?";
		   ArrayList<Object>paramList=new ArrayList<Object>();
		   if(masterProduct.getMasterProductImage()!=null){		        
		        paramList.add(masterProduct.getMasterProductName());
		        paramList.add(masterProduct.isVisibleMasterProduct());
		        paramList.add(masterProduct.getMasterProductImage());
		        paramList.add(masterProduct.getSysId());
		        executeDbUpdates(sql, paramList);
		   }
		   else{			    
		        paramList.add(masterProduct.getMasterProductName());
		        paramList.add(masterProduct.isVisibleMasterProduct());
		        paramList.add(masterProduct.getSysId());
		        executeDbUpdates(sqlUpdate, paramList);
		   }
		}		
		catch(Exception e){
			   throw e;
		}		
	}
	 /*This module use to retrieve a master category records from the database*/
	@Override
	public List<MasterProductVO> getMasterProductCategory() throws Exception {
		String sql="select * from mst_product_category_type";
		List<MasterProductVO>masterProductList=selectDb1(sql,new ProductMasterRowMapper());
		if(masterProductList.isEmpty()){
			throw new InsufficientDataException("No data into the database");
		}
		return masterProductList;
	}
	
	 /*This module use to create a product category and store into the database*/
	@Override
	public void createProductCategory(ProductCategoryVO productCategory)throws Exception {
		try{			
            List<String>masterProductIdList=getMasterProductCategoryId();
            if(!masterProductIdList.contains(productCategory.getMasterProductId())){
            	throw new InvalidInputsException("Given master product category ID is invalid");
            }
			
		String sql="INSERT INTO product_category(product_category_id,product_category_name,product_icon,master_product_id,is_visible_productcategory) VALUES(?,?,?,?,?);";		
		ArrayList<Object>paramList=new ArrayList<Object>();		
		    if(productCategory.getProductCategoryImage()!=null){
		    	paramList.add(productCategory.getSysId());
				paramList.add(productCategory.getProductCategoryName());
				paramList.add(productCategory.getProductCategoryImage());
				paramList.add(productCategory.getMasterProductId());
				paramList.add(productCategory.getIsVisibleProductCategory());
	         	executeDbUpdates(sql, paramList);	
		    }
		   else{
			    paramList.add(productCategory.getSysId());
				paramList.add(productCategory.getProductCategoryName());
				paramList.add("");
				paramList.add(productCategory.getMasterProductId());
				paramList.add(productCategory.getIsVisibleProductCategory());
	            executeDbUpdates(sql, paramList);		
		   }
		}		
		catch(Exception e){
			throw e;
		}
		
	}
	 /*This module use to modify a product category and store into the database*/
	@Override
	public void modifyProductCategory(ProductCategoryVO productCategory)throws Exception {

		try{
			 List<String>masterProductIdList=getMasterProductCategoryId();
	            if(!masterProductIdList.contains(productCategory.getMasterProductId())){
	            	throw new InvalidInputsException("Given master product category ID is invalid");
	            }
	         List<String>productCategoryList=getProductCategoryId();
	         if(!productCategoryList.contains(productCategory.getSysId())){
	        	 throw new InvalidInputsException("Given product category ID is invalid"); 
	         }

		if(productCategory!=null){
		   String sql="UPDATE product_category SET product_category_name=?,product_icon=? ,is_visible_productcategory=? WHERE product_category_id=? and master_product_id=?";	
		   String sqlStmt="UPDATE product_category SET product_category_name=?,is_visible_productcategory=? WHERE product_category_id=? and master_product_id=? ;";
		   ArrayList<Object>paramlist=new ArrayList<Object>();		   
		   if(productCategory.getProductCategoryImage()!=null){		   
			   paramlist.add(productCategory.getProductCategoryName());
			   paramlist.add(productCategory.getProductCategoryImage());		   
			   paramlist.add(productCategory.getIsVisibleProductCategory());
			   paramlist.add(productCategory.getSysId());
			   paramlist.add(productCategory.getMasterProductId());
			   executeDbUpdates(sql, paramlist);  
		   }
		   else{
			   paramlist.add(productCategory.getProductCategoryName());				   
			   paramlist.add(productCategory.getIsVisibleProductCategory());
			   paramlist.add(productCategory.getSysId());
			   paramlist.add(productCategory.getMasterProductId());
			   executeDbUpdates(sqlStmt, paramlist);		
		   }
		   }
		}
		catch(Exception e){
			throw e;
		}
		
	}
   
	 /*This module use to retrieve a master category from the database*/
	@Override
	public List<ProductCategoryVO> getProductCategory() throws Exception {
     
		String sql="SELECT * FROM product_category";
		List<ProductCategoryVO>productList=selectDb1(sql,new ProductRowMapper());
		if(productList.isEmpty()){
			throw new InsufficientDataException("No data into the database");
		}
		return productList;
        }     
	
	
	 /*This module use to create a product and store into the database*/
	@Override
	
	public void createPoductForCategory(ProductDetailVO product)throws Exception {
	 TransactionStatus status=null;		
	 try{
         
		 List<String>productCategoryList=getProductCategoryId();
         if(!productCategoryList.contains(product.getProductCategoryId())){
        	 throw new InvalidInputsException("Given product category ID is invalid"); 
         }
                
         jdbcTemplate.getDataSource().getConnection().setAutoCommit(false);                 
         //  start transaction 
		 DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();			 
		 status=dataSourceTransactionManager.getTransaction(paramTransactionDefinition );

		 String sql1="insert into mst_product(product_id,product_category_id,product_name,description,price,color,is_visible_product,stock)values(?,?,?,?,?,?,?,?)";//edit
		 String sql2="insert into product_images(image_id,product_id,image) values(?,?,?)";		
		 ArrayList<Object>paramList=new ArrayList<Object>();		 
		 paramList.add(product.getSysId());
		 paramList.add(product.getProductCategoryId());
		 paramList.add(product.getProductName());		 		
		 if(product.getDescription()!=null){
			 paramList.add(product.getDescription());
		 }
		 else{
			 
			 paramList.add("");
		 }
		 paramList.add(product.getPrice());
		 if(product.getColor()!=null){
			 paramList.add(product.getColor());
		 }
		 else{
			 paramList.add("");
		 }
		 if(product.getIsVisibleProduct()==false){
			
			 paramList.add(product.getIsVisibleProduct());
		 }
		 else{
			 paramList.add(true);
		 }
		 paramList.add(product.getStock());
		 executeDbUpdates(sql1, paramList);
		 for (int j = 0; j < product.getProductImageList().size(); j++) {
			 paramList.clear();
			 paramList.add(product.getProductImageList().get(j).getSysId());
			 paramList.add(product.getSysId());		 
			 paramList.add(product.getProductImageList().get(j).getProductImage());
			 executeDbUpdates(sql2, paramList);
		 }	
//		 commit transaction
	     dataSourceTransactionManager.commit(status);
	     jdbcTemplate.getDataSource().getConnection().setAutoCommit(true);        
	 }
	
    catch(Exception e){
    	 rollbackTransaction(status);
		 throw e;
	 }
	
	}

	 /*This module use to modify a product details and store into the database*/
	@Override
	public void modifyPoductForCategory(ProductDetailVO product)throws Exception {
		TransactionStatus status=null; 
		 try{	
			 
			 List<String>productCategoryList=getProductCategoryId();
	         if(!productCategoryList.contains(product.getProductCategoryId())){
	        	 throw new InvalidInputsException("Given product category ID is invalid"); 
	         }
	         
	         jdbcTemplate.getDataSource().getConnection().setAutoCommit(false);                 
	         //  start transaction 
			 DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();			 
			 status=dataSourceTransactionManager.getTransaction(paramTransactionDefinition );
	 
			 String sql="update mst_product set  product_name=?,description=?,price=?,color=?,is_visible_product=?,stock=? where product_id=? and product_category_id=?";
			 String sqlupdate="update product_images set image=? where image_id=? and product_id=?";
			 ArrayList<Object>paramList=new ArrayList<Object>();
			 paramList.add(product.getProductName());
			 paramList.add(product.getDescription());
			 paramList.add(product.getPrice());
			 paramList.add(product.getColor());
			 paramList.add(product.getIsVisibleProduct());
			 paramList.add(product.getStock());
			 paramList.add(product.getSysId());
			 paramList.add(product.getProductCategoryId());
			 executeDbUpdates(sql, paramList);
			 
			 for (int i = 0; i < product.getProductImageList().size(); i++) {
				 paramList.clear();
				 paramList.add(product.getProductImageList().get(i).getProductImage());
				 paramList.add(product.getProductImageList().get(i).getSysId());
				 paramList.add(product.getSysId());
				 executeDbUpdates(sqlupdate, paramList);
				 
			} 
			//commit operation
			 dataSourceTransactionManager.commit(status);
			 jdbcTemplate.getDataSource().getConnection().setAutoCommit(true);        
		 }	 
	     catch(Exception e){
	    	 //rollback operation 
	    	 rollbackTransaction(status);
	    	 throw e;
			 
		 }
		 
	}
	
	 /*This module use to retrieve product details from  the database*/
	@Override
	public List<ProductDetailVO> getActualProduct() throws Exception {
   	
		StringBuilder sbSQL=new StringBuilder();
		sbSQL.append("select mp.product_id ProductID,mp.product_category_id ProductCategory,mp.product_name ProductName,");
		sbSQL.append("mp.description Description,mp.price Price,mp.color Color,mp.stock Stock,pimg.image_id ImageID,pimg.image Image ");
		sbSQL.append("  from mst_product mp,product_images pimg  ");
		sbSQL.append("  where  mp.product_id=pimg.product_id");
				
		List<ProductDetailVO>productList=selectDb1(sbSQL.toString(), new ProductDetailRowMapper());
		if(productList.isEmpty()){
			throw new InsufficientDataException("No data into the database");
		}
		Population populateObject=new Population();
		List<ProductDetailVO>product=populateObject.pupulateProductList(productList);		
		return product;
        }
   
	 /*This module use to remove product images from  the database*/
	@Override
	public void removeProductImage(ProductDetailVO productImage)throws Exception {
		TransactionStatus status=null; 
	try{
		List<String>imageIdList=getImageId();
		for (int i = 0; i < productImage.getProductImageList().size(); i++) {
			if(!imageIdList.contains(productImage.getProductImageList().get(i).getSysId())){
				throw new InvalidInputsException("Given ImageId doesn't exist");
			}	
		}
				
		jdbcTemplate.getDataSource().getConnection().setAutoCommit(false);                 
        //  start transaction 
		 DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();			 
		 status=dataSourceTransactionManager.getTransaction(paramTransactionDefinition );
		
	   String sqldelete="delete from product_images where image_id=? and product_id=?;";
	   ArrayList<Object>paramList=new ArrayList<Object>();
	   for (int i = 0; i < productImage.getProductImageList().size(); i++) {
		        paramList.clear();
		        paramList.add(productImage.getProductImageList().get(i).getSysId());
		        paramList.add(productImage.getProductImageList().get(i).getProductId());
		        executeDbUpdates(sqldelete, paramList);
	}
	   
//		 commit transaction
	     dataSourceTransactionManager.commit(status);
	     jdbcTemplate.getDataSource().getConnection().setAutoCommit(true);        
	   
	}
   
	catch(Exception e){
		 rollbackTransaction(status);
		 throw e;
	}
	}
	
	 /*This module use to retrieve product image id from  the database for validate purpose*/
	public List<String>getImageId()throws Exception{    
		List<String>imageIDList=new ArrayList<String>();
		String sql="select image_id from product_images";
		List<ProductImageVO>imageIdList=selectDb1(sql, new ProductImageIDRowMapper());
		if(imageIdList.isEmpty()){
			throw new InsufficientDataException("No data into the database");
		}
		if(imageIdList!=null && imageIdList.size()>0){
			for (int i = 0; i < imageIdList.size(); i++) {
				
				imageIDList.add(imageIdList.get(i).getSysId());
			}			
		}		
		return imageIDList;      		
	}
	
	 /*This module use to retrieve product category details for particular mastercategory from  the database for mobile application*/
	@Override
	public List<ProductCategoryVO> getCategories(String masterProductId)throws Exception {
		
		StringBuilder sbSQL=new StringBuilder();
		sbSQL.append("select pc.product_category_id ProductCategoryID,pc.product_category_name ProductCategoryName,");
		sbSQL.append("pc.product_icon ProductCategoryIcon,pc.master_product_id MasterProduct,");
		sbSQL.append("pc.is_visible_productcategory ProductCategoryVisible ");
		sbSQL.append(" from product_category pc,mst_product_category_type mpc ");
		sbSQL.append(" where pc.master_product_id= mpc.master_product_id and ");
		sbSQL.append(" pc.master_product_id=?");
				
		ArrayList<Object>paramList=new ArrayList<Object>();
		paramList.add(masterProductId);
		List<ProductCategoryVO> productCategoryList=selectDb(sbSQL.toString(),paramList, new ProductCategoryRowMapper());	
		if(productCategoryList.isEmpty()){
			throw new InvalidInputsException("Given master product category ID is invalid");
		}		
		return productCategoryList;
       
	}
	
	 /*This module use to retrieve product  details for particular product category from  the database for mobile application*/
	@Override
	public List<ProductDetailVO> getPoductListForCategory(String productCategoryId) throws Exception {
       
		
		StringBuilder sbSQL=new StringBuilder();
		sbSQL.append("select mp.product_id ProductID,mp.product_name ProductName,mp.price Price,");
		sbSQL.append("pimg.image_id ImageID,pimg.image Image,mp.is_visible_product ProductVisible ");
		sbSQL.append(" from mst_product mp,product_images pimg ");
		sbSQL.append(" where  mp.product_id=pimg.product_id and ");
		sbSQL.append(" mp.product_category_id=?");
		
		
		ArrayList<Object>paramList=new ArrayList<Object>();
		paramList.add(productCategoryId);
		List<ProductDetailVO> productList=selectDb(sbSQL.toString(), paramList,new ProductListRowMapper());
		if(productList.isEmpty()){
			throw new InvalidInputsException("Given product category ID is invalid");
		}
		Population populateObject=new Population();
		List<ProductDetailVO>product=populateObject.pupulateProductList(productList);
		return product;
       
	}	
	
	 /*This module use to retrieve product details for one product from  the database for mobile application*/
	@Override
	public ProductDetailVO getItemDetails(String productId)throws Exception {
  
		ProductDetailVO productDetail=new ProductDetailVO();		
		StringBuilder sbSQL=new StringBuilder();
		sbSQL.append("select  mp.product_id ProductID, mp.product_name ProductName,mp.description Description,mp.price Price,");
		sbSQL.append("mp.stock Stock,mp.is_visible_product ProductVisible,mp.color Color,pimg.image_id ImageID,pimg.image Image  ");
		sbSQL.append(" from mst_product mp,product_images pimg ");
		sbSQL.append(" where  mp.product_id=pimg.product_id and mp.product_id=?");

		ArrayList<Object>paramList=new ArrayList<Object>();
		paramList.add(productId);
		List<ProductDetailVO> ItemDetailList=selectDb(sbSQL.toString(), paramList,new ItemDetailRowMapper());
		if(ItemDetailList.isEmpty()){
			throw new InvalidInputsException("Given produuct ID is invalid");
		}
		Population populateObject=new Population();		
		List<ProductDetailVO>product=populateObject.pupulateProductList(ItemDetailList);
		
		if(product.size()>0 && product!=null){
			productDetail.setProductName(product.get(0).getProductName());
			productDetail.setDescription(product.get(0).getDescription());	
			productDetail.setColor(product.get(0).getColor());			
			productDetail.setPrice(product.get(0).getPrice());
			productDetail.setIsVisibleProduct(product.get(0).getIsVisibleProduct());
			productDetail.setSysId(productId);
			productDetail.setStock(product.get(0).getStock());
			productDetail.setProductImageList(product.get(0).getProductImageList());			
		}
		return productDetail;
       
	}
	
	 /*This module use to retrieve master product category id from  the database for validate purpose*/
	public List<String> getMasterProductCategoryId()throws Exception{

		List<String>roomCategoryIdList=new ArrayList<String>();
		String sql="select master_product_id from mst_product_category_type";
		
			List<MasterProductVO> roomCategoryId=selectDb1(sql,new mstProductCategoryIDRowMapper());
			if(roomCategoryId.isEmpty()){
				throw new InsufficientDataException("No data into the database");
			}	
			for (int i = 0; i < roomCategoryId.size(); i++) {
				roomCategoryIdList.add(roomCategoryId.get(i).getSysId());
			}
			return roomCategoryIdList;	
			
	}
	
	 /*This module use to retrieve product category id from  the database for validate purpose*/
	public List<String> getProductCategoryId () throws Exception{

		List<String>productCategoryIdList=new ArrayList<String>();
		String sql="select product_category_id from product_category";
	
			List<ProductCategoryVO> productCategoryId=selectDb1(sql,new ProductCategoryIDRowMapper());
			if(productCategoryId.isEmpty()){
				throw new InsufficientDataException("No data into the database");
			}	
			for (int i = 0; i < productCategoryId.size(); i++) {
				productCategoryIdList.add(productCategoryId.get(i).getSysId());
			}
			
			return productCategoryIdList;
		
	}
	 /*This module use to retrieve product  id from  the database for validate purpose*/
	public List<String> getProductId () throws Exception{

		List<String>productIdList=new ArrayList<String>();
		String sql="select product_id from mst_product";

			List<ProductDetailVO> productCategoryId=selectDb1(sql,new ProductIDRowMapper());
			if(productCategoryId.isEmpty()){
				throw new InsufficientDataException("No data into the database");
			}	
			for (int i = 0; i < productCategoryId.size(); i++) {
				productIdList.add(productCategoryId.get(i).getSysId());
			}
			return productIdList;		
		
	}

	 /*This module use to save particular product into design booklet table of the database*/
	@Override
	public void saveInDesignBooklet(DesignBookletVO designBooklet)throws Exception {
		TransactionStatus status=null;	
		try{
			String sql="INSERT INTO design_booklet(designbooklet_id,imei_no,product_id) VALUES(?,?,?)";
		    ArrayList<Object>paramList=new ArrayList<Object>();
			List<DesignBookletVO>bookletList=getdesignBooklet(designBooklet.getImeiNumber());	
			if(bookletList==null){
				designBooklet.setSysId(UtilityModel.getNewDesignBookletSysID());			
				if(designBooklet!=null){
				    paramList.add(designBooklet.getSysId());
				    paramList.add(designBooklet.getImeiNumber());
				    paramList.add(designBooklet.getProductId());
					executeDbUpdates(sql, paramList);	
				    }
			   }
			   else{
				    if(bookletList.get(0).getProductId().contains(designBooklet.getProductId())){
					   throw new InvalidInputsException("Given data already exist");
				    }
				    else{
					    designBooklet.setSysId(UtilityModel.getNewDesignBookletSysID());			
					    if(designBooklet!=null){
					    paramList.add(designBooklet.getSysId());
					    paramList.add(designBooklet.getImeiNumber());
					    paramList.add(designBooklet.getProductId());
						executeDbUpdates(sql, paramList);	
					    }
				    }			 
		      }			    
		}
		catch(Exception e){
			  rollbackTransaction(status);
			  throw e;
		}			
	}
	 /*This module use retrieve design booklet details from  the database*/
	@Override
	public List<ProductDetailVO> getDetailsForDesignBooklet(String imieNumber)throws Exception {
	
		String sql="select mp.product_id ProductID,mp.product_name ProductName,mp.price Price,pimg.image_id ImageID,pimg.image Image,mp.is_visible_product ProductVisible from mst_product mp,product_images pimg ,design_booklet db where  mp.product_id=pimg.product_id and mp.product_id=db.product_id and db.imei_no=?";
		ArrayList<Object>paramList=new ArrayList<Object>();
		paramList.add(imieNumber);
		List<ProductDetailVO>bookletList=selectDb(sql, paramList, new ProductListRowMapper());
		if(bookletList.isEmpty()){
			throw new InsufficientDataException("No data into the database");
		}
		Population populateObject=new Population();
		List<ProductDetailVO>booklet=populateObject.pupulateProductList(bookletList);
		return booklet;
		
	}	
		
	

	 /*This module use to retrieve design booklet details for particular imeinumber from  the database*/
	public List<DesignBookletVO> getdesignBooklet(String imieNumber)throws Exception{
        String sql="SELECT * FROM design_booklet WHERE imei_no=?";
		ArrayList<Object>paramList=new ArrayList<Object>();
		paramList.add(imieNumber);
		List<DesignBookletVO>bookletList=selectDb(sql,paramList, new DesignBookletRowMapper());
		if(!bookletList.isEmpty()){
			return bookletList;			
		}	       
		return null;	
		
	}
	 /*This module use to retrieve cart id from  the database for validation purpose*/
	public List<String> getCartID()throws Exception {      
		 String sql="select cartId from cart";		 
		 List<String>cartIDList=new ArrayList<String>();		
		 List<CartVO>cartIdList=selectDb1(sql,new PurchaseCartIDRowMapper());
		 if(cartIdList.isEmpty()){
			throw new InsufficientDataException("No data into the database");
		 }
		 for (int i = 0; i < cartIdList.size(); i++) {
			 cartIDList.add(cartIdList.get(i).getSysId().toString());			
		}		 
	     return cartIDList;        	
	}
	
	
	public List<String>getProductListForCart(String cartId)throws Exception{
	
		String sql="select productId from cart_details where cartId=?";
		ArrayList<Object>paramList=new ArrayList<Object>();
		List<String>productList=new ArrayList<String>();
		paramList.add(cartId);
		List<CartVO>productId=selectDb(sql, paramList,new PurchaseProductIDRowMapper());
		if(productId.isEmpty()|| productId.size()<=0){
			return productList;
		}
		for (int i = 0; i < productId.size(); i++) {
			productList.add(productId.get(i).getListCartItem().get(0).getSysId());
		}
		return productList;
	}
		
		

	
	
	 /*This module use to save cart details into  the database*/
	@Override
	public CartVO saveCartDetails(CartVO cartDetails) throws Exception {
		TransactionStatus status=null;
		try{
			String sql="insert into  cart(cartId,imeino) values(?,?);";
			String sqlst="insert into cart_details(cartId,productId,quantity)values(?,?,?);";
			String sqlUpdate="update cart_details set quantity=? where cartId=? and productId=?";
			ArrayList<Object>paramList=new ArrayList<Object>();
    		List<String>cartProductIdList=null; 
    		    //if imei no exist then new cart id
    		    
    		    if(cartDetails.getImeiNumber()!=null){
//    		    	 //  start transaction 
                    jdbcTemplate.getDataSource().getConnection().setAutoCommit(false);                      
        		    DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();			 
        		    status=dataSourceTransactionManager.getTransaction(paramTransactionDefinition );
    		    	try{
    		    		cartDetails.setSysId(UtilityModel.getNewPurchaseCartSysID());
 		         	    paramList.add(cartDetails.getSysId());
 		         	    paramList.add(cartDetails.getImeiNumber());
 		         	    executeDbUpdates(sql, paramList);
 		         	       
                        //check cartitemvo
 		         	          if(!cartDetails.getListCartItem().isEmpty()){         	         
		         		         paramList.clear();
		         		         paramList.add(cartDetails.getSysId());
		         		         paramList.add(cartDetails.getListCartItem().get(0).getSysId());//product ID
		         		         paramList.add(cartDetails.getListCartItem().get(0).getQuantity());	         		
						         executeDbUpdates(sqlst, paramList);
 		         	        }
    		    	}
    		    	catch(Exception e){
    		    		//org.springframework.dao.DuplicateKeyException
    		    		//fetch cartid using imei no
    		    		CartVO cart=cartDetailsWithImei(cartDetails.getImeiNumber()); 
    		    		cartDetails.setSysId(cart.getSysId());  
    		    		
    		    		//retrieve product id
    		    		cartProductIdList=getProductListForCart(cart.getSysId()); 
    		    		
    		    		if(!cartDetails.getListCartItem().isEmpty()){
    		    			if(!cartProductIdList.contains(cartDetails.getListCartItem().get(0).getSysId().toString())){
	         		         paramList.clear();
	         		         paramList.add(cart.getSysId());
	         		         paramList.add(cartDetails.getListCartItem().get(0).getSysId());//product ID
	         		         paramList.add(cartDetails.getListCartItem().get(0).getQuantity());	         		
					         executeDbUpdates(sqlst, paramList);						  						  
    		    			}
    		    			else{
    		    				//update exist product
    		    				     paramList.clear();
				            		 paramList.add(cartDetails.getListCartItem().get(0).getQuantity()); 
				            		 paramList.add(cartDetails.getSysId());
					         		 paramList.add(cartDetails.getListCartItem().get(0).getSysId());//product ID
					         		 executeDbUpdates(sqlUpdate, paramList);   
    		    			}
    		    	       
    		    		}		
    		    			
    		    	}   		    	
    		    	//commit transaction
	                dataSourceTransactionManager.commit(status);	
	                jdbcTemplate.getDataSource().getConnection().setAutoCommit(true);  		
    		    	jdbcTemplate.getDataSource().getConnection().close();
    		    }
    		    else{
//    		    	//retrieve imei no
    		    	CartVO cart=getImeiNumber(cartDetails.getSysId());
    		    	cartDetails.setImeiNumber(cart.getImeiNumber());
    		    	
    		    	//retrieve product id
		    		cartProductIdList=getProductListForCart(cartDetails.getSysId()); 
    		    	
    		    	if(!cartDetails.getListCartItem().isEmpty()){
    		    		if(!cartProductIdList.contains(cartDetails.getListCartItem().get(0).getSysId().toString())){
        		         paramList.clear();
        		         paramList.add(cartDetails.getSysId());
        		         paramList.add(cartDetails.getListCartItem().get(0).getSysId());//product ID
        		         paramList.add(cartDetails.getListCartItem().get(0).getQuantity());	         		
				         executeDbUpdates(sqlst, paramList);						  						  
		    			}
    		    		else{
		    				//update exist product
		    				     paramList.clear();
			            		 paramList.add(cartDetails.getListCartItem().get(0).getQuantity()); 
			            		 paramList.add(cartDetails.getSysId());
				         		 paramList.add(cartDetails.getListCartItem().get(0).getSysId());//product ID
				         		 executeDbUpdates(sqlUpdate, paramList);
		    				
		    			}
    		    	}
    		    }
      	
             return getCartDetails(cartDetails.getImeiNumber());
			}
		
		   catch(Exception e){
			    //rolback transaction
			      rollbackTransaction(status);	
			      jdbcTemplate.getDataSource().getConnection().close();			   
			      throw e;
		   }
		
	}

	/*This module use to retrieve cart details for particular imeinumber from  the database*/
	@Override
	public CartVO getCartDetails(String imeiNo) throws Exception {
    
		CartVO cartID=cartDetailsWithImei(imeiNo);
		if(cartID==null){
			throw new InsufficientDataException("No data into the database");
		}		
		StringBuilder sbSql=new StringBuilder();
		sbSql.append("select c.cartId CartID,c.imeino IMEI,cd.productId ProductID,cd.quantity Quantity,mp.product_name ProductName,");
		sbSql.append("mp.price Price,pimg.image_id ImageID,pimg.image Image,mp.is_visible_product ProductVisible ");
		sbSql.append(" from mst_product mp,product_images pimg,cart c ,cart_details cd ");
		sbSql.append(" where  mp.product_id=pimg.product_id and cd.cartId=c.cartId and cd.productId=mp.product_id and c.cartId=? ");

		ArrayList<Object>paramList=new ArrayList<Object>();
		paramList.add(cartID.getSysId());
		List<CartVO>cartList=selectDb(sbSql.toString(), paramList, new CartDetailRowMapper());
		if(cartList.isEmpty()){
			throw new InsufficientDataException("No data into the database");
		}		
		Population populateCartList=new Population();
		CartVO newCartList=populateCartList.populateCartList(cartList);
		return newCartList;
       	
	}

	/*This module use to remove product from cart*/
	@Override
	public CartVO removeProductFromCart(String cartId,String productId) throws Exception {
		
		List<String>cartIdList=getCartID();
		if(!cartIdList.contains(cartId)){
			throw new InvalidInputsException("given cartId is invalid");
		}				
		String sql="delete from cart_details where cartId=? and productId=?;";
		ArrayList<Object>paramList=new ArrayList<Object>();
		paramList.add(cartId);
		paramList.add(productId);
		executeDbUpdates(sql, paramList);		
		CartVO cart=getImeiNumber(cartId);			
		CartVO cartvo= getCartDetails(cart.getImeiNumber());
				
		if(cartvo.getListCartItem().isEmpty()){
			throw new InsufficientDataException("No content");
		}
		return cartvo;
		
		     	
			
	}

	/*This module use to retrieve cart details for specified imeinumber from database*/
	public CartVO cartDetailsWithImei(String imeino)throws Exception{
        
		 String sql="select * from cart where imeino=?";	
		 ArrayList<Object>paramList=new ArrayList<Object>();
		 CartVO cart=new CartVO();
		 paramList.add(imeino);
		 List<CartVO>cartList=selectDb(sql,paramList,new CartDetailWithSpecifiedImeiRowMapper());
		 if(cartList.isEmpty()){
			 return null;
		 }	
		 cart.setSysId(cartList.get(0).getSysId());
		 return cart;
       
	}
	
	
	public CartVO getImeiNumber(String cartID)throws Exception{
	
		 String sql="select * from cart where cartId=?";	
		 ArrayList<Object>paramList=new ArrayList<Object>();
		 CartVO cart=new CartVO();
		 paramList.add(cartID);
		 List<CartVO>cartList=selectDb(sql,paramList,new ImeiNumberFromCartIDRowMapper());
		if(cartList.size()>0){
			cart.setImeiNumber(cartList.get(0).getImeiNumber());
			return cart;
		}	 
		  return null;
		
		
	}
	
	 
	 /*This module use to retrieve price of order product from product table*/
	 public ProductDetailVO getProductPrice(String productID)throws Exception{
		
		 String sql="select product_name,price from mst_product where product_id=?";
		 ArrayList<Object>paramList=new ArrayList<Object>();
		 ProductDetailVO productDetail=new ProductDetailVO();
		 paramList.add(productID);
		 List<ProductDetailVO>productPrice=selectDb(sql, paramList, new productPriceRowMapper());
		 if(productPrice!=null ||productPrice.size()>0){
			 productDetail.setPrice(productPrice.get(0).getPrice());
			 productDetail.setProductName(productPrice.get(0).getProductName());
			 return productDetail;
		 }
		return null;
		
	 }
	 
	 /*This module use to retrieve quantity of purchase product*/
	 public List<CartItemVo> getPurchaseProductFromCart(String cartID)throws Exception{
	
//		 String sql="select productId, quantity from cart_details where cartId=?";
		 StringBuilder sb=new StringBuilder();
		 sb.append("select  c.productId, c.quantity , mp.price ,mp.product_name ");
		 sb.append(" from cart_details c, mst_product mp  ");
		 sb.append(" where c.productId = mp.product_id and c.cartId =? ");
 
		 ArrayList<Object>paramList=new ArrayList<Object>();
		 CartItemVo cart=new CartItemVo();
		 paramList.add(cartID);
		 List<CartItemVo>cartList=selectDb(sb.toString(), paramList, new purchaseProductQuantityRowMapper());
		 if(cartList.isEmpty()||cartList.size()<0){
			 throw new InsufficientDataException("NO prouct for purchase");
		 }		 
		 return cartList;			
		 }
		  
	 public void removePaidProductFromCart(String cartId)throws Exception{
//		 TransactionStatus status=null;
		 try{
			 
			//  start transaction 	 
//			 jdbcTemplate.getDataSource().getConnection().setAutoCommit(false);                            
//       		 DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();			 
//       		 status=dataSourceTransactionManager.getTransaction(paramTransactionDefinition );
	 			 
		      String sql="delete from cart where cartId=?";
		      ArrayList<Object>paramList=new ArrayList<Object>();
		      paramList.add(cartId);		 
	
		      executeDbUpdates(sql, paramList);
		      //commit operation
//			  dataSourceTransactionManager.commit(status);
   
		 
		 } 	
		 catch (Exception e) {	
//		     	rollbackTransaction(status);
				throw e;
		}	
	 }
	 /*check for user authentication */
	 public UserVO getUserAuthenticateDetail(String emailId)throws Exception{
		
			 String sql="select isVerifiedUser from user where email_id=?";
			 UserVO user=new UserVO();
			 ArrayList<Object>paramList=new ArrayList<Object>();
			 paramList.add(emailId);
			 List<UserVO>userDetail=selectDb(sql, paramList,new verifyUserRowMapper());
			 if(userDetail==null || userDetail.isEmpty()){
				 throw new InvalidInputsException("No exist user");
			 }
			 user.setVerifiedUser(userDetail.get(0).isVerifiedUser());			 
			 return user; 
	 }
	 
	 /*This module use to retrieve existing user details*/
		public UserVO existingUser(String emailId)throws Exception{
					
				String sql="select * from user where email_id=?";
				ArrayList<Object>paramList=new ArrayList<Object>();
				UserVO user=new UserVO();
				paramList.add(emailId);
				List<UserVO> existUserList=selectDb(sql, paramList, new ExistingUserRowMapper());
				if(existUserList.size()<0 || existUserList==null){
					return null;
				}
				user.setSysId(existUserList.get(0).getSysId());
				user.setAddress(existUserList.get(0).getAddress());	
				user.setCity(existUserList.get(0).getCity());
				user.setEmailId(existUserList.get(0).getEmailId());
				user.setFname(existUserList.get(0).getFname());
				user.setLandLineNo(existUserList.get(0).getLandLineNo());
				user.setMobileNo(existUserList.get(0).getMobileNo());
				user.setPassword(existUserList.get(0).getPassword());
				user.setPincode(existUserList.get(0).getPincode());
				user.setState(existUserList.get(0).getState());		
				user.setRole(existUserList.get(0).getRole());
				user.setVerifiedUser(existUserList.get(0).isVerifiedUser());
				user.setCreatedOn(existUserList.get(0).getCreatedOn());
				user.setUsername(existUserList.get(0).getUsername());		
				return user;			 		
		}

	 /*This module use to create order of product into the database*/
	 @Override
		public void createOrderEntry(ProductOrderVO order) throws Exception {
		 TransactionStatus status=null;
            try{
            	List<CartItemVo>cartList=null;
            	//temporary user authentication part remain into this code
         
            	UserVO user=getUserAuthenticateDetail(order.getUserEmailId());
            	if(user.isVerifiedUser()==false){
            		throw new AccessDeniedException("Unauthorized user");
            	}
            	
            	if(order!=null){           		
            		List<String>cartIdList=getCartID();
    				if(!cartIdList.contains(order.getCartId())){
    					throw new InvalidInputsException("Given cartId is invalid");
    				}  	
    				
    				cartList=getPurchaseProductFromCart(order.getCartId());
                	
    				//Retrieve user detail for sending into email
    				UserVO uservo =existingUser(order.getUserEmailId());
    				order.setUser(uservo);
            	}
                   
            	                  
                 //start transaction 
            	jdbcTemplate.getDataSource().getConnection().setAutoCommit(false);
        		 DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();			 
        		 status=dataSourceTransactionManager.getTransaction(paramTransactionDefinition );

            	if(order!=null){
				StringBuilder sb=new StringBuilder();
				StringBuilder sqlsb=new StringBuilder();								
				sb.append("INSERT INTO  product_order(order_id,user_id,order_status,transaction_id)VALUES(?,?,?,?)");//edit 						
				sqlsb.append("insert into order_details select po.order_id, c.productId, c.quantity , mp.price ");
				sqlsb.append(" from cart_details c, mst_product mp, product_order po ");
				sqlsb.append(" where c.productId = mp.product_id  ");
				sqlsb.append(" and c.cartId = ? and po.order_id = ?;");
				
	
				ArrayList<Object>paramList=new ArrayList<Object>();
				paramList.add(order.getSysId());
				paramList.add(order.getUserEmailId());
				order.setOrderStatus("Process");
				paramList.add(order.getOrderStatus());
				paramList.add(order.getTransactionID());
				executeDbUpdates(sb.toString(), paramList);	
				
				paramList.clear();
				paramList.add(order.getCartId());
				paramList.add(order.getSysId());
				executeDbUpdates(sqlsb.toString(), paramList);
	
				//delete product from cart after transaction done successfully  
				removePaidProductFromCart(order.getCartId());
				
				//send confirmation email about order has been received successfully
				UtilityModel.sendEmailForProductOrder(cartList,order,emailID,password);

				//commit operation
				dataSourceTransactionManager.commit(status);
				jdbcTemplate.getDataSource().getConnection().setAutoCommit(true);                 
							
				
	
			}	  
                       
//			UtilityModel.trial(emailID,password);
            	}
               	
    		catch (Exception e) {
    			//rollback operation 
    			rollbackTransaction(status);
    			throw e;
    		}	
			 
		}
	    /*This module use to retrieve order history of product from  the database currently not using*/
		@Override
		public ProductOrderVO getOrderHistoryForUser(String userId)throws Exception {		
            try{
			ProductOrderVO productOrderVO=new ProductOrderVO();		
			List<ProductDetailVO>productdetaillist=new ArrayList<ProductDetailVO>();		
			String sql="SELECT pd.product_name productName,pd.image,od.product_amount price,porder.order_date orderDate FROM product_order porder,product_details pd,order_details od,user u WHERE porder.user_id=? and pd.product_id=od.product_id and porder.order_id=od.order_id and u.user_id =porder.user_id";
			ArrayList< Object>paramList=new ArrayList<Object>();
			paramList.add(userId);
			
			List<ProductOrderVO>orderHistroyList=selectDb(sql, paramList, new OrderHistoryRowMapper());
			if(orderHistroyList.isEmpty()){
				throw new InvalidInputsException("Given user ID is invalid");
			}
			if(orderHistroyList.size()>0 && orderHistroyList!=null){
				for (int i = 0; i < orderHistroyList.size(); i++) {
//					productOrderVO =(ProductOrderVO)orderHistroyList.get(i);
//					 for (int j = 0; j < orderHistroyList.get(i).getProductList().size(); j++) {
//						ProductDetailVO pd=new ProductDetailVO();
//						pd.setPrice(productOrderVO.getProductList().get(j).getPrice());
//						pd.setProductName(productOrderVO.getProductList().get(j).getProductName());
//						productdetaillist.add(pd);	
//					 }						
				}	
				productOrderVO.setOrderDate(orderHistroyList.get(0).getOrderDate());			
//				productOrderVO.setProductList(productdetaillist);			
			}
			return 	productOrderVO;	
            }     	
    		catch (Exception e) {			
    			return null;
    		}	
		}
		
		/*This module use to retrieve order details from database*/
		@Override
		public List<ProductOrderVO> getorderDetails() throws Exception {		   
		    	StringBuilder sb=new StringBuilder();
		    	sb.append("select po.order_id,po.order_date,");
		    	sb.append("po.order_status,od.product_id,");
		    	sb.append("po.transaction_id,");
		    	sb.append("mp.product_name,");
		    	sb.append("od.quantity,od.product_amount,");
		    	sb.append("u.user_id ,u.email_id ,u.fname ,");
		    	sb.append("u.address ,u.pincode ,u.city ,");
		    	sb.append("u.state,u.mobileNo ,u.landLineNo ");		    	
		    	sb.append(" from product_order po,order_details od,mst_product mp,user u ");
		    	sb.append(" where po.order_id=od.order_id and");
		    	sb.append(" od.product_id=mp.product_id and");
		    	sb.append(" po.user_id=u.email_id ");
		    	
		    	List<ProductOrderVO>orderList=selectDb1(sb.toString(), new OrderDetailRowMapper());
		    	if(orderList.isEmpty()){
		    		throw new InsufficientDataException("There is no any order into the database");
		    	}
		    	/*population work*/
		    	Population populateObject=new Population();
		    	List<ProductOrderVO>orderlist=populateObject.populateProductOrderList(orderList);
		    	return orderlist;		    	
		}	
		/*Retrieve order id for validation purpose */
		public List<String>productOrderIdList()throws Exception{
				String sql="select order_id from product_order";
				List<String>orderidList=new ArrayList<String>();
				List<ProductOrderVO>productOrderIdList=selectDb1(sql,new orderIdRowMapper());				
				if(productOrderIdList!=null ||productOrderIdList.size()>0){									
				   for (int i = 0; i < productOrderIdList.size(); i++) {
					     orderidList.add(productOrderIdList.get(i).getSysId());
				   }
				}
				else{
					throw new InsufficientDataException("There is no any order into the database");
				}
				return orderidList;			
		}

		/*This module use to update order status into the database*/
		@Override
		public void updateOrderStatus(ProductOrderVO order) throws Exception {	
				//check given orderid exist or not
				List<String>orderIdList=productOrderIdList();
				if(!orderIdList.contains(order.getSysId())){
					throw new InvalidInputsException("Given orderId does not exist");
				}				
				String sql="update product_order set order_status=? where order_id=?";
				ArrayList< Object>paramList=new ArrayList<Object>();
				paramList.add(order.getOrderStatus());
				paramList.add(order.getSysId());
				executeDbUpdates(sql, paramList);
		}
		@Override
		public void confirmOrder(ProductOrderVO order) throws Exception {

			//check cartId if not exit
			if(order!=null){
				String sql="select cartId from cart where cartId=?";
				ArrayList< Object>paramList=new ArrayList<Object>();
				paramList.add(order.getCartId());
				List<CartVO>cartIdList=selectDb(sql,paramList,new PurchaseCartIDRowMapper());
				     
				    if(cartIdList.isEmpty()){
					  //send 200 order already place successfully
				    }
				    else{
				    	//recreate an order send 200
				    	order.setSysId(UtilityModel.getNewOrderSysId().substring(0,13));
					     createOrderEntry(order);
				    }

			}
	
		}

}
/*
 working code 
 @Override
	public CartVO saveCartDetails(CartVO cartDetails) throws Exception {
		TransactionStatus status=null;
		try{
			String sql="insert into  cart(cartId,imeino) values(?,?);";
			String sqlst="insert into cart_details(cartId,productId,quantity)values(?,?,?);";
			String sqlUpdate="update cart_details set quantity=? where cartId=? and productId=?";
			ArrayList<Object>paramList=new ArrayList<Object>();
			List<String>cartProductIdList=null;
			
			//check give imei number exist into db 
             CartVO cart=cartDetailsWithImei(cartDetails.getImeiNumber()); 
             //if exist then retrieve product id of respective cartId
             if(cart!=null){
            	 cartDetails.setSysId(cart.getSysId());
	             cartProductIdList=getProductListForCart(cart.getSysId()); 
             }

         //  start transaction 
                jdbcTemplate.getDataSource().getConnection().setAutoCommit(false);     
             
    		    DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();			 
    		    status=dataSourceTransactionManager.getTransaction(paramTransactionDefinition );
                
//    		    dataSourceTransactionManager.setRollbackOnCommitFailure(true);//edit

		         	if(cart==null){
				       cartDetails.setSysId(UtilityModel.getNewPurchaseCartSysID());
		         	   paramList.add(cartDetails.getSysId());
		         	   paramList.add(cartDetails.getImeiNumber());
		         	   executeDbUpdates(sql, paramList);
		         	   for (int i = 0; i < cartDetails.getListCartItem().size(); i++) {
		         		  paramList.clear();
		         		  paramList.add(cartDetails.getSysId());
		         		  paramList.add(cartDetails.getListCartItem().get(i).getSysId());//product ID
		         		  paramList.add(cartDetails.getListCartItem().get(i).getQuantity());	         		
						  executeDbUpdates(sqlst, paramList);						  						  
					}
		         	   //commit transaction
		         	   dataSourceTransactionManager.commit(status);		
		         	   jdbcTemplate.getDataSource().getConnection().setAutoCommit(true);  
	         	   
			        }			
			        else{			          
		                for (int i = 0; i < cartDetails.getListCartItem().size(); i++) {
		            	 if(!cartProductIdList.contains(cartDetails.getListCartItem().get(i).getSysId())){
		               	      paramList.clear();
			         		  paramList.add(cartDetails.getSysId());
			         		  paramList.add(cartDetails.getListCartItem().get(i).getSysId());//product ID
			         		  paramList.add(cartDetails.getListCartItem().get(i).getQuantity());			         		 
							  executeDbUpdates(sqlst, paramList);
                           }
		            	 else{
		            		 paramList.clear();
		            		 paramList.add(cartDetails.getListCartItem().get(i).getQuantity()); 
		            		 paramList.add(cartDetails.getSysId());
			         		 paramList.add(cartDetails.getListCartItem().get(i).getSysId());//product ID
			         		 executeDbUpdates(sqlUpdate, paramList);
		            	    }
		            	 
                         }
                       //commit transaction
		                dataSourceTransactionManager.commit(status);	
		                jdbcTemplate.getDataSource().getConnection().setAutoCommit(true);  


			        }	
             
             return getCartDetails(cartDetails.getImeiNumber());
			}
		
		   catch(Exception e){
			    //rolback transaction
			      rollbackTransaction(status);						
			      throw e;
		   }
		
	}
*/
	
