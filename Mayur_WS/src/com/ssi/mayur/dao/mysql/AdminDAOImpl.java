package com.ssi.mayur.dao.mysql;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.dao.IAdminDAO;
import com.ssi.mayur.dao.rowmapper.ExistingUserRowMapper;
import com.ssi.mayur.dao.rowmapper.ProductListRowMapper;
import com.ssi.mayur.dao.rowmapper.UserVORowMapper;
import com.ssi.mayur.dao.rowmapper.getAccountActivateCodeRowMapper;
import com.ssi.mayur.exception.AccessDeniedException;
import com.ssi.mayur.exception.DatabaseConnectionException;
import com.ssi.mayur.exception.ExistDataException;
import com.ssi.mayur.exception.InvalidInputsException;
import com.ssi.mayur.model.util.UtilityModel;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.UserVO;
import com.ssi.mayur.utility.IMayurServiceUtilityConstants;


public class AdminDAOImpl extends DAOBase implements IAdminDAO,IMayurServiceUtilityConstants{
	DataSource dataSource = null;
	private static Logger logger = LoggerFactory.getLogger(AdminDAOImpl.class);
	
	@Value("${app.emailid}")
	private  String emailID;
	@Value("${app.email.password}")
	private String password;
	
	/*module use for login into mobile application*/
	@Override
	public UserVO login(UserVO user) throws Exception {			
		  UserVO uservo=existingUser(user.getEmailId());
		  if(uservo==null){
			throw new InvalidInputsException("Given emailId is invalid");			
		  }	
		  if(uservo.isVerifiedUser()==true){
			    if(uservo.getEmailId().equals(user.getEmailId()) && uservo.getPassword().equals(user.getPassword())){
				      return uservo;
			    }
			    else{
				     throw new InvalidInputsException("Given password is wrong");
			    }
		  }
		  else{
			  throw new AccessDeniedException("User not verified");
		  }
	}
	
	/*module use for retrieve user details*/
	@Override
	public UserVO getUser(String userId)throws Exception{        
		     UserVO user=new UserVO();
		     String sql="SELECT * FROM user WHERE user_name=?";
		     ArrayList<Object>paramList=new ArrayList<Object>();
		     paramList.add(userId);
		     List<UserVO> userList=selectDb(sql, paramList,new UserVORowMapper());
		     if(userList.isEmpty()){	
			       throw new InvalidInputsException("Given userID is invalid");
		     }
		     if(userList.size()>0 && userList!=null){
			       user=userList.get(0);									
		     }
		     return user;     
	}
    
	
	/*This module use to retrieve existing user details*/
	public UserVO existingUser(String emailId)throws Exception{
				
			String sql="select * from user where email_id=?";
			ArrayList<Object>paramList=new ArrayList<Object>();
			UserVO user=new UserVO();
			paramList.add(emailId);
			List<UserVO> existUserList=selectDb(sql, paramList, new ExistingUserRowMapper());
			if(existUserList.size()<=0 || existUserList.isEmpty()){
				return null;
			}
			user.setSysId(existUserList.get(0).getSysId());
			user.setAddress(existUserList.get(0).getAddress());	
			user.setCity(existUserList.get(0).getCity());
			user.setEmailId(existUserList.get(0).getEmailId());
			user.setFname(existUserList.get(0).getFname());
			user.setLandLineNo(existUserList.get(0).getLandLineNo());
			user.setMobileNo(existUserList.get(0).getMobileNo());
			user.setPassword(existUserList.get(0).getPassword());
			user.setPincode(existUserList.get(0).getPincode());
			user.setState(existUserList.get(0).getState());		
			user.setRole(existUserList.get(0).getRole());
			user.setVerifiedUser(existUserList.get(0).isVerifiedUser());
			user.setCreatedOn(existUserList.get(0).getCreatedOn());
			user.setUsername(existUserList.get(0).getUsername());		
			return user;			 
		
	}
	
	/*This module randomly generate verify alphanumeric code*/
	public String generateVerifyCode(SecureRandom random) {
	    return new BigInteger(130, random).toString(16);
	  
	  }
	
	
	
	/*module use for create user for mobile apps */
	@Override
	public void createUser(UserVO user) throws Exception {
		TransactionStatus status=null;
		try{
 
       StringBuilder sb=new StringBuilder();
       sb.append("insert into user (user_id,user_name,email_id,password,fname,");
       sb.append("address,pincode,city,state,mobileNo,landLineNo,");
       sb.append("isVerifiedUser,role)");
       sb.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
	   ArrayList<Object>paramList=new ArrayList<Object>();
	   
	   /*check for given user exist or not*/	 
	     UserVO uservo=existingUser(user.getEmailId());
	     if(uservo!=null) {
	    	 throw new ExistDataException("Given user already exist");
	     }
	     
	   //check whether give code already exist or not
		 UserVO activatedetails=getActivateAccountCode(user);	
	     

         
		//  start transaction
		    jdbcTemplate.getDataSource().getConnection().setAutoCommit(false);           
		    DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();			 
		    status=dataSourceTransactionManager.getTransaction(paramTransactionDefinition );
	     
	     
	     user.setSysId(UtilityModel.getNewUserSysId());		
	     paramList.add(user.getSysId());
		 paramList.add("mobile user");//set user name
		 paramList.add(user.getEmailId());
		 paramList.add(user.getPassword());
		 paramList.add(user.getFname());
		 paramList.add(user.getAddress());
		 paramList.add(user.getPincode());
		 paramList.add(user.getCity());
		 paramList.add(user.getState());
		 paramList.add(user.getMobileNo());
		 if(user.getLandLineNo()==null){
			 paramList.add(""); 
		 }
		 else{
			 paramList.add(user.getLandLineNo()); 
		 }		 
		 paramList.add(false);//set verified user flag
		 paramList.add("Role_User");//set role
		 executeDbUpdates(sb.toString(), paramList);	
 
//		 //check whether give code already exist or not
//		 UserVO activatedetails=getActivateAccountCode(user);	
		  if(activatedetails==null){			  
			   //send an email to user for activate account purpose
				 SecureRandom random = new SecureRandom();		  
				 String randomString=generateVerifyCode(random);
				 String activateCode=randomString.substring(0,8);
				 System.out.print(activateCode);
				 user.setActivateAccountCode(activateCode);
				 
				 //entry to the db
				 createActivateAccountCode(user);				
		  }
		  else{
			    //send an email to user for activate account purpose
				 SecureRandom random = new SecureRandom();		  
				 String randomString=generateVerifyCode(random);
				 String activateCode=randomString.substring(0,8);
				 System.out.print(activateCode);
				 user.setActivateAccountCode(activateCode);
				 
				 //update to the db
				 updateActivateCode(user);	  
		  }
		//commit transaction
          dataSourceTransactionManager.commit(status);	
          jdbcTemplate.getDataSource().getConnection().setAutoCommit(true);  
          jdbcTemplate.getDataSource().getConnection().close();
		  
		  
		   //send email
			 UtilityModel.sendEmailForActivateAccount(user,emailID,password);
		}
		catch(Exception e){
			 //rolback transaction
		      rollbackTransaction(status);	
		      jdbcTemplate.getDataSource().getConnection().close();
		      throw e;
		}
	
}

	
	/*this module is use to insert generate user verify code into the database*/
	public void createActivateAccountCode(UserVO user)throws Exception{	
			String sql="insert into activate_account(user_id,email_id,activate_code)values(?,?,?)";
			ArrayList<Object>paramList=new ArrayList<Object>();
			paramList.add(user.getSysId());
			paramList.add(user.getEmailId());
			paramList.add(user.getActivateAccountCode());
			executeDbUpdates(sql, paramList);	
	}
	/*This module use to retrieve verify code from database for specified user*/
	public UserVO getActivateAccountCode(UserVO user)throws Exception{
	
			String sql="select * from activate_account where email_id=?";
			ArrayList<Object>paramList=new ArrayList<Object>();
			UserVO uservo=new UserVO();
			paramList.add(user.getEmailId());
			List<UserVO>userList=selectDb(sql, paramList, new getAccountActivateCodeRowMapper());
			if(userList.isEmpty()){
				return null;
			}			
			uservo.setActivateAccountCode(userList.get(0).getActivateAccountCode());
			uservo.setSysId(userList.get(0).getSysId());
			uservo.setEmailId(userList.get(0).getEmailId());
			return uservo;
		
	}
	/*This module is use to update activate code into the database */
	public void updateActivateCode(UserVO user)throws Exception{
	
			String sql="update activate_account set activate_code=? where email_id=? and user_id=?";
			ArrayList<Object>paramList=new ArrayList<Object>();
			paramList.add(user.getActivateAccountCode());
			paramList.add(user.getEmailId());
			paramList.add(user.getSysId());
			executeDbUpdates(sql, paramList);				
		
	}
	
	public void deleteActivateCode(UserVO user)throws Exception{
		
			String sql="delete from activate_account where  email_id=?";
			ArrayList<Object>paramList=new ArrayList<Object>();
			paramList.add(user.getEmailId());				
			executeDbUpdates(sql, paramList);			
		
	}
	
	/*This module use to verify user identity and set verified property to true*/
	public void authenticateUser(UserVO user)throws Exception{
	
			String sql="update user set isVerifiedUser=true where email_id=?";
			ArrayList<Object>paramList=new ArrayList<Object>();
			paramList.add(user.getEmailId());				
			executeDbUpdates(sql, paramList);				
		
	}
	
	/*activate mayur mobile apps account with activate code*/
	@Override
	public UserVO activateAccount(UserVO user) throws Exception {
		
		   
		UserVO account=getActivateAccountCode(user);
		if(account!=null){
			if(!account.getActivateAccountCode().equals(user.getActivateAccountCode())){
				throw new InvalidInputsException("invalid verification code.Please retry");
			}
			//delete activate code records
			deleteActivateCode(user);
			//update into user table with verifiedUser field
			authenticateUser(user);
			account.setVerifiedUser(true);
			return account;		
		}	
		throw new InvalidInputsException("Access denied.Please retry.Not valid user");
		
	}	

	
}
