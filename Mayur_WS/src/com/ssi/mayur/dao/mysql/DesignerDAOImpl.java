package com.ssi.mayur.dao.mysql;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.CannotGetJdbcConnectionException;

import com.ssi.mayur.dao.DAOBase;
import com.ssi.mayur.dao.IDesignerDAO;
import com.ssi.mayur.dao.rowmapper.DesignerDetailRowMapper;
import com.ssi.mayur.dao.rowmapper.DesignerWorkRowMapper;
import com.ssi.mayur.exception.DatabaseConnectionException;
import com.ssi.mayur.exception.InsufficientDataException;
import com.ssi.mayur.exception.InvalidInputsException;
import com.ssi.mayur.model.vo.CustomerReviewVO;
import com.ssi.mayur.model.vo.DesignerExpertVO;
import com.ssi.mayur.model.vo.WorkVO;

public class DesignerDAOImpl extends DAOBase  implements IDesignerDAO {	
	 private static Logger logger = LoggerFactory.getLogger(DesignerDAOImpl.class);	
	 DataSource dataSource=null;
	
	 /*This module use to get the detail regarding designer*/
	@Override
	public List<DesignerExpertVO> getDesigner() throws Exception {	
	
		List<DesignerExpertVO>designerExpertList1=new ArrayList<DesignerExpertVO>();		
		List<String>designerIdList=new ArrayList<String>();										
		String sql="select d.designer_id DesignerID,d.designer_name DesignerName,d.designer_place DesignerPlace,d.rating DesignerRating,dd.work_image DesignerWorkImage from designer d,designer_details dd where d.designer_id=dd.designer_id";		
		List<DesignerExpertVO>designerList=selectDb1(sql,new DesignerDetailRowMapper());							
		if(designerList.isEmpty()){
			throw new InsufficientDataException("No data into the database");
		}
		if(designerList.size()>0 && designerList!=null){					
		    for (int i = 0; i <designerList.size(); i++) {
			    if(designerExpertList1.isEmpty()){
				    designerIdList.add(designerList.get(i).getSysId().toString());
					designerExpertList1.add(designerList.get(i));
					designerExpertList1.get(i).setWorkImageUrl(designerExpertList1.get(i).getWorkImage().get(0).getWorkimage());
			    }
			   else{
				    int designlistsize=designerExpertList1.size();
				    for (int j = 0; j < designlistsize; j++) {
					    if(designerExpertList1.get(j).getSysId().equals(designerList.get(i).getSysId())){					  
                           }
					    else{
						     if(!designerIdList.contains(designerList.get(i).getSysId())){
							  designerIdList.add(designerList.get(i).getSysId());
							  designerExpertList1.add(designerList.get(i));	
							  designlistsize=designerExpertList1.size();
							  designerExpertList1.get(designlistsize-1).setWorkImageUrl(designerExpertList1.get(designlistsize-1).getWorkImage().get(0).getWorkimage());							  
						   }
						 }  
					}
		      }
		 	  		    		  
		}	
	}		
		return designerExpertList1;
		
		
}
	 /*This module use to get the detail regarding designer work*/
	@Override
	public DesignerExpertVO getDesignerWork(String designerId)throws Exception {	
		
		DesignerExpertVO designerWorkvo=new DesignerExpertVO();
		List<WorkVO>workList=new ArrayList<WorkVO>();
		List<CustomerReviewVO>customerReviewList=new ArrayList<CustomerReviewVO>();
		List<String>customerReviewIdList=new ArrayList<String>();
		List<String>workIdList=new ArrayList<String>();
		ArrayList<Object>paramList=new ArrayList<Object>();
		paramList.add(designerId);
		String sql="select d.contactno,d.email_id,d.detail_designer,d.designer_profile_image,dd.work_id,dd.work_image,dd.work_description,cv.review_id,cv.customer_rating,cv.review_description,cv.review_date,u.fname from   designer d,designer_details dd ,customer_review cv,user u where  d.designer_id=dd.designer_id and cv.customer_id=u.user_id and cv.designer_id=d.designer_id and d.designer_id=?;";
		List<DesignerExpertVO>designerList=selectDb(sql, paramList, new DesignerWorkRowMapper());
		if(designerList.isEmpty()){
			throw new InvalidInputsException("Given designer ID is invalid");			
		}		

			designerWorkvo.setContactNo(designerList.get(0).getContactNo());
			designerWorkvo.setEmailId(designerList.get(0).getEmailId());		
			designerWorkvo.setDetailOFDesignFirm(designerList.get(0).getDetailOFDesignFirm());
			designerWorkvo.setPersonImageUrl(designerList.get(0).getPersonImageUrl());

		for (int i = 0; i < designerList.size(); i++) {
			if(customerReviewList.isEmpty()){
				customerReviewIdList.add(designerList.get(i).getCustReview().get(0).getSysId().toString());
				customerReviewList.add(designerList.get(i).getCustReview().get(0));
		    }
		   else{
			    int customerReviewListsize=customerReviewList.size();
			    for (int j = 0; j < customerReviewListsize; j++) {
				    if(customerReviewList.get(j).getSysId().equals(designerList.get(i).getCustReview().get(0).getSysId())){	
				    	
				    }                     
				    else{
				    	if(!customerReviewIdList.contains(designerList.get(i).getCustReview().get(0).getSysId())){
				    		customerReviewIdList.add(designerList.get(i).getCustReview().get(0).getSysId());
				    		customerReviewList.add(designerList.get(i).getCustReview().get(0));
				    		customerReviewListsize=customerReviewList.size();
				    	}
				    }
			    }
					
			}  
					
			if(workList.isEmpty()){
				workIdList.add(designerList.get(i).getWorkImage().get(0).getSysId().toString());
				workList.add(designerList.get(i).getWorkImage().get(0));
		    }
		   else{
			    int workListsize=workList.size();
			    for (int j = 0; j < workListsize; j++) {
				    if(workList.get(j).getSysId().equals(designerList.get(i).getWorkImage().get(0).getSysId())){	
				    	
				    }                     
				    else{
				    	if(!workIdList.contains(designerList.get(i).getWorkImage().get(0).getSysId())){
				    		workIdList.add(designerList.get(i).getWorkImage().get(0).getSysId());
				    		workList.add(designerList.get(i).getWorkImage().get(0));
				    		workListsize=workList.size();
				    	}
				    }
			    }					
			}  			
		}	
	
		designerWorkvo.setWorkImage(workList);
		designerWorkvo.setCustReview(customerReviewList);
		return designerWorkvo;		
		

}
}