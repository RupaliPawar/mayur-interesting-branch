package com.ssi.mayur.dao;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public interface IDAOBase {
	public TransactionStatus startTransaction() throws Exception;
	public void commitTransaction(TransactionStatus status) throws Exception;	
	public void rollbackTransaction(TransactionStatus status) throws Exception;
}
