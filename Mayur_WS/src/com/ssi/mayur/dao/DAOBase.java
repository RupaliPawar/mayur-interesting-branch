package com.ssi.mayur.dao;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;
import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.transaction.support.DefaultTransactionDefinition;














import org.springframework.transaction.support.DefaultTransactionStatus;

import com.google.gson.reflect.TypeToken;
import com.ssi.mayur.exception.TimeOutException;
import com.ssi.mayur.model.vo.CartItemVo;
import com.ssi.mayur.model.vo.CartVO;
import com.ssi.mayur.model.vo.DesignBookletVO;
import com.ssi.mayur.model.vo.ProductCategoryVO;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.MasterProductVO;
import com.ssi.mayur.model.vo.ProductImageVO;
import com.ssi.mayur.model.vo.ProductOrderVO;
import com.ssi.mayur.model.vo.UserVO;
import com.sun.jersey.core.util.Base64;


public class DAOBase  implements IDAOBase{
	private static Logger logger = LoggerFactory.getLogger(DAOBase.class);
	
	protected JdbcTemplate jdbcTemplate;
	private static DataSource dataSource;
	
	protected DataSourceTransactionManager dataSourceTransactionManager;
	public void setDataSource(DataSource dataSource) {
		
		jdbcTemplate = new JdbcTemplate(dataSource);
		
	}
	
	public void setDataSourceTransactionManager(DataSourceTransactionManager dataSourceTransactionManager) {
		this.dataSourceTransactionManager = dataSourceTransactionManager;	
		
	}
	

	
    public void executeDbUpdates(String sql, List<Object> paramList) throws Exception{	
		
		if(paramList != null && paramList.size() > 0){
			jdbcTemplate.update(sql, paramList.toArray(new Object[paramList.size()]));
//			jdbcTemplate.getDataSource().getConnection().close();
			
		}
		else{
			jdbcTemplate.update(sql);
//			jdbcTemplate.getDataSource().getConnection().close();
			
		}

		
    }
	public List selectDb(String sql, List<Object> paramList, RowMapper rowMapper) throws Exception{	
		List list = null;	
		if(paramList != null && paramList.size() > 0){

			
			list = jdbcTemplate.query(sql, paramList.toArray(new Object[paramList.size()]), rowMapper);
//			jdbcTemplate.getDataSource().getConnection().close();
		}
		else{
			list = jdbcTemplate.query(sql, rowMapper);
//			jdbcTemplate.getDataSource().getConnection().close();
		}
		return list;
		
	}
	public List selectDb1(String sql,  RowMapper rowMapper) throws Exception{	
		List list = null;	

			list = jdbcTemplate.query(sql,rowMapper);
//			jdbcTemplate.getDataSource().getConnection().close();
//			
		    return list;
		
	}
	
	public Connection getConnection() throws Exception{			
		return this.jdbcTemplate.getDataSource().getConnection();
	}

	protected Map<String, Object> callStoreProcedure(String spNameParamString, List<SqlParameter> sqlParams) throws Exception{		
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(this.jdbcTemplate)
	    //.withSchemaName(schema)
	    //.withCatalogName(package)
	    .withProcedureName(spNameParamString);
		if(sqlParams != null && sqlParams.size() >0){
			int size = sqlParams.size();
			SqlParameter[] spArray = sqlParams.toArray(new SqlParameter[sqlParams.size()]);
			jdbcCall.declareParameters(spArray);
			Map<String, Object> values = new HashMap<String, Object>();
			for(int i=0; i<size; i++){
				SqlParameter sp = sqlParams.get(i);
				if(sp instanceof MySqlParameter){
					MySqlParameter msp = (MySqlParameter)sp;
					values.put(msp.getName(), msp.value);
				}
				else if(sp instanceof SqlOutParameter){
					SqlOutParameter sop = (SqlOutParameter)sp;
					String name = sop.getName();
					values.put(sop.getName(), "");
				}
			}
			return jdbcCall.execute(values);
		}
		else{
			return jdbcCall.execute();
		}
	}
	public static boolean getBooleanColumnValue(ResultSet rs, String columnName)throws SQLException {
		try{
			return rs.getBoolean(columnName);
		}
		catch(Exception e){
			//do nothing as given column name may not be present in select clause
		}
		return false;
	
	}
	public static double getDoubleColumnValue(ResultSet rs,String colmunName)throws SQLException{
		try{
			return rs.getDouble(colmunName);
		}
		catch(Exception e){
			//do nothing as given column name may not be present in select clause
		}
		return 0.0;
	}
	
	public static String getStringColumnValue(ResultSet rs, String columnName) throws SQLException {
		try{
			return rs.getString(columnName);
		}
		catch(Exception e){
			//do nothing as given column name may not be present in select clause
		}
		return null;
	}
	public static Integer getIntColumnValue(ResultSet rs, String columnName) throws SQLException {
		try{
			return rs.getInt(columnName);
		}
		catch(Exception e){
			//do nothing as given column name may not be present in select clause
		}
		return null;
	}
	
	class MySqlParameter extends SqlParameter{
		Object value;

		public MySqlParameter(String name, int sqlType, Object value) {
			super(name, sqlType);
			this.value = value;
		}		
	}
	
	/*Query retrieve the result from database and Row mapper map the each records into java object*/
  public class ProductCategoryIDRowMapper implements RowMapper<ProductCategoryVO>{
		@Override
		public ProductCategoryVO mapRow(ResultSet rs, int arg1)throws SQLException {
			ProductCategoryVO productCategory=new ProductCategoryVO();
			productCategory.setSysId(DAOBase.getStringColumnValue(rs, "product_category_id"));
			return productCategory;
		}		
	}
  
  public class ProductIDRowMapper implements RowMapper<ProductDetailVO>{
		@Override
		public ProductDetailVO mapRow(ResultSet rs, int arg1)throws SQLException {
			ProductDetailVO productCategory=new ProductDetailVO();
			productCategory.setSysId(DAOBase.getStringColumnValue(rs, "product_id"));
			return productCategory;
		}
		
	}
  
  
  public class PurchaseCartIDRowMapper implements RowMapper<CartVO>{
	@Override
	public CartVO mapRow(ResultSet rs, int arg1) throws SQLException {
		CartVO cart=new CartVO();
		cart.setSysId(DAOBase.getStringColumnValue(rs, "cartId"));
		return cart;
	}
	  
  }
  public class ImeiNumberFromCartIDRowMapper implements RowMapper<CartVO>{
		@Override
		public CartVO mapRow(ResultSet rs, int arg1) throws SQLException {
			CartVO cart=new CartVO();
			cart.setImeiNumber(DAOBase.getStringColumnValue(rs, "imeino"));
			return cart;
		}
		  
	  }
  public class PurchaseProductIDRowMapper implements RowMapper<CartVO>{

	@Override
	public CartVO mapRow(ResultSet rs, int arg1) throws SQLException {
		 CartVO cart=new CartVO();
		 CartItemVo cartItem=new CartItemVo();
		 cartItem.setSysId(DAOBase.getStringColumnValue(rs, "productId"));
		 List<CartItemVo>cartItemList=new ArrayList<CartItemVo>();
		 cartItemList.add(cartItem);
		 cart.setListCartItem(cartItemList);		 
		 return cart;
	}
	  
  }
  
 
  public class ProductImageIDRowMapper implements RowMapper<ProductImageVO>{

	@Override
	public ProductImageVO mapRow(ResultSet rs, int arg1) throws SQLException {
		ProductImageVO image=new ProductImageVO();
		image.setSysId(DAOBase.getStringColumnValue(rs, "image_id"));		
		return image;
	}
	  
  }
   	
  public class mstProductCategoryIDRowMapper implements RowMapper<MasterProductVO>{

	@Override
	public MasterProductVO mapRow(ResultSet rs, int arg1) throws SQLException {
		 MasterProductVO product=new MasterProductVO();
		 product.setSysId(DAOBase.getStringColumnValue(rs, "master_product_id"));
		 return product;
	}
	  
  }
  
  public class orderIdRowMapper implements RowMapper<ProductOrderVO>{

	@Override
	public ProductOrderVO mapRow(ResultSet rs, int arg1) throws SQLException {
		ProductOrderVO order=new ProductOrderVO();
	    order.setSysId(DAOBase.getStringColumnValue(rs, "order_id"));
		return order;
	}
	  
  }
  
  public class productPriceRowMapper implements RowMapper<ProductDetailVO>{

	@Override
	public ProductDetailVO mapRow(ResultSet rs, int arg1) throws SQLException {
		
		ProductDetailVO product =new ProductDetailVO();
		product.setPrice(DAOBase.getDoubleColumnValue(rs, "price"));
		product.setProductName(DAOBase.getStringColumnValue(rs, "product_name"));
		return product;
	}
	  
  }
  
  public class purchaseProductQuantityRowMapper implements RowMapper<CartItemVo>{

	@Override
	public CartItemVo mapRow(ResultSet rs, int arg1) throws SQLException {
		CartItemVo cart=new CartItemVo();
		cart.setSysId(DAOBase.getStringColumnValue(rs, "productId"));
		cart.setQuantity(DAOBase.getIntColumnValue(rs, "quantity"));
		cart.setProductName(DAOBase.getStringColumnValue(rs, "product_name"));
		cart.setPrice(DAOBase.getDoubleColumnValue(rs, "price"));
		return cart;
	}
	  
  }
  
  public class verifyUserRowMapper implements RowMapper<UserVO>{

	@Override
	public UserVO mapRow(ResultSet rs, int arg1) throws SQLException {
		UserVO user=new UserVO();
		user.setVerifiedUser(DAOBase.getBooleanColumnValue(rs, "isVerifiedUser"));		
		return user;
	}
	  
  }
  

	@Override
	public TransactionStatus startTransaction() throws Exception {
		DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();			 
		return dataSourceTransactionManager.getTransaction(paramTransactionDefinition );
	}

	@Override
	public void commitTransaction(TransactionStatus status) throws Exception {
		if(status != null){
			dataSourceTransactionManager.commit(status);
		}
	}

	@Override
	public void rollbackTransaction(TransactionStatus status) throws Exception {
		if(status != null){
			dataSourceTransactionManager.rollback(status);
		}

	}

	
	
}
