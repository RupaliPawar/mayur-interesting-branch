package com.ssi.mayur.utility;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.awt.image.BufferedImage;



	
public class UtilityFile {
	public static byte[] getFileAsByteArray(File file) throws Exception{
		
		int imageLen = (int)file.length();
		byte[] imgData = new byte[imageLen];
		FileInputStream fis = new FileInputStream(file);
		fis.read(imgData);
		return imgData;
		
	}
	
}
