package com.ssi.mayur.utility;

import java.nio.ByteBuffer;

public class UUIDUtil {
	
	/*This module use to generate unique system id*/
	public static java.util.UUID getTimeUUID() {

		return java.util.UUID.fromString(new com.eaio.uuid.UUID().toString());

	}
	
	public static java.util.UUID toUUID( byte[] uuid ) {
		return java.util.UUID.fromString(getUUIDAsString( uuid ));
	}
	public static java.util.UUID toUUID( String uuid ) {
		return java.util.UUID.fromString(getUUIDAsString( uuid.getBytes() ));
	}

	
	public static java.util.UUID getRandomUUID(){
		return java.util.UUID.randomUUID();
	}
	
	public static byte[] asByteArray(java.util.UUID uuid) {

		ByteBuffer buffer = ByteBuffer.allocate(16);
	
		buffer.putLong(uuid.getMostSignificantBits());
	
		buffer.putLong(uuid.getLeastSignificantBits());
	
		return buffer.array();

	}
	public static String getUUIDAsString(java.util.UUID uuid) {
		byte[] bytes = asByteArray(uuid);		
		String strUUID = getUUIDAsString(bytes);
		return strUUID;
	}
	public static String getNewUUIDAsString() {
		byte[] bytes = asByteArray(getRandomUUID());		
		String strUUID = getUUIDAsString(bytes);
		return strUUID;
	}
	
	private static String getUUIDAsString( byte[] uuid ) {
		ByteBuffer buffer = ByteBuffer.allocate(16);	
		buffer.put(uuid);	
		buffer.rewind();	
		com.eaio.uuid.UUID u = new com.eaio.uuid.UUID(buffer.getLong(),buffer.getLong());	
		return u.toString();
	}
}
