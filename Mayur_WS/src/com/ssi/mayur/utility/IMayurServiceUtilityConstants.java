package com.ssi.mayur.utility;

public interface IMayurServiceUtilityConstants {
	//web services method	
	public static final String M_CREATE_MASTER_PRODUCT_CATEGORY="createMasterProductCategory";//PHP
	public static final String M_MODIFY_MASTER_PRODUCT_CATEGORY="modifyMasterProductCategory";//PHP
	public static final String M_GET_MASTER_PRODUCT_CATEGORY="getMasterProductCategory";//PHP and Android
	
	public static final String M_CREATE_PRODUCT_CATEGORY="createProductCategory";//PHP
	public static final String M_MODIFY_PRODUCT_CATEGORY="modifyProductCategory";//PHP
	public static final String M_GET_PRODUCT_CATEGORY="getProductCategory";//PHP
	
	public static final String M_CREATE_PRODUCT_FOR_CATEGORY="createPoductForCategory";//PHP
	public static final String M_MODIFY_PRODUCT_FOR_CATEGORY="modifyPoductForCategory";	//PHP	
	public static final String M_GET_ACTUAL_PRODUCT="getActualProduct";//PHP
	public static final String M_REMOVE_PRODUCT_IAMGE="removeProductImage";//PHP
	
	public static final String M_CREATE_ORDER_ENTRY="createOrderEntry";//Android
	public static final String M_CONFIRM_ORDER="confirmOrder";//Android
	public static final String M_GET_ORDER_HISTORY_FOR_USER="getOrderHistoryForUser";//PHP
	
	public static final String M_CANCEL_PRODUCT_ORDER="cancelProductOrder";
	public static final String M_UPDATE_ORDER_STATUS="updateOrderStatus";//PHP
	public static final String M_GET_ORDER_DETAILS="getOrderDetails";//PHP
	
	
	
	public static final String M_GET_CATEGORIES ="getCategories";//Android
	public static final String M_GET_PRODUCT_LIST_CATEGORY ="getPoductListForCategory";//Android retrieving image	
	public static final String M_GET_ITEM_DETAILS ="getItemDetails";//Android retrieving image	
	
	public static final String M_GET_DESIGNER="getDesigner";//Android
	public static final String M_GET_DESIGNER_WORK="getDesignerWork";//Android
	
	public static final String M_SAVE_IN_DESIGNBOOKLET="saveInDesignBooklet";//Android
	public static final String M_GET_DETAILS_FOR_DESIGNBOOKLET="getDetailsForDesignBooklet";//Android
	
	
	public static final String M_SAVE_CART_DETAILS="saveCartDetails";//Android
	public static final String M_GET_CART_DETAILS="getCartDetails";//Android
	public static final String M_REMOVE_PRODUCT_FROM_CART="removeProductFromCart";//Android
	
	public static final String M_AUTHENTICATE_USER = "authenticateUser";//PHP login		
	public static final String M_CREATE_USER = "createUser";//Android user creation	
	public static final String M_GET_USER = "getUser";//not using					
	public static final String M_LOGIN="login";//Android login for authorized user
	public static final String M_ACTIVATE_ACCOUNT="activateAccount";//Android 
	public static final String M_PASSWORD_RECOVERY="passwordRecovery";//Android not using
		
	
		
		    //services
			public static final String S_AUTH = "auth";
			public static final String S_ADMIN = "admin";
			public static final String S_FEEDBACK = "feedback";
			public static final String S_PRODUCT="product";
			public static final String S_DESIGNER="designer";
			public static final String S_DESIGNBOOKET="designBooklet";
			public static final String S_PURCHASECART="purchaseCart";
						
			//status
			public static final int STATUS_OK = 200;
			public static final int STATUS_NO_CONTENT=204;
			public static final int STATUS_EXIST_DATA=205;
			
			 /*Client error code*/
			public static final int STATUS_INVALID_INPUTS = 400;
			public static final int STATUS_UNAUTHORISED = 401;			
			public static final int STATUS_INTIALIZE_FIELD=403;	
			public static final int STATUS_ACCESS_DENIED=406;
			
			/*Server error code*/
			public static final int STATUS_INTERNAL_EROR = 500;			
			public static final int STATUS_SERVICE_UNAVAILABLE =503;
			public static final int STATUS_SERVICE_TIME_OUT =504;
	
			//message			
			public static final String INVALID_INPUT="Invalid Input";
			public static final String MESSAGE_NO_DATA="No data into database";
			public static final String MESSAGE_INTIALIZE_FIELD="Field must contain value";
			public static final String MESSAGE_SERVICE_UNAVAILABLE="Service Unavailable";
			public static final String MESSAGE_ACCESS_DENIY="Please activate your account";
			public static final String MESSAGE_EXIST_DATA="Already exist";
			public static final String MESSAGE_SERVICE_TIME_OUT="Service time out";
			
			//request headers
			public static final String H_AUTH_KEY = "authKey";
			
			//keys
			public static final String K_API_REQUEST_ID = "apiReqId";
			public static final String K_REQUEST = "Request";
			public static final String K_RESPONSE = "Response";
			
			//Delimiters
			//public static final String DELIMETER_FOR_REQUEST_LOG = "^";
			public static final String DELIMETER_LOG_FIELD  = "^";
			public static final String DELIMETER_FOR_KEY_VALUE = "=";


}
