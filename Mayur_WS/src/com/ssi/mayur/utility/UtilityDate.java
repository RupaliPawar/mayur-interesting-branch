package com.ssi.mayur.utility;



import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;
import org.joda.time.format.DateTimePrinter;
import org.joda.time.DateTime;

public class UtilityDate {
	public static final String F_FULL_DATE_TIME = "dd/MM/yyyy H:mm:ss SSS";
	public static final String DATE_FORMAT_E_d_MMM_YYYY_H_mm_ss_Z = "E, d MMM YYYY HH:mm:ss Z";//"Sat, 3 Aug 2013 19:33:56 +0530";
	public static String getFormatedDate(long milisec, String format){
		DateTime dateTime = new DateTime(milisec);
		return dateTime.toString(format);
	}
	public static String getFormatedUTCDate(long milisec, String format){
		DateTime dateTime = new DateTime(milisec);
		DateTime newDateTime = dateTime.toDateTime(DateTimeZone.UTC);
		return newDateTime.toString(format);
	}
	
	
	public static Long getLongDateFormStringDate(String date, String format){
		DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
		DateTime dt = formatter.parseDateTime(date);
		return dt.getMillis();
	}
	
	public static Long getLongDateFromString(String date, String... formats){
		if(formats != null){
			int length = formats.length;
			if(length > 0){
				DateTimeParser[] parsers = new DateTimeParser[length];
				for(int i=0; i<length; i++){
					DateTimeFormatter fmt = DateTimeFormat.forPattern(formats[i]);
					parsers[i] = fmt.getParser();
				}
				DateTimeFormatterBuilder dtfb = new DateTimeFormatterBuilder();
				dtfb.append(null, parsers);
				DateTimeFormatter dtf = dtfb.toFormatter();
				DateTime dt = dtf.parseDateTime(date);
				return dt.getMillis();
			}
		}
		return null;
	}
	

}
