package com.ssi.mayur.utility;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import sun.misc.BASE64Encoder;

import com.google.gson.Gson;


/*This module use to convert json string to java object*/
public class MayurServiceCommonUtility {
	private static Gson mGson = null;
	public static String getJson(Object obj){
		if(obj != null){
			return getGsonInstance().toJson(obj);
		}
		return "";
	}	
	public static Object getObjectFromJson(String jsonString, Type type){
		return getGsonInstance().fromJson(jsonString, type);
	}
	
	private static Gson getGsonInstance(){
		if(mGson == null){
			mGson = new Gson();
		}
		return mGson;
	}
	
	public static String concatinateStrings(String... strings){
		return getStringBuilderOfGivenStrings(strings).toString();
	}
	public static StringBuilder getStringBuilderOfGivenStrings(String... strings){
		StringBuilder sb = new StringBuilder();
		if(strings != null){
			int length = strings.length;			
			for(int i=0; i<length; i++){
				sb.append(strings[i]);
			}
		}
		return sb;
	}
	
	public static String getStackTrace(Exception e){
		StringBuilder sb = new StringBuilder();
		String seperator = " ";
		StackTraceElement[] steList = e.getStackTrace();
		if(steList != null){
			int length = steList.length;
			for(int i=0; i<length; i++){
				StackTraceElement ste = steList[i];
				sb.append(ste.getClassName()).append(seperator);
				sb.append(ste.getMethodName()).append(seperator);
				sb.append(ste.getLineNumber()).append(seperator);
				sb.append("\n");
			}
		}
		return sb.toString();
	}
	public static String getExceptionAsString(Exception e){
		StringBuilder sb = new StringBuilder();
		sb.append(e.getMessage()).append("\n");
		sb.append(getStackTrace(e));
		return sb.toString();
	}
	
	public static boolean isBlank(String strData){
		if(strData == null || "".equals(strData.trim())){
			return true;
		}
		return false;
	}
	
	public static byte[] getMD5(String strInput) throws NoSuchAlgorithmException{
		MessageDigest md = MessageDigest.getInstance("MD5");
		return md.digest(strInput.getBytes());
	}
	
	public static String getEncrypatedPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		//return new String(getMD5(password));
		MessageDigest md = null;	    
		md = MessageDigest.getInstance("SHA"); //step 2
		
	    md.update(password.getBytes("UTF-8")); //step 3
	    byte raw[] = md.digest(); //step 4
	    String hash = (new BASE64Encoder()).encode(raw); //step 5
	    
	    return hash; //step 6
	}
	
	
	
}
