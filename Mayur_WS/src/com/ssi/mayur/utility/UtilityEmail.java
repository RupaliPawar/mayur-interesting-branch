package com.ssi.mayur.utility;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.util.SharedByteArrayInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class UtilityEmail {
	public static Logger logger = LoggerFactory.getLogger(UtilityEmail.class);
	private static final String DATE_FORMAT_RECEIVED_DATE = "E, d MMM YYYY HH:mm:ss Z";//"Sat, 3 Aug 2013 19:33:56 +0530";
	private static final String H_FROM = "From";
	private static final String H_SUBJECT = "Subject";
	private static final String H_DATE = "Date";
	
	public static void sendEmailViaGmail(EmailVO emailVO, String gmailPassword) throws AddressException, MessagingException {
		

 
		Session session = getGmailSession(emailVO.getSendersEmailId(), gmailPassword);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailVO.getSendersEmailId()));
			ArrayList<String> toList = emailVO.getToList();
			StringBuilder sb = new StringBuilder();
			int size = toList.size();
			for(int i=0; i<size; i++){
				if(i>0){
					sb.append(",");
				}
				sb.append(toList.get(i));
			}
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(sb.toString()));
			message.setSubject(emailVO.getSubject());			
			message.setText(emailVO.getMessage());//edit
			message.setContent(emailVO.getMessage(), "text/html");
			
 
			Transport.send(message);
 	
	}
	
	private static Session getGmailSession(String emailId, String gmailPassword) {				 
		final String username = emailId;
		final String password = gmailPassword;
 
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });		
		return session;		
	}
	
	
	private static Session getGmailPopSession(String emailId, String gmailPassword) {				 
		final String username = emailId;
		final String password = gmailPassword;
 
		Properties props = new Properties();

		props.put("mail.pop3.host", "pop.gmail.com");

		props.put("mail.pop3.user", username);

		props.put("mail.pop3.socketFactory", 995);

		props.put("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

		props.put("mail.pop3.port", 995);

		Session session = Session.getDefaultInstance(props, new Authenticator() {
		    @Override
		    protected PasswordAuthentication getPasswordAuthentication() {
		        return new PasswordAuthentication(username, password);

		    }
		});
		return session;		
	}
	
	public static void readGmails(IEmailListener emailListner, String emailId, String password, Logger pLogger) throws MessagingException, IOException{
		if(pLogger == null){
			pLogger = logger;
		}
		Store store = null;
		Folder fldr = null;
		try{
			Session session = getGmailPopSession(emailId, password);
			
			store = session.getStore("pop3");
			store.connect("pop.gmail.com", emailId, password);
	
		    fldr = store.getFolder("INBOX");
	
		    fldr.open(Folder.READ_ONLY);
		    int messageCount = fldr.getMessageCount();
		    System.out.println("messageCount = " + messageCount);
	
		    Message[] msg = fldr.getMessages();
	
		    Address[] address;
	
		    EmailVO emailVO = null;
		    for (int i = 0; i < msg.length; i++) {
		    	
		    	Message message = msg[i];
		    	emailVO = new EmailVO();
		    	emailVO.setSubject(message.getSubject());
		    	
		    	// Retrieve the Multipart object from the message
		    	Multipart multipart = (Multipart) message.getContent(); 
		    	
		    	int count  = multipart.getCount();
		    	for(int j=0; j<count; j++){
		    		Part part = multipart.getBodyPart(j);
		    		
		    		String disposition = part.getDisposition();

		    	      if (disposition == null) {
		    	    	  
		    	    	  
		    	      }
		    	      else if(disposition.equals(Part.ATTACHMENT) || disposition .equals(Part.INLINE)){
		    	    	  MimeBodyPart mbp = (MimeBodyPart) part;
		    	    	  
		    	    	  if (mbp.isMimeType("text/plain")) {
		    	    	        System.out.println("Email Content = \n" + (String) mbp.getContent());
		    	    	  } 
		    	    	  else {
		    	    		  if(emailListner != null){
		    	    			  emailListner.fileAttachmentFound(message, part, decodeName(part.getFileName()));
		    	    		  }		    	    		 	    	        
		    	    	  }
		    	    	  
		    	      }
		    	}		    	
		    }
		    //closing folder and store is not done in finally because once closed we can not get same email
		    //again as we are using POP. Thus in case of exception we do not close folder store
		    try{
				fldr.close(true);
			}
			catch(Exception e){
				//do nothing
			}
			try{
		    	store.close();
			}
			catch(Exception e){
				//do nothing
			}
		}
		catch(MessagingException e){
			throw e;
		}
		catch(IOException e){
			throw e;
		}
		
	}
	
	public static void printHeader(Message message, Logger pLogger){
		if(pLogger == null){
			pLogger = logger;
		}
		Enumeration<Header> headerEnum = null;
		try {
			headerEnum = message.getAllHeaders();
		} catch (MessagingException e) {
			pLogger.error("MessagingException in printHeader for getAllHeaders = ", e);	
			return;
		}
		if(headerEnum != null){
			while(headerEnum.hasMoreElements()){
				Header header = headerEnum.nextElement();
				String key = header.getName();
				String value = header.getValue();				
				pLogger.info("key="+key+", vlalue="+value);					
			}
		}
	}
	
	private static String[] getHeader(Message message, String header, Logger pLogger){
		
		if(pLogger == null){
			pLogger = logger;
		}
		try {
			String values[] = message.getHeader(header);
			return values;			
		} catch (MessagingException e) {
			pLogger.error("MessagingException in getDate", e);			
		}
		return null;
	}
	public static String getReceivedDateAsString(Message message, Logger pLogger){
		
		if(pLogger == null){
			pLogger = logger;
		}
		String values[] = getHeader(message, H_DATE, pLogger);
		System.out.println("values[] ----------------->" + values);
		if(values != null && values.length>0){
				return values[0];
		}
			
		return "";
	}
	public static Long getReceivedDateAsLong(Message message, Logger pLogger){
		
		if(pLogger == null){
			pLogger = logger;
		}
		String strDate = getReceivedDateAsString(message, pLogger);
		if(strDate != null ){
			strDate = strDate.trim();
			if(strDate.length()>0){
				return UtilityDate.getLongDateFormStringDate(strDate, DATE_FORMAT_RECEIVED_DATE);
			}
		}
		return null;
	}
	
	private static String decodeName(String name) throws UnsupportedEncodingException {
		  if (name == null || name.length() == 0) {
		   return "unknown";
		  }
		  String ret = "";
		
		  ret = java.net.URLDecoder.decode(name, "UTF-8");
		 

		  // also check for a few other things in the string:
		  ret = ret.replaceAll("=\\?utf-8\\?q\\?", "");
		  ret = ret.replaceAll("\\?=", "");
		  ret = ret.replaceAll("=20", " ");

		  return ret;
	}

	public static int saveEmailAttachmentFileOnHardDisk(File saveFile, Part part) throws IOException, MessagingException{

		  BufferedOutputStream bos = null;
		  InputStream is = null;
		  int count = 0;
		  try{
			  bos = new BufferedOutputStream(new FileOutputStream(saveFile));
	
			  byte[] buff = new byte[2048];
			  is = part.getInputStream();
			  int ret = 0;
			  while ((ret = is.read(buff)) > 0) {
				  bos.write(buff, 0, ret);
				  count += ret;
			  }
		  }
		  catch(IOException e){
			  throw e;
		  }
		  catch(MessagingException e){
			  throw e;
		  }
		  finally{
			  try{
				  bos.close();
			  }
			  catch(Exception e){
				  //do nothing
			  }
			  
			  try{
				  is.close();
			  }
			  catch(Exception e){
				  //do nothing
			  }			  
		  }
		  return count;
	}
	
	public static EmailVO getEmailVO(){
		return new EmailVO();
	}
	public static class EmailVO {
		private String sendersEmailId;
		private String subject;
		private String message;
		private ArrayList<Object> attachment = new ArrayList<Object>();
		private ArrayList<String> toList = new ArrayList<String>();
		private ArrayList<String> ccList = new ArrayList<String>();
		private ArrayList<String> bccList = new ArrayList<String>();
		
		
		
		public String getSendersEmailId() {
			return sendersEmailId;
		}
		public void setSendersEmailId(String sendersEmailId) {
			this.sendersEmailId = sendersEmailId;
		}
		public ArrayList<Object> getAttachment() {
			return attachment;
		}
		public void addAttachment(Object obj){
			attachment.add(obj);
		}
		public ArrayList<String> getCcList() {
			return ccList;
		}
		public ArrayList<String> getBccList() {
			return bccList;
		}
		public String getSubject() {
			return subject;
		}
		public void setSubject(String subject) {
			this.subject = subject;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		
		public ArrayList<String> getToList(){
			return toList;
		}
		public void clearTiList(){
			if(toList == null){
				toList = new ArrayList<String>();
			}
			toList.clear();
		}
		public void addEmailInToList(String emailId){
			if(toList == null){
				toList = new ArrayList<String>();
			}
			toList.add(emailId);
		}
	}
	
	public interface IEmailListener{
		public void fileAttachmentFound(Message message, Part part, String name);
	}
}

