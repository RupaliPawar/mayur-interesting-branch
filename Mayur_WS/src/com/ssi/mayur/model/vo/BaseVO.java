package com.ssi.mayur.model.vo;

public class BaseVO {
	//This class use to generate unique id for all java object
	public static final String SYS_ID = "sysId";
	private String sysId;

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}
	
}
