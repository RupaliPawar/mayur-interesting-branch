package com.ssi.mayur.model.vo;

public class MasterProductVO extends BaseVO{
//	This class contain master product details
	private String masterProductName = null;
	private boolean isVisibleMasterProduct;
	private String masterProductImage = null;
	
	
	public String getMasterProductName() {
		return masterProductName;
	}

	public void setMasterProductName(String masterProductName) {
		this.masterProductName = masterProductName;
	}

	public String getMasterProductImage() {
		return masterProductImage;
	}

	public void setMasterProductImage(String masterProductImage) {
		this.masterProductImage = masterProductImage;
	}

	public boolean isVisibleMasterProduct() {
		return isVisibleMasterProduct;
	}

	public void setVisibleMasterProduct(boolean isVisibleMasterProduct) {
		this.isVisibleMasterProduct = isVisibleMasterProduct;
	}


}
