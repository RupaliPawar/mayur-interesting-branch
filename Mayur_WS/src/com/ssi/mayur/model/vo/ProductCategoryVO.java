package com.ssi.mayur.model.vo;

public class ProductCategoryVO extends BaseVO {
//	This class contain product category details
	private String productCategoryName=null;
	private String productCategoryImage=null;
	private String masterProductId=null;
	private boolean isVisibleProductCategory;

	public String getProductCategoryName() {
		return productCategoryName;
	}

	public void setProductCategoryName(String productCategoryName) {
		this.productCategoryName = productCategoryName;
	}

	
	public String getMasterProductId() {
		return masterProductId;
	}

	public void setMasterProductId(String masterProductId) {
		this.masterProductId = masterProductId;
	}

	public String getProductCategoryImage() {
		return productCategoryImage;
	}

	public void setProductCategoryImage(String productCategoryImage) {
		this.productCategoryImage = productCategoryImage;
	}

	public Boolean getIsVisibleProductCategory() {
		return isVisibleProductCategory;
	}

	public void setIsVisibleProductCategory(boolean isVisibleProductCategory) {
		this.isVisibleProductCategory = isVisibleProductCategory;
	}

		
}
