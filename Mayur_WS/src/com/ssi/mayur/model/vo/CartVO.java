package com.ssi.mayur.model.vo;

import java.util.ArrayList;
import java.util.List;

public class CartVO extends BaseVO{
	
	private String imeiNumber;
	List<CartItemVo> listCartItem = new ArrayList<CartItemVo>(); 

	public String getImeiNumber() {
		return imeiNumber;
	}
	public void setImeiNumber(String imeiNumber) {
		this.imeiNumber = imeiNumber;
	}
	public List<CartItemVo> getListCartItem() {
		return listCartItem;
	}
	public void setListCartItem(List<CartItemVo> listCartItem) {
		this.listCartItem = listCartItem;
	}
	
	
	

}
