package com.ssi.mayur.model.vo;

public class UserVO  extends BaseVO{
//	This class contain user details
	
	private String username;
	private String password;
    private String emailId;
    private String createdOn;    
    private String address;       
	private String fname;
	private String pincode;
	private String city;
	private String state;
	private String mobileNo;
	private String landLineNo;
	private boolean isVerifiedUser;
	private String role;
	private String activateAccountCode;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getLandLineNo() {
		return landLineNo;
	}
	public void setLandLineNo(String landLineNo) {
		this.landLineNo = landLineNo;
	}
	public boolean isVerifiedUser() {
		return isVerifiedUser;
	}
	public void setVerifiedUser(boolean isVerifiedUser) {
		this.isVerifiedUser = isVerifiedUser;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getActivateAccountCode() {
		return activateAccountCode;
	}
	public void setActivateAccountCode(String activateAccountCode) {
		this.activateAccountCode = activateAccountCode;
	}
		
}
