package com.ssi.mayur.model.vo;

import java.util.ArrayList;
import java.util.List;

public class ProductDetailVO extends BaseVO{	
	private String productName;
	private String productCategoryId;
	private Double price;
	private String color;
	private String description;
	private boolean isVisibleProduct;
	private int stock;
	private int orderedQuantity;
	
	private List<ProductImageVO>productImageList=new ArrayList<ProductImageVO>();
	

	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

	public String getProductCategoryId() {
		return productCategoryId;
	}
	public void setProductCategoryId(String productCategoryId) {
		this.productCategoryId = productCategoryId;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public List<ProductImageVO> getProductImageList() {
		return productImageList;
	}
	public void setProductImageList(List<ProductImageVO> productImageList) {
		this.productImageList = productImageList;
	}
	public boolean getIsVisibleProduct() {
		return isVisibleProduct;
	}
	public void setIsVisibleProduct(boolean isVisibleProduct) {
		this.isVisibleProduct = isVisibleProduct;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public int getOrderedQuantity() {
		return orderedQuantity;
	}
	public void setOrderedQuantity(int orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}
	
	

}
