package com.ssi.mayur.model.vo;

public class CustomerReviewVO extends BaseVO  {
//	This class contain customer review details
	private String customerImage;
	private double customerRating;
	private String customerReviewDescription;
	private String reviewDate;	
	private String customerName;
	
	public String getCustomerImage() {
		return customerImage;
	}
	public void setCustomerImage(String customerImage) {
		this.customerImage = customerImage;
	}
	public double getCustomerRating() {
		return customerRating;
	}
	public void setCustomerRating(double customerRating) {
		this.customerRating = customerRating;
	}
	public String getCustomerReviewDescription() {
		return customerReviewDescription;
	}
	public void setCustomerReviewDescription(String customerReviewDescription) {
		this.customerReviewDescription = customerReviewDescription;
	}
	public String getReviewDate() {
		return reviewDate;
	}
	public void setReviewDate(String reviewDate) {
		this.reviewDate = reviewDate;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
		
}
