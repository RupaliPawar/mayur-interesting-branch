package com.ssi.mayur.model.vo;

public class ProductImageVO extends BaseVO {
//	This class contain product image details
	
	private String productImage;
	private String productId;
	
		
	public String getProductImage() {
		return productImage;
	}
	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	

}
