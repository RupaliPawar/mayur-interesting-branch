package com.ssi.mayur.model.vo;

public class WorkVO extends BaseVO {
//	This class contain  designer work details
	private String designerId;
	private String workimage;
	private String workDescription;

	public String getWorkimage() {
		return workimage;
	}

	public void setWorkimage(String workimage) {
		this.workimage = workimage;
	}

	public String getDesignerId() {
		return designerId;
	}

	public void setDesignerId(String designerId) {
		this.designerId = designerId;
	}

	public String getWorkDescription() {
		return workDescription;
	}

	public void setWorkDescription(String workDescription) {
		this.workDescription = workDescription;
	}

}
