package com.ssi.mayur.model.vo;

import java.util.ArrayList;
import java.util.List;

public class ProductOrderVO  extends BaseVO{
//	This class contain order details
	
	private String userEmailId;
	private String orderDate;
	private String cartId;
	private String orderStatus;
	 private String transactionID;
	List<ProductDetailVO> productList=new ArrayList<ProductDetailVO>();	
	UserVO user=new UserVO();
	
	public String getUserEmailId() {
		return userEmailId;
	}
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}
	
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getCartId() {
		return cartId;
	}
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}	
	public List<ProductDetailVO> getProductList() {
		return productList;
	}
	public void setProductList(List<ProductDetailVO> productList) {
		this.productList = productList;
	}
	public UserVO getUser() {
		return user;
	}
	public void setUser(UserVO user) {
		this.user = user;
	}
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	
}
