package com.ssi.mayur.model.vo;

import java.util.ArrayList;
import java.util.List;

public class DesignerExpertVO extends BaseVO {	
//	This class contain designer expert details
	private String name;
	private String place;
	private double rating;		
	private String contactNo;
	private String emailId;
	private String personImageUrl;
	private String detailOFDesignFirm;
	private String workImageUrl;
	
	private List<WorkVO>workImage=new ArrayList<WorkVO>();
	private List<CustomerReviewVO>custReview=new ArrayList<CustomerReviewVO>();
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}

	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getDetailOFDesignFirm() {
		return detailOFDesignFirm;
	}
	public void setDetailOFDesignFirm(String detailOFDesignFirm) {
		this.detailOFDesignFirm = detailOFDesignFirm;
	}
	public List<WorkVO> getWorkImage() {
		return workImage;
	}
	public void setWorkImage(List<WorkVO> workImage) {
		this.workImage = workImage;
	}
	public String getWorkImageUrl() {
		return workImageUrl;
	}
	public void setWorkImageUrl(String workImageUrl) {
		this.workImageUrl = workImageUrl;
	}
	public String getPersonImageUrl() {
		return personImageUrl;
	}
	public void setPersonImageUrl(String personImageUrl) {
		this.personImageUrl = personImageUrl;
	}
	
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public List<CustomerReviewVO> getCustReview() {
		return custReview;
	}
	public void setCustReview(List<CustomerReviewVO> custReview) {
		this.custReview = custReview;
	}	

}
