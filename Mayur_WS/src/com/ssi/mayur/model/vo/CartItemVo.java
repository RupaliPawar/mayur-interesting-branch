package com.ssi.mayur.model.vo;

public class CartItemVo extends ProductDetailVO{

	private int quantity;

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
