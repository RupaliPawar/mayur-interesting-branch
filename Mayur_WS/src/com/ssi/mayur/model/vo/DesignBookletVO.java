package com.ssi.mayur.model.vo;

public class DesignBookletVO extends BaseVO{
//	This class contain design booklet details
	private String imeiNumber;
	private String productId;
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getImeiNumber() {
		return imeiNumber;
	}
	public void setImeiNumber(String imeiNumber) {
		this.imeiNumber = imeiNumber;
	}	

}
