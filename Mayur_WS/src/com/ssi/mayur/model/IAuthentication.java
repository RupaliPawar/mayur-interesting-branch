package com.ssi.mayur.model;

import com.ssi.mayur.model.vo.AuthenticationUserDetails;
import com.ssi.mayur.model.vo.UserVO;



public interface IAuthentication {
	//use for login into PHP application
	public UserVO authenticateUserLogin(UserVO userVO) throws Exception;
	
	//use retrieve the user authenticate detail for PHP application
	public AuthenticationUserDetails getAuthenticationUserDetails(String username) throws Exception;
}
