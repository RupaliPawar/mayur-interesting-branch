package com.ssi.mayur.model;

import java.util.List;

import com.ssi.mayur.model.vo.DesignerExpertVO;

public interface IDesigner {
//  module use retrieve  detail of designers 
	public List<DesignerExpertVO> getDesigner()throws Exception;
	
//	 module use retrieve  detail of designers work for specified designer
	public DesignerExpertVO getDesignerWork(String designerId)throws Exception;

}
