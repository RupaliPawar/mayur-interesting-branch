package com.ssi.mayur.model;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;













import com.ssi.mayur.dao.IGeneralDAO;
import com.ssi.mayur.dao.mysql.GeneralDAOImpl;
import com.ssi.mayur.exception.InvalidInputsException;
import com.ssi.mayur.model.util.UtilityModel;
import com.ssi.mayur.model.vo.CartVO;
import com.ssi.mayur.model.vo.DesignBookletVO;
import com.ssi.mayur.model.vo.ProductCategoryVO;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductOrderVO;
import com.ssi.mayur.model.vo.MasterProductVO;
import com.sun.org.apache.regexp.internal.recompile;

public class GeneralImpl implements IGeneral {
	@Autowired
	IGeneralDAO generalDAO;

	/*This module use for create master product category*/
	@Override
	public void createMasterProductCategory(MasterProductVO masterProduct) throws Exception {
		masterProduct.setSysId(UtilityModel.getNewRoomSysId());
		generalDAO.createMasterProductCategory(masterProduct);	
	}

	@Override
	public void modifyMasterProductCategory(MasterProductVO masterProduct) throws Exception {
		generalDAO.modifyMasterProductCategory(masterProduct);		
	}

	@Override
	public List<MasterProductVO> getMasterProductCategory() throws Exception {
		List<MasterProductVO> roomList=generalDAO.getMasterProductCategory();
		return roomList;
	}


	@Override
	public void createProductCategory(ProductCategoryVO productCategory) throws Exception {
		productCategory.setSysId(UtilityModel.getNewProductCategorySysID());
		generalDAO.createProductCategory(productCategory);		
	}

	@Override
	public void modifyProductCategory(ProductCategoryVO productCategory) throws Exception {
		generalDAO.modifyProductCategory(productCategory);
	}


	@Override
	public List<ProductCategoryVO> getProductCategory() throws Exception {
		List<ProductCategoryVO>productList=generalDAO.getProductCategory();
		return productList;
	}

	
	@Override
	public void createPoductForCategory(ProductDetailVO productDetails) throws Exception {
		productDetails.setSysId(UtilityModel.getNewProductSysId());
		for (int i = 0; i < productDetails.getProductImageList().size(); i++) {
			productDetails.getProductImageList().get(i).setProductId(productDetails.getSysId());
			productDetails.getProductImageList().get(i).setSysId(UtilityModel.getNewProductImageSysID());			
		}
		generalDAO.createPoductForCategory(productDetails);
	}

	@Override
	public void modifyPoductForCategory(ProductDetailVO productDetails)throws Exception {
		generalDAO.modifyPoductForCategory(productDetails);		
	}

	
	@Override
	public List<ProductDetailVO> getActualProduct() throws Exception {
		return generalDAO.getActualProduct();
		
	}
	
	@Override
	public void removeProductImage(ProductDetailVO productImage)throws Exception {
	      generalDAO.removeProductImage(productImage);
	}


	@Override
	public List<ProductCategoryVO> getCategories(String masterProductId)throws Exception {
		List<ProductCategoryVO> productCategory=generalDAO.getCategories(masterProductId);		
		return productCategory;
	}

	@Override
	public List<ProductDetailVO> getPoductListForCategory(String productCategoryId) throws Exception {
		List<ProductDetailVO>productListCategory=generalDAO.getPoductListForCategory(productCategoryId);
		return productListCategory;		   		
	}

	@Override
	public ProductDetailVO getItemDetails(String productId)throws Exception {
		ProductDetailVO productDetail=generalDAO.getItemDetails(productId);
		return productDetail;	
	}
	
	@Override
	public void saveInDesignBooklet(DesignBookletVO designBooklet)throws Exception {					
		generalDAO.saveInDesignBooklet(designBooklet);				
	}

	@Override
	public List<ProductDetailVO> getDetailsForDesignBooklet(String imieNumber)throws Exception {
		List<ProductDetailVO>designbook=generalDAO.getDetailsForDesignBooklet(imieNumber);		
		return designbook;
	}

	@Override
	public CartVO saveCartDetails(CartVO cartDetails) throws Exception {
		CartVO cartList=generalDAO.saveCartDetails(cartDetails);	
		return cartList;	
	}

	@Override
	public CartVO getCartDetails(String imeiNumber) throws Exception {
		CartVO cartDetailsList=generalDAO.getCartDetails(imeiNumber);		
		return cartDetailsList;
	}

	@Override
	public CartVO removeProductFromCart(String cartId,String productId) throws Exception {
		return generalDAO.removeProductFromCart(cartId,productId);
		
	}
		
	@Override
	public void createOrderEntry(ProductOrderVO order) throws Exception {	
		order.setSysId(UtilityModel.getNewOrderSysId().substring(0,13));
		generalDAO.createOrderEntry(order);	
	
	}

	@Override
	public ProductOrderVO getOrderHistoryForUser(String userId)throws Exception {
		ProductOrderVO product=generalDAO.getOrderHistoryForUser(userId);
		return product;
	}

	@Override
	public List<ProductOrderVO> getorderDetails() throws Exception {
		List<ProductOrderVO>orderDetail=generalDAO.getorderDetails();
		return orderDetail;
	}

	@Override
	public void updateOrderStatus(ProductOrderVO order) throws Exception {
		generalDAO.updateOrderStatus(order);
		
	}

	@Override
	public void confirmOrder(ProductOrderVO order) throws Exception {		
		generalDAO.confirmOrder(order);	
	}

	
}
