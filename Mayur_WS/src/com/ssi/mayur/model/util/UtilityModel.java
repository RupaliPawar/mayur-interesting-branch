package com.ssi.mayur.model.util;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;

import com.ssi.mayur.model.vo.CartItemVo;
import com.ssi.mayur.model.vo.ProductOrderVO;
import com.ssi.mayur.model.vo.UserVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.utility.UUIDUtil;
import com.ssi.mayur.utility.UtilityEmail;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIConversion.User;

public class UtilityModel {
	/*This module use to generate unique system id*/
	protected static Logger logger = LoggerFactory.getLogger(UtilityModel.class);
	
	
	
	public static String getNewAPIRequestSysId(){
		return MayurServiceCommonUtility.concatinateStrings("REQU_", UUIDUtil.getNewUUIDAsString());
	}

	public static String getNewProductCategorySysID(){
		return MayurServiceCommonUtility.concatinateStrings("PRODUCTCATEGORY_",UUIDUtil.getNewUUIDAsString());
	}
	public static String getNewProductSysId(){
		return MayurServiceCommonUtility.concatinateStrings("PRODUCT_",UUIDUtil.getNewUUIDAsString());
	}
	
	public static String getNewRoomSysId(){
		return MayurServiceCommonUtility.concatinateStrings("PRODUCTMASTER_",UUIDUtil.getNewUUIDAsString());
	}
	
	public static String getNewUserSysId(){
	return MayurServiceCommonUtility.concatinateStrings("USER_", UUIDUtil.getNewUUIDAsString());
		
	}
	public static String getNewOrderSysId(){
		return MayurServiceCommonUtility.concatinateStrings("ORDER_",UUIDUtil.getNewUUIDAsString());
	}
	public static String getNewDesignBookletSysID(){
		return MayurServiceCommonUtility.concatinateStrings("DESIGNBOOKLET_",UUIDUtil.getNewUUIDAsString());
	}
	
	public static String getNewPurchaseCartSysID(){
		return MayurServiceCommonUtility.concatinateStrings("CART_",UUIDUtil.getNewUUIDAsString());
	}
	
	public static String getNewProductImageSysID(){
		return MayurServiceCommonUtility.concatinateStrings("PRODUCTIAMGE_",UUIDUtil.getNewUUIDAsString());
	}
	
	
	/*This module send an email to user after order has been place*/
	public static void sendEmailForProductOrder(List<CartItemVo> productList,ProductOrderVO order,String senderAccount,String senderPassword) throws Exception{
		try{
		
	    double sum=0;		
		UtilityEmail.EmailVO email = new UtilityEmail.EmailVO();
		email.setSubject("Mayur-IntResting.com");
		
		StringBuilder sb=new StringBuilder();
		sb.append("Dear ");
		sb.append(order.getUser().getFname()+",<br>");
		sb.append("Thank you for your order. This e-mail confirms that your order has been received by us.<br>");	
		sb.append("<b>Order ID :");
		sb.append(order.getSysId()+"</b>&nbsp;&nbsp;");
		sb.append("<b>Order Status :</b>");
		sb.append(order.getOrderStatus());
		sb.append("<br>");
		sb.append("<b>Product Delivery Address :</b><br>");
		sb.append(order.getUser().getAddress()+",<br>");
		sb.append(order.getUser().getCity()+",<br>");
		sb.append(order.getUser().getPincode()+",<br>");
		sb.append(order.getUser().getState()+",<br>");
		sb.append("Mobile No-");
		sb.append(order.getUser().getMobileNo()+"<br>");
				
		sb.append("<table border=1 >");
		sb.append("<tr>");
		sb.append("<td>Product Name</td>");
		sb.append("<td>Quantity</td>");
		sb.append("<td>Price</td>");
		sb.append("<td>Total</td>");
		sb.append("</tr>");
		
        for (int i = 0; i < productList.size(); i++) {
        	sb.append("<tr>");
    		sb.append("<td>"+productList.get(i).getProductName()+"</td>");
    		sb.append("<td>"+productList.get(i).getQuantity()+"</td>");
    		sb.append("<td>"+productList.get(i).getPrice()+"</td>");
    		sb.append("<td>"+productList.get(i).getPrice()*productList.get(i).getQuantity()+"</td>");
    		sum=sum+(productList.get(i).getPrice()*productList.get(i).getQuantity());        
    		sb.append("</tr>");
		  }			
		sb.append("<tr>");
		sb.append("<td colspan=3><b>Grand Total</b></td>");
		sb.append("<td>"+sum+"</td>");
		sb.append("</tr>");
		
		sb.append("</table>");
	
		sb.append("Note: Post shipment, the delivery of your item(s) will take anywhere between 1 to 4 days depending on the location being served");
		email.setMessage(sb.toString());
		email.setSendersEmailId(senderAccount);//sender
		email.addEmailInToList(order.getUserEmailId());//receiver
		UtilityEmail.sendEmailViaGmail(email, senderPassword);
	}     	
	catch (Exception e) {			
		throw e;
	}	
	
	}
	
	/*This module send an email to user for activate account */
	public static void sendEmailForActivateAccount(UserVO user,String senderEmailId,String senderPassword) throws Exception{
		try{

		UtilityEmail.EmailVO email = new UtilityEmail.EmailVO();
		email.setSubject("Mayur-IntResting.com");
		
		StringBuilder sb=new StringBuilder();
		sb.append("Dear customer,\n");		
		sb.append("Thank you for account creation.");
		sb.append("Please verify your user account by login-in with your email id and password in the application and then enter the activation code provided in this mail.");
		sb.append("Your activation code:-");		
		sb.append("<b>"+user.getActivateAccountCode()+"</b>");
		
		
				
		
		email.setMessage(sb.toString());
		email.setSendersEmailId(senderEmailId);//sender
		email.addEmailInToList(user.getEmailId());//receiver
		UtilityEmail.sendEmailViaGmail(email, senderPassword);
	}     	
	catch (Exception e) {			
		throw e;
	}	
	
	}
	
	
	
	public static void trial(String senderAccount,String senderPassword) throws Exception{
		try{
		
	    double sum=0;
		
		UtilityEmail.EmailVO email = new UtilityEmail.EmailVO();
		email.setSubject("Mayur-IntResting.com");
		
		StringBuilder sb=new StringBuilder();
		sb.append("Dear");
		sb.append("Rupali,<br>");
		sb.append("Thank you for your order. This e-mail confirms that your order has been received by us successfully.<br>");
		sb.append("<b>Order ID :");
		sb.append("Order1234</b>&nbsp;&nbsp;");
		sb.append("<b>Order Status :</b>");
		sb.append("Process");
		sb.append("<br>");
		sb.append("<b>Product Delivery Address :</b><br>");
		sb.append("Good will Apartment,<br>");
		sb.append("Sec-25,Thane,<br>");
		sb.append("Maharashtra,<br>");
		sb.append("Mobile No-");
		sb.append("74954385783<br>");
		sb.append("Pin code-123 456<br>");		
		sb.append("<table border=1 >");
		sb.append("<tr>");
		sb.append("<td>Product Name</td>");
		sb.append("<td>Quantity</td>");
		sb.append("<td>Price</td>");
		sb.append("<td>Total</td>");
		sb.append("</tr>");
		//for loop start from here 
		
		sb.append("<tr>");
		sb.append("<td>Chair</td>");
		sb.append("<td>1</td>");
		sb.append("<td>2000</td>");
		sb.append("<td>2000</td>");
		sb.append("</tr>");
		
		sb.append("<tr>");
		sb.append("<td>Table</td>");
		sb.append("<td>2</td>");
		sb.append("<td>1000</td>");
		sb.append("<td>2000</td>");
		sb.append("</tr>");
		sb.append("</tr>");
		
		sb.append("<tr>");
		sb.append("<td colspan=3><b>Grand Total</b></td>");
		sb.append("<td>4</td>");
		sb.append("</tr>");
		
		sb.append("</table>");
		
		sb.append("Note: Post shipment, the delivery of your item(s) will take anywhere between 1 to 4 days depending on the location being served");

		
		
		email.setMessage(sb.toString());
		email.setSendersEmailId(senderAccount);//sender
		email.addEmailInToList("rupal70it@gmail.com");//receiver
		UtilityEmail.sendEmailViaGmail(email, senderPassword);
	}     	
	catch (Exception e) {			
		throw e;
	}	
	
	}
	

}
