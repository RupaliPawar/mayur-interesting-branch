package com.ssi.mayur.model.util;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class EnvironmentProperties {
	/*This class is use to setup environment properties  */
	public static PropertyPlaceholderConfigurer allProperties = null;

	public static PropertyPlaceholderConfigurer getAllProperties() {
		return allProperties;
	}

	public static void setAllProperties(PropertyPlaceholderConfigurer allProperties) {
		EnvironmentProperties.allProperties = allProperties;
	}
	
}
