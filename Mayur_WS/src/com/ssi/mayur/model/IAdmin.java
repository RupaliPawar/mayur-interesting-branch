package com.ssi.mayur.model;

import com.ssi.mayur.model.vo.UserVO;
public interface IAdmin {
	public UserVO login(UserVO user)throws Exception;//use for login into mobile apps
	public void createUser(UserVO user)throws Exception;//create user account for mobile apps
	public UserVO getUser(String userName)throws Exception;//give user detail
	public UserVO activateAccount(UserVO user)throws Exception;//activate the user account for authenticate user
}
