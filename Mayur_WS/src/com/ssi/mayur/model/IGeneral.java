package com.ssi.mayur.model;

import java.util.List;

import sun.org.mozilla.javascript.internal.EcmaError;







import com.ssi.mayur.model.vo.CartVO;
import com.ssi.mayur.model.vo.DesignBookletVO;
import com.ssi.mayur.model.vo.ProductCategoryVO;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductOrderVO;
import com.ssi.mayur.model.vo.MasterProductVO;

public interface IGeneral {	 
//	 module use for create,modify and retrieve the master product category data
	 public void createMasterProductCategory(MasterProductVO masterProduct)throws Exception;
	 public void modifyMasterProductCategory(MasterProductVO masterProduct)throws Exception; 
	 public List<MasterProductVO> getMasterProductCategory()throws Exception;
	 
//	 module use for create,modify and retrieve the product category data 
	 public void createProductCategory(ProductCategoryVO productCategory)throws Exception;
	 public void modifyProductCategory(ProductCategoryVO productCategory)throws Exception;
	 public List<ProductCategoryVO>getProductCategory()throws Exception; 
	 
//	 module use for create,modify and retrieve the product data 
     public void createPoductForCategory(ProductDetailVO productDetails)throws Exception;
     public void modifyPoductForCategory(ProductDetailVO productDetails)throws Exception;
     public List<ProductDetailVO>getActualProduct()throws Exception;
     
//   module use for remove product image
     public void removeProductImage(ProductDetailVO productImage)throws Exception;
     
//   module use for retrieve product category details for retrieve a list of product category for specified master product category 
     public List<ProductCategoryVO> getCategories(String masterProductId)throws Exception;	
     
// 	 module use for to retrieve a list of product  for specified product category 
	 public List<ProductDetailVO> getPoductListForCategory(String productCategoryId)throws Exception;
	
//	 module use for to retrieve a one product details for specified  product 
	 public ProductDetailVO getItemDetails(String productId)throws Exception;
     
	 
//	 module use for to store details of like product 
     public void saveInDesignBooklet(DesignBookletVO designBooklet)throws Exception;
     
//   module of retrieve  details of like product which u have store into the Designbooklet
     public List<ProductDetailVO>getDetailsForDesignBooklet(String imieNumber)throws Exception;
     
     
//   module use for to add product into cart
     public CartVO saveCartDetails(CartVO cartDetails)throws Exception;
     
//   module use for to retrieve details of product available into cart   
     public CartVO getCartDetails(String imeiNumber)throws Exception;
     
//   module use for to remove product from cart
     public CartVO removeProductFromCart(String cartId,String productId)throws Exception;
 
     
     public void createOrderEntry(ProductOrderVO order)throws Exception;
     
     public void confirmOrder(ProductOrderVO order)throws Exception; 
     
     public List<ProductOrderVO> getorderDetails()throws Exception;
     
     public void updateOrderStatus(ProductOrderVO order)throws Exception;
     
     public ProductOrderVO getOrderHistoryForUser(String userId)throws Exception;
     
     
}

