package com.ssi.mayur.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.ssi.mayur.dao.IAdminDAO;
import com.ssi.mayur.exception.InvalidInputsException;
import com.ssi.mayur.model.vo.AuthenticationUserDetails;
import com.ssi.mayur.model.vo.UserVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;



public class AuthenticationImpl implements IAuthentication{
	@Autowired
	@Qualifier("adminDAO")
	IAdminDAO adminDAO;

	@Override
	public UserVO authenticateUserLogin(UserVO userVO) throws Exception {
		UserVO user = adminDAO.getUser(userVO.getUsername());
		if(!user.getPassword().equals(userVO.getPassword())){
			throw new InvalidInputsException("Given password is wrong.Please Enter a correct password");
		}
//		if(user.getPassword().equals(MayurServiceCommonUtility.getEncrypatedPassword(userVO.getPassword()))){
//			user.setIsAuthorised(true);
//		}
//		user.setIsAuthorised(true);
		return user;
	}
	@Override
	public AuthenticationUserDetails getAuthenticationUserDetails(String username) throws Exception {
		UserVO user = adminDAO.getUser(username);
		AuthenticationUserDetails authUserDetails = new AuthenticationUserDetails();
		authUserDetails.setPassword(user.getPassword());
		authUserDetails.setUsername(user.getUsername());
		return authUserDetails;
	}

	

}
