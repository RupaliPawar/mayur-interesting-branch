package com.ssi.mayur.model;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ssi.mayur.dao.IDesignerDAO;
import com.ssi.mayur.dao.IGeneralDAO;
import com.ssi.mayur.model.vo.DesignerExpertVO;

public class DesignerImpl implements IDesigner {
	
	
	@Autowired
	IDesignerDAO designerDAO;

	@Override
	public List<DesignerExpertVO> getDesigner() throws Exception {	
		List<DesignerExpertVO>designerList=designerDAO.getDesigner();		
		return designerList;
	}

	@Override
	public DesignerExpertVO getDesignerWork(String designerId)throws Exception {
		DesignerExpertVO designerWorkList=designerDAO.getDesignerWork(designerId);	
		return designerWorkList;
	}

}
