package com.ssi.mayur.model;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.ssi.mayur.dao.IAdminDAO;
import com.ssi.mayur.model.util.UtilityModel;
import com.ssi.mayur.model.vo.UserVO;
import com.ssi.mayur.utility.UUIDUtil;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIConversion.User;



public class AdminImpl implements IAdmin{
	@Autowired
	@Qualifier("adminDAO")
	IAdminDAO adminDAO;

	@Override
	public UserVO login(UserVO user) throws Exception {
	   UserVO uservo=adminDAO.login(user);
	   return uservo;
	}

	@Override
	public void createUser(UserVO user) throws Exception {		
		adminDAO.createUser(user);

	}

	@Override
	public UserVO getUser(String userName) throws Exception {
	    UserVO user=adminDAO.getUser(userName);
		return user;
	}

	@Override
	public UserVO activateAccount(UserVO user) throws Exception {
		UserVO uservo=adminDAO.activateAccount(user);
		return uservo;
	}
	
	
}
