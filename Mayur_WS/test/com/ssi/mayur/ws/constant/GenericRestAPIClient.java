package com.ssi.mayur.ws.constant;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class GenericRestAPIClient {

	public static String makeGetCall(String url) throws ClientProtocolException, IOException {

		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);			
		HttpResponse response = client.execute(request);		
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuilder sbResponse = new StringBuilder();
		String line = "";
		while ((line = rd.readLine()) != null) {
			sbResponse.append(line);
		}
		return sbResponse.toString();
		}

	public static String makePostCall(String url, String postData) throws ClientProtocolException, IOException {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		post.setHeader("authKey", "authKeyVal");
		StringEntity input = new StringEntity(postData);
		input.setContentType("application/json");
		post.setEntity(input);
		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuilder sbResponse = new StringBuilder();
		String line = "";
		while ((line = rd.readLine()) != null) {
			sbResponse.append(line);
		}
		return sbResponse.toString();
	}

}
