package com.ssi.mayur.ws.constant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.BeforeClass;






import com.google.gson.reflect.TypeToken;
import com.ssi.mayur.utility.IMayurServiceUtilityConstants;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.ws.vo.ErrorVO;
import com.ssi.mayur.ws.vo.ResponseVO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class JunitTestBase implements IMayurConstant,IMayurServiceUtilityConstants {
	protected static String authKeyVal = "";
	protected static WebResource service;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ClientConfig config = new DefaultClientConfig();
	    Client client = Client.create(config);
	    service = client.resource(MAYUR_WS_LOCAL_URL);
	}
	protected ResponseVO checkForSuccess(ClientResponse response){
		ResponseVO responseBean = checkForResponse(response);
		Integer statusCode = responseBean.getStatusCode();
		assertNotNull("Response Status Code is Null", statusCode);
		
		if(STATUS_OK != statusCode){
			ErrorVO errorBean = (ErrorVO) MayurServiceCommonUtility.getObjectFromJson(responseBean.getData(), new TypeToken<ErrorVO>(){}.getType() );
			assertEquals(MayurServiceCommonUtility.concatinateStrings( errorBean.getMessage(), " Status Code"), new Integer(STATUS_OK), statusCode);
		}
		return responseBean;
	}
	
	protected ResponseVO checkForResponse(ClientResponse response){
		String data = response.getEntity(String.class);
		System.out.println(data);
		ResponseVO responseBean = (ResponseVO)MayurServiceCommonUtility.getObjectFromJson(data, new TypeToken<ResponseVO>(){}.getType() );
		assertNotNull("ResponseBean is Null", responseBean);
		return responseBean;
	}
}
