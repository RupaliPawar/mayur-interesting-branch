package com.ssi.mayur.ws.constant;

public interface IConstants {
	 public static final String CONVERSATION_POSITION = "conversation_position";
	 public static final String WEB_SERVICE_URL = "http://54.200.43.254:8080/AirIT_WS/api/"; // 54.200.43.254//"http://192.168.1.20:8081/AirIT_WS/api/"; http://192.168.1.20:8080/AirITService_WS/api/admin/login
	 public static final String WEB_SERVICE_METHOD_AUTHENTICATION ="admin/login";
	 public static final String WEB_SERVICE_METHOD_USER_DATA_SUBBMISSION ="conversation/saveDataCaptured";
	 public static final String WEB_SERVICE_METHOD_GET_USER_DATA = "conversation/getClientSpecificDataCaptureField";
	 public static final String WEB_SERVICE_METHOD_GET_USER_SURVEY_QUESTION ="conversation/getClientSpecificSurveyQuestions";
	 public static final String WEB_SERVICE_METHOD_USER_SURVEY_SUBBMISSION ="conversation/saveSurveryFeedback";
	 
	 public static final String SCREEN_DATA_FIELD_TYPE_NAME_TEXT="text";
	 public static final String SCREEN_DATA_FIELD_TYPE_NAME_RADIO_BUTTON="rdbt";
	 public static final String SCREEN_DATA_FIELD_TYPE_NAME_CHECK_BOX="chkbox";
	 public static final String SCREEN_DATA_FIELD_TYPE_NAME_NUMBER="number";
	 public static final String SCREEN_DATA_FIELD_TYPE_NAME_ALPHANUMERIC="alphanum";
	 
	 public static final String SCREEN_DATA_FIELD_DETAIL_TYPE_NAME_LABEL="label";
	 public static final String SCREEN_DATA_FIELD_DETAIL_TYPE_NAME_OPTION="option";
	}
