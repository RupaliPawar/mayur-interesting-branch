package com.ssi.mayur.ws.rest;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.util.StopWatch;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ssi.mayur.model.vo.CartItemVo;
import com.ssi.mayur.model.vo.CartVO;
import com.ssi.mayur.model.vo.MasterProductVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.ws.constant.GenericRestAPIClient;
import com.ssi.mayur.ws.constant.JunitTestBase;
import com.ssi.mayur.ws.vo.ResponseVO;

public class PurchaseCartServiceTest extends JunitTestBase {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPurchaseCartService() {
		fail("Not yet implemented");
	}
	
	/*
	 PRODUCT_39659b70-a38b-4a47-9cb4-41408d4f6eac
	 PRODUCT_924788d0-2eb1-4546-a23b-8bba3abfd74a
	 PRODUCT_662e84ed-b35d-4e2c-b5b0-28c53a0e0258
	 PRODUCT_5d295507-a2ba-4203-8d66-2a7d29ffb47d
	 */

	@Test
	public void testSaveCartDetails() {
	
		String methodname="purchaseCart/saveCartDetails";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);	
		CartVO cart=new CartVO();
//        cart.setSysId("CART_fa01cd4c-6b02-4363-863e-756f10bead15");
		cart.setImeiNumber("1234567");
		CartItemVo item=new CartItemVo();						
		item.setQuantity(3);
		item.setSysId("PRODUCT_39659b70-a38b-4a47-9cb4-41408d4f6eac");

		List<CartItemVo>itemList=new ArrayList<CartItemVo>();
		itemList.add(item);
		
		cart.setListCartItem(itemList);

		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(cart) );
			System.out.print(str);
			
//			List<CartVO>list=new ArrayList<CartVO>();
//			Gson gson =new Gson();			
//			ResponseVO res=gson.fromJson(str, ResponseVO.class);
//			CartVO cart1=gson.fromJson(res.getData(),CartVO.class);
//		    list=gson.fromJson(res.getData(),new TypeToken<ArrayList<CartVO>>() {} .getType() );
//		list.add(new CartVO());
			
		} catch (Exception e) {			
			e.printStackTrace();
		} 
		
		
	}

	@Test
	public void testGetCartDetails() throws Exception {
		String methodname="purchaseCart/getCartDetails/1234567";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);			
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString());
			System.out.print(str);		
		} catch (Exception e) {			
			e.printStackTrace();
		} 
//		try{     
//		    HttpGet httpGet = new HttpGet(sb.toString());
//		    HttpParams httpParameters = new BasicHttpParams();
//		    // Set the timeout in milliseconds until a connection is established.
//		    // The default value is zero, that means the timeout is not used. 
//		    int timeoutConnection = 5000;
//		    HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//		    // Set the default socket timeout (SO_TIMEOUT) 
//		    // in milliseconds which is the timeout for waiting for data.
//		    int timeoutSocket =8000;
//		    HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
//
//		    DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
//		    HttpResponse response = httpClient.execute(httpGet);
//		    BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//			StringBuilder sbResponse = new StringBuilder();
//			String line = "";
//			while ((line = rd.readLine()) != null) {
//				sbResponse.append(line);
//			}
//		    System.out.print(sbResponse.toString());
//		    
//		} catch (Exception e) {
//			    e.printStackTrace();
//		        //Here Connection TimeOut excepion    
////		      Toast.makeText(xyz.this, "Your connection timedout", 10000).show();
//		   }
		
	}

	@Test
	public void testRemoveProductFromCart() {
				
		String methodname="purchaseCart/removeProductFromCart";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);
		sb.append("/CART_fa01cd4c-6b02-4363-863e-756f10bead15/PRODUCT_4a858322-181d-4594-97fe-b2cc0c49f480");
	
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString());
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 		
		
	}

}
