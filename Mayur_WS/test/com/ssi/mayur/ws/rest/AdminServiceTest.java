package com.ssi.mayur.ws.rest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ssi.mayur.model.vo.MasterProductVO;
import com.ssi.mayur.model.vo.UserVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.validator.EmailValidator;
import com.ssi.mayur.ws.constant.GenericRestAPIClient;
import com.ssi.mayur.ws.constant.JunitTestBase;

public class AdminServiceTest extends JunitTestBase {
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testlogin() {
		String methodname="admin/login";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_REMOTE_URL);
		sb.append(methodname);	
		UserVO user=new UserVO();

		user.setEmailId("rupal70it@gmail.com");
		user.setPassword("rupali1234");

		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(user) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 	
	}
	
	@Test
	public void testcreateUser() {
		//testssiandroid
		String methodname="admin/createUser";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_REMOTE_URL);
		sb.append(methodname);	
		UserVO user=new UserVO();
//		user.setSysId("USER_51904555-e339-4b7c-b8e9-16123f5742c6");
		user.setAddress("sec-7,Shanti niwas");
		user.setCity("kalyan");
		user.setEmailId("ram7@gmail.com");
		user.setFname("rupali");
		user.setLandLineNo("23654178");
		user.setMobileNo("9874562130");
		user.setPassword("are");
		user.setPincode("452684");
		user.setState("maharashtra");
	
		/*{"address":"asdlaksjd nasdjnl nasdkjasl ,asjdhlkjas ,askldalskjd ,ka;lksdj","city":"Thane",
		 * "emailId":"rupal70it@gmail.com","fname":"Nikhil","state":"Maharashtra","landLineNo":"25420222",
		 * "mobileNo":"9920636666","password":"123456","pincode":"400602","isVerifiedUser":false}*/
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(user) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 		
	}
	
	
	@Test
	public void testactivateAccount() {
		//testssiandroid
		String methodname="admin/activateAccount";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);	
		UserVO user=new UserVO();

		user.setEmailId("rupal70it@gmail.com");
		user.setActivateAccountCode("3f51a5e6");

	
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(user) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 		
	}
	
	
	
	
	
	
	
	
	
	
	@Test
	public void testvalidate(){		
		EmailValidator e=new EmailValidator();
		String emailAdd="123.4fdas_d123c@bmail.com.in";
		boolean flag=e.validate(emailAdd);
		System.out.print(flag);
	}

}
