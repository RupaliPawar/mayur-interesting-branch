package com.ssi.mayur.ws.rest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;




import com.ssi.mayur.model.vo.DesignBookletVO;
import com.ssi.mayur.model.vo.ProductCategoryVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.ws.constant.GenericRestAPIClient;
import com.ssi.mayur.ws.constant.JunitTestBase;

public class DesignerBookletServiceTest extends JunitTestBase{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testDesignBookletService() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveInDesignBooklet() {
		String methodname="designBooklet/saveInDesignBooklet";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);	
		DesignBookletVO booklet=new DesignBookletVO();
		booklet.setImeiNumber("1245");
		booklet.setProductId("1234");
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(booklet) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 		
	}

	@Test
	public void testGetDetailsForDesignBooklet() {
		String methodname="designBooklet/getDetailsForDesignBooklet/1234";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);		
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString());
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 	
	}

}
