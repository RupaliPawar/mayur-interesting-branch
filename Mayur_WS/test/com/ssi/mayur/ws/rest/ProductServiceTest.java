package com.ssi.mayur.ws.rest;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


















import com.ssi.mayur.model.vo.ProductCategoryVO;
import com.ssi.mayur.model.vo.ProductDetailVO;
import com.ssi.mayur.model.vo.ProductImageVO;
import com.ssi.mayur.model.vo.ProductOrderVO;
import com.ssi.mayur.model.vo.MasterProductVO;
import com.ssi.mayur.utility.MayurServiceCommonUtility;
import com.ssi.mayur.ws.constant.GenericRestAPIClient;
import com.ssi.mayur.ws.constant.JunitTestBase;



public class ProductServiceTest extends JunitTestBase {
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	@Before
	public void setUp() throws Exception {
	}
	@Test
	public void test() {
		fail("Not yet implemented");
	}	
	@Test
	public void testcreateMasterProductCategory(){
		String methodname="product/createMasterProductCategory";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);		
		MasterProductVO room=new MasterProductVO();
		room.setMasterProductName("Electronics1235");
		room.setVisibleMasterProduct(true);
		room.setMasterProductImage("http://54.201.34.102/intrest/assets/1400824693rounded tab.png");
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(room) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 
		
		//file name does not contain this character *,/,\,|,:,?,<,>,"
		
//		int price=34;
//		boolean test=room.getMasterProductName().matches("[A-Za-z]+");
//		System.out.print(test);
		
		
		
	}
	
		
	@Test
	public void testmodifyMasterProductCategory(){
		String methodname="product/modifyMasterProductCategory";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);		
		MasterProductVO room=new MasterProductVO();
		room.setSysId("PRODUCTMASTER_2d696dc4-e113-4a83-a1fe-3524d8d6f3ac");
		room.setMasterProductName("Test");
		room.setVisibleMasterProduct(false);
//		room.setMasterProductImage("image1");;
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(room) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 		
	}
	
	@Test
	public void testgetMasterProductCategory(){
		String methodname="product/getMasterProductCategory";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_REMOTE_URL);
		sb.append(methodname);				
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString());
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 			
	}	
	
	@Test
	public void testgetCategories(){
		String methodname="product/getCategories/PRODUCTMASTER_f7abdaf0-bebf-4435-8e42-bf0717315ccd";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_REMOTE_URL);
		sb.append(methodname);		
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString());
 
			 System.out.print(str.toString());
		
		} catch (Exception e) {			
			e.printStackTrace();
		} 	
	}
	@Test
	public void testcreateProductCategory() {		
		String methodname="product/createProductCategory";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);		
		ProductCategoryVO productcategory=new ProductCategoryVO();
		productcategory.setProductCategoryName("Test product category");
		productcategory.setProductCategoryImage("http://54.201.34.102/intrest/assets/1400822039chairtab.png");
		productcategory.setMasterProductId("PRODUCTMASTER_2d696dc4-e113-4a83-a1fe-3524d8d6f3ac");
		productcategory.setIsVisibleProductCategory(true);
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(productcategory) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 		
	}
	@Test
	public void testmodifyProductCategory(){
		String methodname="product/modifyProductCategory";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);
		
		//http://54.201.34.102/intrest/assets/1400580137Kimi-signature.jpg
		ProductCategoryVO productcategory=new ProductCategoryVO();
		productcategory.setSysId("PRODUCTCATEGORY_0f1f6caf-6c7e-47d7-9950-b37f1b8311b9");
		productcategory.setProductCategoryName("Sofa cum Beds change");
		productcategory.setMasterProductId("PRODUCTMASTER_2d696dc4-e113-4a83-a1fe-3524d8d6f3ac");
		productcategory.setIsVisibleProductCategory(false);
//		productcategory.setProductCategoryImage("img");
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(productcategory) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 	
	}
	
	
	@Test
	public void testcreatePoductForCategory(){
		String methodname="product/createPoductForCategory";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);
		 
		ProductDetailVO product=new ProductDetailVO();
		product.setProductName("Alpha testing");
		product.setColor("black");
		product.setDescription("description");
		product.setPrice(20000.00);
		product.setIsVisibleProduct(true);
		product.setStock(8);
		product.setProductCategoryId("PRODUCTCATEGORY_30daeb79-85f7-4d6e-ab17-a6e63a8e1081");
		
		
		ProductImageVO image1=new ProductImageVO();
		ProductImageVO image2=new ProductImageVO();
		ProductImageVO image3=new ProductImageVO();
		image1.setProductImage("abc");
		image2.setProductImage("imageurl2");
		image3.setProductImage("image3");
		List<ProductImageVO>imageList=new ArrayList<ProductImageVO>();
		imageList.add(image1);
		imageList.add(image2);
//		imageList.add(image3);
		product.setProductImageList(imageList);
		
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(product) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 	
		
	}
	
	
	@Test
	public void testmodifyPoductForCategory(){
		String methodname="product/modifyPoductForCategory";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);
		
		ProductDetailVO product=new ProductDetailVO();
		product.setProductName("testProduct changes");
		product.setColor("yellow");
		product.setDescription("details");
		product.setPrice(1111.23);
		product.setIsVisibleProduct(false);
		product.setStock(6);
		product.setSysId("PRODUCT_25cfb794-3af4-4a80-9b18-d57481bdc903");
		product.setProductCategoryId("PRODUCTCATEGORY_30daeb79-85f7-4d6e-ab17-a6e63a8e1081");
		
		
//		ProductImageVO image1=new ProductImageVO();
//		ProductImageVO image2=new ProductImageVO();
//		image1.setProductImage("imageurl1 change");
//		image1.setSysId("PRODUCTIAMGE_0159d031-b637-4577-b230-858fdf93f0d3");
//		
//		image2.setProductImage("imageurl2 change");
//		image2.setSysId("PRODUCTIAMGE_a90491f5-669a-4042-b156-32c7260e3a70");
//		List<ProductImageVO>imageList=new ArrayList<ProductImageVO>();
//		imageList.add(image1);
//		imageList.add(image2);
//		
//		product.setProductImageList(imageList);
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(product) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 	
	}

	@Test
	public void testgetActualProduct(){
		String methodname="product/getActualProduct";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);		
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString());
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 
		
	}
	
	@Test
	public void testremoveProductImage(){
		String methodname="product/removeProductImage";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);
		
		ProductDetailVO product=new ProductDetailVO();
//		product.setProductName("testProduct changes");
//		product.setColor("yellow");
//		product.setDescription("details");
//		product.setPrice(1111.23);
//		product.setIsVisibleProduct(false);
//		product.setSysId("PRODUCT_64233404-3e52-44b6-9007-8eafd1d66d8d");
//		product.setProductCategoryId("PRODUCTCATEGORY_134d6b37-4c41-4601-bb32-7b2c7a8f0922");
		
		
		ProductImageVO image1=new ProductImageVO();
		ProductImageVO image2=new ProductImageVO();
		ProductImageVO image3=new ProductImageVO();
		image1.setProductImage("imageurl1 change");
		image1.setSysId("PRODUCTIAMGE_b33af975-6491-40c3-93a7-6203ef2b133f");
		image1.setProductId("PRODUCT_55141f5c-067f-45d8-8aee-2954ca1ddda4");
		
		image2.setSysId("PRODUCTIAMGE_7776793a-7b35-458e-98d2-aa4efb728045");
		image2.setProductId("PRODUCT_55141f5c-067f-45d8-8aee-2954ca1ddda4");
//		image3.setSysId("PRODUCTIAMGE_b33af975-6491-40c3-93a7-6203ef2b133f");
//		image3.setProductId("PRODUCT_55141f5c-067f-45d8-8aee-2954ca1ddda4");
		List<ProductImageVO>imageList=new ArrayList<ProductImageVO>();
		imageList.add(image1);
		imageList.add(image2);
//		imageList.add(image3);
		
		product.setProductImageList(imageList);
		
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(product) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 	
		
	}
	
	
	@Test
	public void testgetOrderHistoryForUser(){
		String methodname="product/getOrderHistoryForUser/USER_7e70f297-f34d-4b18-a968-59ad3931b845";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_REMOTE_URL);
		sb.append(methodname);
		
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString());
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 
		
	}
	
	@Test
	public void testgetOrderDetails(){
		String methodname="product/getOrderDetails";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_REMOTE_URL);
		sb.append(methodname);
		
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString());
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 
		
	}
	
	
	//updateOrderStatus
	@Test
	public void testupdateOrderStatus(){
		String methodname="product/updateOrderStatus";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);

		
		ProductOrderVO order=new ProductOrderVO();
		order.setOrderStatus("deliverd");
		order.setSysId("order234");
		
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(order) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 	
		
	}
	
	
	@Test
	public void testcreateOrderEntry(){
		String methodname="product/createOrderEntry";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);

		
		ProductOrderVO order=new ProductOrderVO();
		order.setCartId("CART_6b746f13-d8d3-4ecd-95ef-1cc10cc516bc");	
		order.setUserEmailId("rupal70it@gmail.com");
		order.setTransactionID("123dsfs45");
		
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(order) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 	
		
	}
	
	
	@Test
	public void testconfirmOrder(){
		String methodname="product/confirmOrder";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_REMOTE_URL);
		sb.append(methodname);

		
		ProductOrderVO order=new ProductOrderVO();
		order.setCartId("CART_5ef0cd39-4cb3-45aa-b9ce-37c538010f6c");	
		order.setUserEmailId("rupal70it@gmail.com");
		order.setTransactionID("123dsfs45");
		
		try {
			String str=GenericRestAPIClient.makePostCall(sb.toString(),MayurServiceCommonUtility.getJson(order) );
			System.out.print(str);
		} catch (Exception e) {			
			e.printStackTrace();
		} 	
		
	}
	
	
	

	
	
	
	public String nextSessionId(SecureRandom random) {
	    return new BigInteger(130, random).toString(16);
	  
	  }
	@Test
	public void testgetProductCategory(){
//		String methodname="product/getProductCategory";
//		StringBuilder sb=new StringBuilder();
//		sb.append(MAYUR_WS_LOCAL_URL);
//		sb.append(methodname);		
//		try {
//			String str=GenericRestAPIClient.makeGetCall(sb.toString());
//			System.out.print(str);
//		} catch (Exception e) {			
//			e.printStackTrace();
//		}
		SecureRandom random = new SecureRandom();
		  
		  String xyz=nextSessionId(random);
		  String first8=xyz.substring(0,8);
		  System.out.print(first8);
		  
	}
	
	
	@Test
	public void testgetPoductListForCategory() {		
		String methodname="product/getPoductListForCategory/PRODUCTCATEGORY_2db8615d-c278-4c88-b39d-29705904d921";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);	
		sb.append(methodname);		
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString().trim());
			System.out.print(str);
		} catch (Exception e) {
			
			e.printStackTrace();
		} 		
	}
	@Test
	public void testgetItemDetails() {		
		String methodname="product/getItemDetails/PRODUCT_4a858322-181d-4594-97fe-b2cc0c49f480";
//		String parameter="p3".trim();
//		parameter=parameter.replaceAll("\\s", "");
//		parameter.trim();
		
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);
//		sb.append(parameter);
				
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString());
			System.out.print(str);
		} catch (Exception e) {
			
			e.printStackTrace();
		} 
		
	}
	
	
}
