package com.ssi.mayur.ws.rest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ssi.mayur.ws.constant.GenericRestAPIClient;
import com.ssi.mayur.ws.constant.JunitTestBase;

public class DesignerServiceTest extends JunitTestBase{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testDesignerService() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetDesigner() {
		
		String methodname="designer/getDesigner";
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString());
			System.out.print(str);
		} catch (Exception e) {			
			
			e.printStackTrace();
		}	
	}

	@Test
	public void testGetDesignerWork() {
		String methodname="designer/getDesignerWork/";
		String parameter="designer_2";
		parameter=parameter.replaceAll("\\s", "");
		StringBuilder sb=new StringBuilder();
		sb.append(MAYUR_WS_LOCAL_URL);
		sb.append(methodname);
		sb.append(parameter);
		try {
			String str=GenericRestAPIClient.makeGetCall(sb.toString());
			System.out.print(str);
		} catch (Exception e) {			
			
			e.printStackTrace();
		}	
		
		
	}

}
